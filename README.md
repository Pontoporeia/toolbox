# README #

### What is this repository for? ###

This repository contains bits and pieces of code, in different languages (java, javascript, etc.) used at Arts� - digital art classes.

It provides examples and common issue resolution.

The root folders are named depending on the language/plateform they are running on.

### How do I get set up? ###

* for any processing code
* * Download and install [processing](https://processing.org/download/)
* * Download all the files from this repo and place them in the sketch folder 
* * Launch processing
* * Open project "Utilitaires"

### OpenNI, processing & Mac OSX 10.8 ###

You may have a problem with openni on old versions of Mac OSX (experienced on 10.8).

The bug fix is slow but simple: install JDK from oracle, same version as processing is using.

Change the **libfreenect.0.1.2.dylib** and **libusb-1.0.0.dylib** used by openni with these ones: https://github.com/kronihias/head-pose-estimation/tree/master/mac-libs

Path: **[your sketch folder]/libraries/SimpleOpenNI/library/osx/OpenNI2/Drivers**

Info found here: http://forum.processing.org/two/discussion/873/error-when-trying-to-run-sketch-with-simpleopenni

### Who do I talk to? ###

* Created and managed by [frankie zafe](http://www.frankiezafe.org)
* Teaching at [Arts�](http://blog.artsaucarre.be/artsnumeriques/)