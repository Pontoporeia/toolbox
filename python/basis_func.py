def func( i, puissance = 2 ):
    try:
        num = float( i )
        return pow( num, puissance )
    except:
        print( "Impossible to square: " + str(i) )
        return 0.0

def mafunc( i ):
    return func(i, 10) / func(i, 3)

a = func( "2" )
b = func( None )
c = func( 4, 3 )
d = func( 156 ) + func( 43336 )
e = func( func( func( a ) ) )
print( a, b, c, d, e )

f = mafunc( 3 )
print( f )

