# https://www.tutorialspoint.com/python/python_lists.htm

a = "ljkhr"

print( a[:1] )

print( "#### tuple ####" )

tu = (9090,431,233,"gdfuygf", True)

#for( Object o in ArrayList<Object> list )
for t in tu: #for each t in tu
    print(t)

for i in range(0,len(tu)):
    print( i, tu[i] )

print( "#### list ####" )

li = [ 456, 467, 1234, 89 ]
li.append( 45 )
li.insert( 1, 900000 )
li.remove( 467 )

for l in li:
    print(l)

for i in range(0,len(li)):
    print( i, li[i] )

print( "#### dictionnary ####" )

fraise = {}
fraise["couleur"] = "rouge"
fraise["nom"] = "fraise"
fraise["poids"] = 50
fraise["gout"] = ["sucre", "acidite"]

for key in fraise:
    print(key, fraise[key] )

pomme = {}
pomme["couleur"] = "vert"
pomme["nom"] = "pomme"
pomme["poids"] = 190
pomme["gout"] = ["sucre", "astringeant"]

print( "#### tous les fruits ####" )

fruits = [ fraise, pomme ]

for fruit in fruits:
    print( fruit['nom'] )


