﻿print( "\nA00_basics, ou les prémices de la programmation python.\n" )

'''
# préambule #

Python, tout comme javascript et d'autres, est un language de script.
Il a été conçu pour faciliter l'apprentissage de la programmation, ce
qui fait de lui un incontournable en arts numériques.
Il a l'énorme avantage de fonctionner au coeur de l'os et de pouvoir
accéder à toutes ses fonctionnalités.

# exécution #

Pour exécuter ce script, 
* ouvrir une fenêtre de terminal,
* vérifier que vous êtes bien dans le bon dossier
* taper 'python A00_basics.py'

Si python n'est pas installer sur votre machine ou que 'terminal'
n'évoque absolument rien pour vous, allez sur 
http://anwiki.artsaucarre.be et rechercher "bases python"

Mais assez papoté, codons!
'''

# la variable

# création d'une variable nommée 'a', et mise d'une valeur dans la
# variable (assignation en language info.), le nombre '2' en l'occurence:
a = 2

# affichage de la valeur de 'a' dans la console
print( a )

# opération mathématique basique: l'addition
a = a + 5
print( a )

# remise de 'a' à 2
a = 2

# a = a + 5 est identique à:
a += 5
print( a )

# python permet de modifier la 'nature' de la valeur qui lui assignée 
# à une variable - ci-dessous, 'a' devient une chaîne de caractères
a = "un texte"
print( a )

# il est devenu impossible de lui ajouter un chiffre (nous  
# reviendrons sur 'try' et 'except' plus tard)
try:
	a += 5
except TypeError:
	print( "Cette tentative va générer une erreur vous disant: TypeError: cannot concatenate 'str' and 'int' objects" )

# l'erreur est explicite: 'a' est maintenant une chaîne de caractères
# ce n'est plus possible de lui ajouter un nombre
# par contre...
a += "5"
print( a )

# à la grande différence de la ligne 56, '5' entouré de guillemets 
# (double quotes) n'est plus un nombre mais une chaîne de
# caractères, il est donc possible de les ajouter l'une à l'autre
# (concaténation en language info.)

# création d'une deuxième variable:
b = 3
print( b )

# transfer de la valeur de 'b' dans la variable 'a'
a = b
# et modification de la variable 'b'
b = 5
# impression des 2 valeurs dans le terminal
print( a, b )

# simple...