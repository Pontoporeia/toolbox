try:
    from cStringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO
from PIL import Image
import os
import shutil
import subprocess

'''
------------- GLOBAL ------------- 
'''

# set to true if the source video has not been extracted already
regenarate_video_frames = False
recompress_video_frames = False
regenarate_output_video = True
# source video path
video = 'BHAD BHABIE - I Got It (Official Music Video) Danielle Breg.webm'
# folder path to export source video frames
folder_frames = 'igotit_frames'
# folder path to store compressed video frames
folder_compressed = 'igotit_compressed'
# type of compressor - JPG or GIF
compressor = 'JPG'

# jpg compressor settings
jpg_from = 40
jpg_to = 1
jpg_steps = 1

# gif compressor settings
gif_colors = 4

# output frames limits
limit_from = 1800
limit_to = 3125

# output frame rate
fps = 25

fprefix = 'output_'

'''
------------- FUNCTIONS ------------- 
'''

def create_folder(p):
	if not os.path.exists(p):
		os.makedirs(p)
	else:
		clear_folder(p)
		
def clear_folder(p):
	for root, dirs, files in os.walk( p ):
		for f in files:
			os.unlink(os.path.join( root, f ))
		for d in dirs:
			shutil.rmtree(os.path.join( root, d ))

def jpg_compress( src_path, dst_path ):
	jpg_q = jpg_from
	while jpg_q >= jpg_to:
		src = Image.open( src_path )
		buffer = BytesIO()
		src.save( buffer, "JPEG", quality = jpg_q, optimize=True, progressive=True )
		jpg_q -= jpg_steps
		buffer.seek(0)
		with open( dst_path, "w") as handle:
			handle.write(buffer.read())
		src_path = dst_path
		
def gif_compress( src_path, dst_path ):
	src = Image.open( src_path )
	buffer = BytesIO()
	src = src.convert('P', palette=Image.ADAPTIVE, colors=gif_colors)
	src.save( buffer, "PNG" )
	buffer.seek(0)
	with open( dst_path, "w") as handle:
		handle.write(buffer.read())

'''
------------- PROCESS ------------- 
'''

if regenarate_video_frames == True:
	create_folder( folder_frames )
	subprocess.call(['ffmpeg', '-i', video, '-vcodec', 'png', folder_frames + '/' + fprefix + '%05d.png' ])

if recompress_video_frames == True:
	create_folder( folder_compressed )
	ext = '.jpg'
	if compressor == 'GIF':
		ext = '.png'
	for root, dirs, files in os.walk( folder_frames ):
		files.sort()
		i = 0
		outi = 0
		for f in files:
			if limit_from > -1 and i >= limit_from and i < limit_to:
				outf = folder_compressed + '/' + fprefix
				if outi < 10:
					outf += '0000'
				elif outi < 100:
					outf += '000'
				elif outi < 1000:
					outf += '00'
				elif outi < 10000:
					outf += '0'
				outf += str(outi) + ext
				outi += 1
				if compressor == 'JPG':
					jpg_compress(  folder_frames + '/' + f, outf )
				elif compressor == 'GIF':
					gif_compress(  folder_frames + '/' + f, outf )
				print( str( outi ) + '/' + str( ( limit_to - limit_from ) ) + ' frame (' + ext + ')' )
			i += 1

if regenarate_output_video == True:
	movie_path = 'movie_' + str(fps) + 'fps_' + str(limit_from) + '-' + str(limit_to)
	im_path = folder_compressed + '/' + fprefix + '%5d'
	if compressor == 'JPG':
		movie_path += '_' + str(jpg_from) + '-' + str(jpg_to) + '-' + str(jpg_steps) + 'jpg'
		im_path += '.jpg'
	elif compressor == 'GIF':
		movie_path += '_' + str(gif_colors) + 'gif'
		im_path += '.png'
	else:
		pass
	movie_path += '.mkv'
	subprocess.call(['ffmpeg', '-f', 'image2', '-framerate', str(fps), '-r', str(fps), '-i', im_path, '-an', '-vcodec', 'mjpeg', '-q:v', '1', folder_compressed + '/' + movie_path ])
		

