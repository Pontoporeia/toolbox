
# opening the original text file in READ mode: "r"
inputfile = open( "text_to_html_src.txt", 'r' )
# opening/creating the output html file in WRITE mode: "w"
outputfile = open( "text_to_html_src.html", 'w' )

# special characters to remove from the output: tabs, line returns and new lines
invalid_chars = [ '\t', '\r', '\n' ]

# function to remove invalid characters from the original text
def cleanup( txt ):
	txt = txt.strip()
	txt = txt.replace( '   ',' ')
	txt = txt.replace( '  ', ' ')
	for ic in invalid_chars:
		txt = txt.replace( ic, '')
	return txt

# will wrap the given word or sentence in an H* tag
def header_word( line, word, level ):
	return line.replace( word,'<h'+str(level)+'>' + word + '</h'+str(level)+'><br/>')

# will wrap the given word or sentence in a SPAN tag with the given class ("style" argument)
def style_word( line, word, style ):
	return line.replace( word,'<span class="'+style+'">' + word + '</span>')

# will contains all valid lines
all_lines = []

# loading all lines of the text file in "all_lines"
for line in inputfile:
	cleanline = cleanup(line)
	if len(cleanline) == 0:
		continue
	print( len(cleanline), cleanline )
	all_lines.append( cleanline )

# closing the input file
inputfile.close()

# generation of the output html file

# header part with css definition
outputfile.write( '<html>' )
outputfile.write( '<head>' )
outputfile.write( '<style>' )
outputfile.write( 'html, body { font-family:Courier; font-size: 14px; color: #555; }' )
outputfile.write( 'body { margin: 150px; }' )
outputfile.write( 'h1, h2, h3, h4, h5, h6 { margin: 0; padding: 0; font-weight:normal; }' )
outputfile.write( 'p { padding: 0 0 10px 0; margin: 0 0 10px 0; border-bottom: dashed 1px #ccc; }' )
outputfile.write( 'span.bold { color: #000; font-weight: bold; }' )
outputfile.write( 'span.italic { color: #f00; font-style: italic; }' )
outputfile.write( 'span.green { letter-spacing: 2px; color: #fff; font-weight: bold; background: #f63; padding: 2px 10px; }' )
outputfile.write( '</style>' )
outputfile.write( '</head>' )
outputfile.write( '<body>' )
outputfile.flush()

# actual content, using style & header function to adapat the display
for l in all_lines:
	l = header_word( l, 'Lorem ipsum', 1 )
	l = style_word( l, 'at', 'bold' )
	l = style_word( l, 'eu', 'italic' )
	l = style_word( l, 'Sed', 'green' )
	outputfile.write( '<p>' + l + '</p>' )

# end of the file
outputfile.write( '</body>' )
outputfile.write( '</html>' )

# closing the output file
outputfile.close()