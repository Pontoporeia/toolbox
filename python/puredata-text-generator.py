import random
import math

f = open( "../puredata/generated.txt", "w" )
for i in range(0,10000):
	#f.write( str( random.random() ) + ";\n" )
	f.write( str( 
			( 
				( 1 + math.sin( i * 0.0578 ) ) +
				( 1 + math.sin( i * 0.04959212 ) ) +
				( 1 + math.sin( i * 0.03959212 ) )
			) * (1.0/6) ) + ";\n" )
f.close()