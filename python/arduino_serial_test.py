import sys,serial,time,socket

SERIAL_PORT='/dev/ttyATH0'
UDP_PORT=25000

print( "opening serial " + SERIAL_PORT )
arduino=serial.Serial( SERIAL_PORT, 9600, timeout=1 )
print( arduino )

print( "opening socket " + str(UDP_PORT) )
try:
	udpsocket = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
	udpsocket.bind(( "", UDP_PORT ))
except socket.error, msg:
	print( "Failed to connect to " + str( UDP_PORT ) + ", error: " + str(msg[0]) + ": " + msg[1] )
	sys.exit()
print( "socket started" )

while 1:
	data, addr = udpsocket.recvfrom(1024)
	if not data:
		break
	#print( "data received: ")
	#print( data )
	#print( addr )
	#print( "***********" )
	arduino.write( data )
	
udpsocket.close()

arduino.close()
print( "arduino closed" )
