﻿# regex for multiple spaces : \s(\w(?:\s\w){2,})\s
# test one https://regex101.com/
import re

def unspacer(txt):
    m = re.findall( '\s(\w(?:\s\w){2,})\s', txt )
    print( txt, m )
    out = txt
    for sb in m:
        needle = sb
        repl = ' {} '.format(re.sub(r'\s', '', sb))
        out = out.replace( needle, repl, 1 )
    return out

print( unspacer( 'je s u i s très c o n t ent  !' ) )
