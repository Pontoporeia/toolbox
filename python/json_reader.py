import json

def parse_ae_features( features ):
	# looping over layers
	for layer in features:
		print layer["name"]
		pd_config = []
		pd_previous_value = 0
		for feat in layer["feature_animations"]:
			if feat["property"] == "OPACITY":
				print "number of keys", len( feat["key_values"] )
				parse_opacity_key( feat["key_values"] )

def parse_opacity_key( data ):
	for k in range( 0, len( data ) ):
		samplei = int( data[k]["start_frame"] * sample_multiplier )
		value = float( data[k]["data"][0] )
		if len( pd_config ) == 0:
			pd_config.append(0)
		prev_value = pd_config[ len(pd_config) - 1 ]
		diff = value - prev_value
		diffi = samplei - len(pd_config)
		print "should add", diffi
		while( samplei > len(pd_config) ):
			prev_value += diff / diffi
			pd_config.append( prev_value )
		pd_config.append( value )
		print samplei
		#print data[k]["start_frame"]
		#print data[k]["data"]

pd_config = []
pd_previous_value = 0

f = open( "../puredata/jdo.aep.comp-68-Composition 1.kf.json" )
data = json.load( f )
sample_multiplier = 1

for a in data:
	if a == "animation_frame_count":
		sample_multiplier = 10000 / float(data[a])
		print "COUNT", data[a], sample_multiplier
	elif a == "features":
		parse_ae_features( data[a] )