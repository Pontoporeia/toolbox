import csv

f = open( "list.csv", 'rb' )
csv_content = csv.reader( f )

csv_rows = []
for row in csv_content:
	csv_rows.append( row )

'''
print( csv_content )
print( csv_rows )
'''

def is_in_csv( needle ):
	found = None
	for row in csv_rows:
		for column in row:
			if column.find( needle ) != -1:
				found = row
				break
		if found != None:
			break
	return found

def all_rows_csv( needle ):
	found = []
	for row in csv_rows:
		for column in row:
			if column.find( needle ) != -1:
				found.append( row ) 
				break
	return found

print( is_in_csv( 's' ) )
print( all_rows_csv( 'rt' ) )
print( is_in_csv( 'rt' ) )