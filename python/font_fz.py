﻿import os

ascii_font_fz = {
	'name': 'Xylophone Wagner',
	'characters': ' \tabcdefghijklmnopqrstuvwxz0123456789,;./\\*-+°²³#=?!()[]%&:$\'"',
	'lines': 5
}

# space character
ascii_font_fz[ 32 ] = '''
  
  
  
  
'''

# tab character
ascii_font_fz[ 9 ] = '''
    
 ¬  
    
    
'''

ascii_font_fz[ ord( 'a' ) ] = '''
 /¯\  
|___| 
|   | 
      
'''

ascii_font_fz[ ord( 'b' ) ] = '''
|¯¯)  
|---  
|___) 
      
'''

ascii_font_fz[ ord( 'c' ) ] = '''
|¯¯\ 
|    
 \__ 
     
'''

ascii_font_fz[ ord( 'd' ) ] = '''
|¯¯\  
|   | 
|___| 
      
'''

ascii_font_fz[ ord( 'e' ) ] = '''
 /¯¯ 
|__  
|___ 
     
'''

ascii_font_fz[ ord( 'f' ) ] = '''
 /¯¯ 
|__  
|    
     
'''

ascii_font_fz[ ord( 'g' ) ] = '''
|¯¯\  
|  _  
 \__| 
      
'''

ascii_font_fz[ ord( 'h' ) ] = '''
|   | 
|___| 
|   | 
      
'''

ascii_font_fz[ ord( 'i' ) ] = '''
  |  
  |  
  |  
     
'''

ascii_font_fz[ ord( 'j' ) ] = '''
   | 
   | 
|__/ 
     
'''

ascii_font_fz[ ord( 'k' ) ] = '''
|  | 
|__/ 
|  \ 
     
'''

ascii_font_fz[ ord( 'l' ) ] = '''
|     
|     
|___  
      
'''

ascii_font_fz[ ord( 'm' ) ] = '''
|  | 
|\/| 
|  | 
     
'''

ascii_font_fz[ ord( 'n' ) ] = '''
|\ | 
| \| 
|  | 
     
'''

ascii_font_fz[ ord( 'o' ) ] = '''
|¯¯\  
|   | 
 \__| 
      
'''

ascii_font_fz[ ord( 'p' ) ] = '''
|¯¯\ 
|__/ 
|    
     
'''

ascii_font_fz[ ord( 'q' ) ] = '''
|¯¯¯| 
|   | 
|___| 
   \  
'''

ascii_font_fz[ ord( 'r' ) ] = '''
|¯¯\ 
|__/ 
|  \ 
     
'''

ascii_font_fz[ ord( 's' ) ] = '''
 |¯¯) 
  \   
(__|  
      
'''

ascii_font_fz[ ord( 't' ) ] = '''
¯¯|¯¯ 
  |   
  |   
      
'''

ascii_font_fz[ ord( 'u' ) ] = '''
|   | 
|   | 
 \_/  
      
'''

ascii_font_fz[ ord( 'v' ) ] = '''
|    | 
 \  /  
  \/   
       
'''

ascii_font_fz[ ord( 'w' ) ] = '''
|   | 
| | | 
 \|/  
      
'''

ascii_font_fz[ ord( 'x' ) ] = '''
 \ / 
  ·  
 / \ 
     
'''

ascii_font_fz[ ord( 'y' ) ] = '''
\  / 
 \/  
 /   
     
'''

ascii_font_fz[ ord( 'z' ) ] = '''
¯¯/  
 /   
|__  
     
'''

ascii_font_fz[ ord( '0' ) ] = '''
    
/¯\ 
\_/ 
    
'''

ascii_font_fz[ ord( '1' ) ] = '''
   
¯| 
 | 
   
'''

ascii_font_fz[ ord( '2' ) ] = '''
    
(¯/ 
 /_ 
    
'''

ascii_font_fz[ ord( '3' ) ] = '''
    
 ¯) 
 ¯) 
¯¯  
'''

ascii_font_fz[ ord( '4' ) ] = '''
    
|_| 
  | 
    
'''

ascii_font_fz[ ord( '5' ) ] = '''
    
|¯  
 ¯) 
¯¯  
'''

ascii_font_fz[ ord( '6' ) ] = '''
    
/¯  
|¯) 
 ¯  
'''

ascii_font_fz[ ord( '7' ) ] = '''
    
¯¯/ 
 |  
    
'''

ascii_font_fz[ ord( '8' ) ] = '''
    
(¯) 
(¯) 
 ¯  
'''

ascii_font_fz[ ord( '9' ) ] = '''
    
(¯| 
 ¯| 
 ¯  
'''

ascii_font_fz[ ord( ',' ) ] = '''
   
   
   
/  
'''

ascii_font_fz[ ord( ';' ) ] = '''
   
   
·  
/  
'''

ascii_font_fz[ ord( '.' ) ] = '''
   
   
   
°  
'''


# / char
ascii_font_fz[ 47 ] = '''
  / 
 ·  
/   
    
'''

# \ char
ascii_font_fz[ 92 ] = '''
\   
 ·  
  \ 
    
'''

ascii_font_fz[ ord( '*' ) ] = '''
    
`·’ 
’ ` 
    
'''

ascii_font_fz[ ord( '-' ) ] = '''
   
 _ 
   
   
'''

ascii_font_fz[ ord( '+' ) ] = '''
    
_|_ 
 |  
    
'''

# ° char
ascii_font_fz[ 176 ] = '''
() 
   
   
   
'''

# ² char
ascii_font_fz[ 178 ] = '''
¯) 
(_ 
   
   
'''

# ³ char
ascii_font_fz[ 179 ] = '''
¯) 
_) 
   
   
'''

# # char
ascii_font_fz[ 35 ] = '''
 _/_/_ 
_/_/_  
/ /    
       
'''

ascii_font_fz[ ord( '=' ) ] = '''
    
--- 
--- 
    
'''

ascii_font_fz[ ord( '?' ) ] = '''
 ___  
(  _) 
  |   
  ·   
'''

ascii_font_fz[ ord( '!' ) ] = '''
   
 | 
 | 
 · 
'''

ascii_font_fz[ ord( '(' ) ] = '''
 / 
(  
 \ 
   
'''

ascii_font_fz[ ord( ')' ) ] = '''
\  
 ) 
/  
   
'''

ascii_font_fz[ ord( '[' ) ] = '''
|¯ 
|  
|_ 
   
'''

ascii_font_fz[ ord( ']' ) ] = '''
¯| 
 | 
_| 
   
'''

ascii_font_fz[ ord( '%' ) ] = '''
() /  
  /   
 / () 
      
'''

ascii_font_fz[ ord( '&' ) ] = '''
 (¯  
 _\/ 
(_)\ 
     
'''

ascii_font_fz[ ord( ':' ) ] = '''
   
 · 
 · 
   
'''

ascii_font_fz[ ord( '$' ) ] = '''
 ||_ 
(||  
_||) 
 ||  
'''

ascii_font_fz[ ord( '\'' ) ] = '''
| 
  
  
  
'''

ascii_font_fz[ ord( '"' ) ] = '''
|| 
   
   
   
'''

def ascii_dump_font( font ):
	tmp_theme = {
		'wordwrap': True,
		'font' : font
	}
	return ascii_txt( font['characters'], tmp_theme )

def ascii_char_font( c, font ):
	try:
		if ord( c ) in font:
			return font[ ord( c ) ]
	except:
		if c in font:
			return font[ c ]
	else:
		return None

def ascii_left_padding( lines, lid, theme ):
	if 'padding' in theme and theme['padding'][3] > 0:
		for j in range( 0, theme['padding'][3] ):
			lines[ lid ] += ' '

def ascii_newline( lines, theme ):
	
	font = theme[ 'font' ]
	if font == None:
		return
	
	line_spacing = 0
	if 'line_spacing' in theme:
		line_spacing = theme['line_spacing']
	
	for i in range( font[ 'lines' ] + line_spacing ):
		llen = len( lines )
		lines.append('')
		ascii_left_padding( lines, llen, theme )

def ascii_txt( txt, theme ):
	
	font = theme[ 'font' ]
	if font == None:
		return txt
	
	max_len = -1
	if theme[ 'wordwrap' ]:
		rows, columns = os.popen('stty size', 'r').read().split()
		max_len = int( columns )
	
	# num of line in the font:
	lines = []
	ascii_newline( lines, theme )
	
	baseline = 0
	
	padding = (0,0,0,0)
	if 'padding' in theme:
		padding = theme['padding']
		max_len -= padding[1]
	
	line_spacing = 0
	if 'line_spacing' in theme:
		line_spacing = theme['line_spacing']
	max_char_height = font[ 'lines' ] + line_spacing
	
	for j in range( 0, padding[0] ):
		llen = len( lines )
		lines.append('')		
		ascii_left_padding( lines, llen, theme )
		baseline += 1
	
	for c in txt:
		if c == '\n':
			baseline += max_char_height
			ascii_newline( lines, theme )
			continue
		res = ascii_char_font( c, font )
		if res != None:
			char_len = 0
			for l in res:
				if l == '\n' and char_len > 0:
					break
				elif l != '\n':
					char_len += 1
			if ( len( lines[ baseline ] ) + char_len ) > max_len:
				baseline += max_char_height
				ascii_newline( lines, theme )
			i = -1
			for l in res:
				if l == '\n':
					i += 1
				else:
					if i >= max_char_height:
						break
					lines[baseline + i] += l
		else:
			#print( "missing char! " + str( ord( c ) ) )
			pass
	
	for j in range( 0, padding[2] ):
		lines.append('')
		
	out = ''
	for l in lines:
		out += l + '\n'
	return out

ascii_theme = {
	'wordwrap': True,
	'padding': ( 5, 8, 5, 8 ),
	'line_spacing': 2,
	'font' : ascii_font_fz
}

print( ascii_txt( 'abcdef\nghijkl\nmnopqr\nstuvwx\nyz0123\n456789', ascii_theme ) )
print( ascii_txt( '#xylophone wagner #\n\t0.2451 * 10³ - 12² + 390° = 3 ?!; ef.g ba/ba c\\abel baba cabel', ascii_theme ) )

print( ascii_dump_font( ascii_font_fz ) )

# '¯˛¿¯|'`'' ’ "
