# Notes à propos de patch pd #

## arduino/controller.pd ##

Configuration d'une arduino UNO:

 allumage séquentiel des pins digital de 10 à 13
 récupération des entrées analogiques A0 à A5 en fonction de la pin allumée

Développé pour Amas Dryades en 2018.

## abs/ ##

Set d'abstractrons.

## list_recorder.pd ##

Enregisteur séquentiel de micro et ajout du fichier dans une liste de lecture.
Lecture prioritaire du dernier enregistrement et ensuite enclenchement d'une lecture aléatoire de la liste.

## creation_args.pd ##

Récupération des arguments de création d'un patch.

 $0: numéro unique de l'instance
 $1 - $5: arguments passés à l'instance


