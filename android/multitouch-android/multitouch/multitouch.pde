import oscP5.*;
import netP5.*;

public static final int inPort =       1200;
public static final String remoteIP =  "192.168.10.206";
public static final int remotePort =   9001;

OscP5 oscP5;
NetAddress remote;
boolean last_message_was_not_empty;

String eventresult;
String updatetouchresult;

void setup() {
  
  size( displayWidth, displayHeight );
  
  orientation( PORTRAIT );

  touchInit();
  
  updatetouchresult = "";
  eventresult = "";

  oscP5 = new OscP5(this, inPort);
  remote = new NetAddress( remoteIP, remotePort );
  last_message_was_not_empty = false;

}

void draw() {

  updatetouchresult = "";
  updateTouch();
  
  waitTouchLocked();
  touchLock();
  OscMessage touchmsg = new OscMessage( "/touch" );
  boolean send = false;
  for ( int i = 0; i < TOUCHPOINTS; i++ ) {
    TouchPoint tp = touchpoints[ i ];
    if ( tp.enabled ) {
      touchmsg.add( tp.ID );
      touchmsg.add( tp.normalized_pos.x );
      touchmsg.add( tp.normalized_pos.y );
      touchmsg.add( tp.size );
      send = true;
    }
  }
  touchUnlock();
  
  if ( send || last_message_was_not_empty ) {
    oscP5.send( touchmsg, remote );
  }
  if ( send ) {
    last_message_was_not_empty = true;
  } else {
    last_message_was_not_empty = false;
  }
  
  background( 255, 0, 0 );

  drawTouch();

  float ly = 200;
  fill( 255 );
  text( frameRate, 20, ly ); ly += 15;
  text( eventresult, 20, ly ); ly += 15;
  text( updatetouchresult, 20, ly + 200 );

}

void touchPressed( int tID, float x , float y ) {
  updatetouchresult += tID + " pressed\n";
}


void touchReleased( int tID, float x , float y ) {
  updatetouchresult += tID + " released\n";
}


void touchDragged( int tID, float x , float y ) {
  updatetouchresult += tID + " dragged\n";
}
