// see https://developer.android.com/reference/android/view/MotionEvent.html for values of each action

import android.view.MotionEvent;

int TOUCHPOINTS = 10;
TouchPoint[] touchpoints;
boolean touch_lock;

public void waitTouchLocked() {
  while( touch_lock ) {
    try {
      Thread.sleep( 1 );
    } catch ( Exception e ) {
    }
  }
  return;
}

public void touchLock() {
  touch_lock = true;
}

public void touchUnlock() {
  touch_lock = false;
}

public void touchInit() {
  touchpoints = new TouchPoint[ TOUCHPOINTS ];
  for ( int i = 0; i < TOUCHPOINTS; i++ ) {
    touchpoints[ i ] = new TouchPoint( i );
  }
  touch_lock = false;
}

public void updateTouch() {
  
  waitTouchLocked();
  touch_lock = true; 
  for ( int i = 0; i < TOUCHPOINTS; i++ ) {
    TouchPoint tp = touchpoints[ i ];
    tp.update();
    // triggering main class methods
    if ( tp.changed() ) {
      if ( tp.enabled ) {
        touchPressed( tp.ID, tp.x, tp.y );
      } else {
        touchReleased( tp.ID, tp.x, tp.y );
      }
    } else if ( tp.enabled ) {
      touchDragged( tp.ID, tp.x, tp.y );
    }
  }
  touch_lock = false;
  
}

public void drawTouch() {
  
  waitTouchLocked();
  touchLock();
  
  float ly = 20;
  for ( int i = 0; i < TOUCHPOINTS; i++ ) {    
    TouchPoint tp = touchpoints[ i ];
    noStroke();
    fill( 255 );
    if ( tp.enabled ) {
      text( tp.ID + " :: " + tp.foreignID + ", size " + tp.size + " dead in " + ( tp.aliveUntil - millis() ), 20, ly ); ly += 15;
      fill( 255 );
    } else {
      text( tp.ID + " :: " + tp.foreignID + " dead", 20, ly ); ly += 15;
      fill( 255, 100 );
    }
    line( tp.x, tp.y - 50, tp.x, tp.y + 50 );
    line( tp.x - 50, tp.y, tp.x + 50, tp.y );
    line( tp.x, tp.y, tp.x + tp.dir.x, tp.y + tp.dir.y );    
    if ( tp.enabled ) {
      fill( 255, 100 );
      ellipse( tp.x, tp.y, tp.size * 250, tp.size * 250 );
      ellipse( tp.last.x, tp.last.y, tp.last_size * 250, tp.last_size * 250 );
    }
  }
  
  touchUnlock();

}

public boolean surfaceTouchEvent( MotionEvent event ) {
  
  waitTouchLocked();
  touchLock();
  
  eventresult = "";
  
  // creation of a table of correspondance between position of the touch
  // and distance to last knwon positions
  int pts = event.getPointerCount();
  
  // storage of distances
  float[][] touch_list = new float[ pts ][ 3 ];
  for (int p = 0; p < pts; p++) {
    touch_list[ p ] = new float[ 3 ];
    touch_list[ p ][ 0 ] = event.getX( p );
    touch_list[ p ][ 1 ] = event.getY( p );
    touch_list[ p ][ 2 ] = event.getSize( p );
  }
  
  // collecting distances
  float[][] distance_map = new float[ TOUCHPOINTS ][ pts ];
  for ( int i = 0; i < TOUCHPOINTS; i++ ) {
    TouchPoint tp = touchpoints[ i ];
    tp.foreignID = -1;
    distance_map[ i ] = new float[ pts ];
    for ( int p = 0; p < pts; p++ ) {
      distance_map[ i ][ p ] = dist( tp.x, tp.y, touch_list[ p ][ 0 ], touch_list[ p ][ 1 ] );
    }
  }
  
  // searching the lowest distance per eventID
  for ( int p = 0; p < pts; p++ ) {
    TouchPoint tp = null;
    float smallest = 0;
    for ( int i = 0; i < TOUCHPOINTS; i++ ) {
      TouchPoint newtp = touchpoints[ i ];
      if ( newtp.foreignID == -1 ) {
        if ( tp == null ) {
          smallest = distance_map[ i ][ p ];
          tp = touchpoints[ i ];
        } else if ( smallest > distance_map[ i ][ p ] ) {
          smallest = distance_map[ i ][ p ];
          tp = touchpoints[ i ];
        }
      }
    }
    // ok, FOUND!
    if ( tp != null ) {
      tp.foreignID = p;
      tp.move( touch_list[ p ][ 0 ], touch_list[ p ][ 1 ], touch_list[ p ][ 2 ] );
      eventresult += "point " + tp.ID + " remapped to " + tp.foreignID +" \n";
    }
    
  }
  
  if ( event.getActionMasked() == 1 ) { // ACTION_UP
    int pid = event.getActionIndex();
    for ( int i = 0; i < TOUCHPOINTS; i++ ) {
      TouchPoint newtp = touchpoints[ i ];
      if ( newtp.foreignID == pid ) {
        newtp.foreignID = -1;
        break;
      }
    }
  }
  
  eventresult += "at frame " + frameCount + "\n";

  for ( int i = 0; i < TOUCHPOINTS; i++ ) {
    touchpoints[ i ].update();
  }
  
  touchUnlock();
  
  return super.surfaceTouchEvent( event );
  
}
