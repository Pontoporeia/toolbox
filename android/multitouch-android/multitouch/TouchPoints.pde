public static final int DELAY2KILL = 500;

class TouchPoint extends PVector {

  int ID;
  int foreignID;
  PVector last;
  boolean was_enabled;
  boolean enabled;
  PVector dir;
  float speed;
  float size;
  float last_size;
  int aliveUntil;
  PVector normalized_pos;
  PVector normalized_dir;
  
  public TouchPoint( int uniqueID ) {
    super();
    ID = uniqueID;
    foreignID = -1;
    last = null;
    dir = new PVector();
    was_enabled = false;
    enabled = false;
    speed = 0;
    size = 0;
    last_size = 0;
    aliveUntil = 0;
    normalized_pos = new PVector();
    normalized_dir = new PVector();
  }
  
  public boolean changed() {
    boolean changed = false;
    if ( was_enabled != enabled ) {
      changed = true;
    }
    was_enabled = enabled;
    return changed;  
  }
  
  public void update() {    
    if (
      ( enabled && foreignID == -1 ) || 
      ( enabled && aliveUntil - millis() < 0 )
      ) {
      size = 0;
      last_size = 0;
      last = null;
      enabled = false;
    }
    if ( !enabled && aliveUntil - millis() < 0 && x != 0 && y != 0 ) {
      set( 0,0,0 );
    }
    if ( enabled ) {
      normalized_pos.set( ( PVector ) this );
      normalized_pos.x /= displayWidth;
      normalized_pos.y /= displayHeight;
      normalized_dir.set( dir );
      normalized_dir.x /= displayWidth;
      normalized_dir.y /= displayHeight;
    }
    speed = dir.mag();
  }
  
  public void move( float x, float y, float size ) {
    if ( last == null ) {
      last = new PVector();
    }
    last.set( ( PVector ) this );
    set( x, y, 0 );
    last_size = size;
    this.size = size;
    if ( enabled ) {
      dir.set( ( PVector ) this );
      dir.x -= last.x;
      dir.y -= last.y;
    } else  {
      dir.set( 0,0,0 );
    }
    enabled = true;
    aliveUntil = millis() + DELAY2KILL;
  }

}
