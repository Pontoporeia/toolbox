import oscP5.*;
import netP5.*;

public static final int inPort = 9001;

OscP5 oscP5;

public boolean lock;
ArrayList< float[] > touchpoints;

void setup() {

  size( 800, 800 );

  oscP5 = new OscP5(this, inPort);

  lock = false;
  touchpoints = new ArrayList< float[] >();

}

void draw() {

  background( 250, 50, 0 );
  noStroke();
  lock = true;
  for ( int i = 0; i < touchpoints.size(); i++ ) {
    float[] pt = touchpoints.get( i );
    fill( 255 );
    ellipse( pt[ 1 ], pt[ 2 ], pt[ 3 ], pt[ 3 ] );
    fill( 0 );
    text( "ID: " + int( pt[ 0 ] ), pt[ 1 ], pt[ 2 ] );
  }
  lock = false;

}

void oscEvent( OscMessage msg ) {
  
  while( lock ) {
    try {
      Thread.sleep( 1 );
    } catch ( Exception e ) {}
  }
  
  if( msg.checkAddrPattern("/touch") == true ) {
    touchpoints.clear();
    Object[] values = msg.arguments();
    for ( int i = 0; i < values.length; i += 4 ) {
      if ( i + 3 >= values.length ) {
        return;
      }
      touchpoints.add( new float[] { 
        Float.parseFloat( values[ i ].toString() ), 
        Float.parseFloat( values[ i + 1 ].toString() ) * width, 
        Float.parseFloat( values[ i + 2 ].toString() ) * height, 
        Float.parseFloat( values[ i + 3 ].toString() ) * width * 0.2f } );
    }
  }
  
}

