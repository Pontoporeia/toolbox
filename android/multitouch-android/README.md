# README #

## Multitouch android ##

Processing.org sketches. The sketch called "multitouch" is for the table, the "multitouchReceive" for a computer.
Code based on an example of [jesses.co.tt](http://stackoverflow.com/questions/17166522/can-processing-handle-multi-touch)

## Setup android ##

* Download and install android processing from android market
* Download and install addon oscP5 from APDE (android processing) : "(top menu) Tools > Import Library > Manage Libraries > (top menu) Get libraries".
* Place the skecth "multitouch" in the skecth folder
* Open it and adapt IP & port of the remote client (top of the file)
* Run and install


## Setup computer ##

* Download and install [processing](https://processing.org/download/)
* Download and install addon oscP5
* Place the skecth "multitouchReceive" in the skecth folder
* Open project and run

## Integration ##
To use touchPoints in your code, copy paste TouchMethods.pde & TouchPoints.pde in your skecth folder. In main file, you'll have to do several things.

### setup() method ###
* Call "touchInit()", wherever you want, and that's it

### draw() method ###
* Before doing anything else, call "updateTouch()". This method will manage all touchpoints and trigger cutsom pressed, release and dragged methods (see below).
* If you want to access the points, to send OSC messages for instance, you'll have to call "touchLock()" before looping over touchpoints. Once done, DO NOT FORGET TO CALL "touchUnlock()"! If you don't do that, nothing will happen anymore.
* If you want to draw the points, call "drawTouch()"

### additional methods ###
To easily trigger actions, add these three methods at the end of the main class:

* void touchPressed( int tID, float x , float y ) { }
* void touchReleased( int tID, float x , float y ) { }
* void touchDragged( int tID, float x , float y ) { }

"tID" is the number of the finger, x & y are its position.

### about lock and unlock ###
The method "surfaceTouchEvent()" is NOT synchronised with "draw()" method of your sketch, and processing IDE doesn't support "synchronised" keyword (this would have solved the problem much more easily). This means that touchpoints may changed when you using them in "draw()". This usually ends up in an application crash, caused by a "concurrent access".
To avoid this, an easy way is to use a mutex, "touch_lock" in this code. When a method wants to access touchpoints objects, it first waits for "touch_lock" to be "false". Once it becomes "false", it locks it while it reads or changes the data. When job is done, it's crucial to unlock the mutex to allow other methods to access touchpoints data.
A short explanation: [http://www.webopedia.com/TERM/M/mutex.html](http://www.webopedia.com/TERM/M/mutex.html)

## Who do I talk to? ##

Created and managed by [frankie zafe](http://www.frankiezafe.org)
