
void setup() {
  
  size( 600, 600 );
  noLoop();
  
}

void draw() {

  background( 0 );
  
  int y = 50;
  
  int i = 20;
  float f = 20;
  
  text( "nombre entier => " + i, 30, y ); y += 20;
  text( "nombre à virgule => " + i, 30, y ); y += 20;

  i /= 3;
  f /= 3;
  
  text( "nombre entier divisé par 3 => " + i, 30, y ); y += 20;
  text( "nombre à virgule divisé par 3 => " + f, 30, y ); y += 20;
  
  boolean b = true;
  
  text( "booléen => " + b, 30, y ); y += 20;
  
  b = !b;
  
  text( "inverse du booléen => " + b, 30, y ); y += 20;
  
  i = 20;
  f = 20;
  
  b = i == f;
  
  text( "égalité entre entier (" + i + ") et no  mbre à virgule (" + f + ")  => " + b, 30, y ); y += 20;
  
  i /= 3;
  f /= 3;
  
  b = i == f;
  
  text( "égalité entre le tiers des mêmes nombres  => " + b, 30, y ); y += 20;
  
  char c = 'A';
  
  text( "caractère => " + c, 30, y ); y += 20;
  
  i = c;
  
  text( "valeur entière du caractère => " + i, 30, y ); y += 20;
  
  c += 1;
  
  text( "caractère incrémenté de 1 => " + c, 30, y ); y += 20;
  
  String chaine_de_caracteres = "booya";
  
  text( "chaîne de caractères => " + chaine_de_caracteres, 30, y ); y += 20;
  
  text( "caractères de la chaîne: ", 30, y ); y += 20;
  text( "[0] => '" + chaine_de_caracteres.charAt(0) + "', équivalent entier => " + int( chaine_de_caracteres.charAt(0) ), 30, y ); y += 20;
  text( "[1] => '" + chaine_de_caracteres.charAt(1) + "', équivalent entier => " + int( chaine_de_caracteres.charAt(1) ), 30, y ); y += 20;
  text( "[2] => '" + chaine_de_caracteres.charAt(2) + "', équivalent entier => " + int( chaine_de_caracteres.charAt(2) ), 30, y ); y += 20;
  text( "[3] => '" + chaine_de_caracteres.charAt(3) + "', équivalent entier => " + int( chaine_de_caracteres.charAt(3) ), 30, y ); y += 20;
  text( "[4] => '" + chaine_de_caracteres.charAt(4) + "', équivalent entier => " + int( chaine_de_caracteres.charAt(4) ), 30, y ); y += 20;
  
} 