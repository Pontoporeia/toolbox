// la boucle est une manière de répéter une opération un nomnbre de fois défini

void setup() {
  
  size( 600, 600 );
  noStroke();
  noLoop();
  
}

void draw() {

  background( 0 );

  for ( int i = 0; i < 10; i++ ) {
      fill( 255 );
      rect( 150, 25 + i * 60, 5, 5 );
      fill( 0, 255, 0 );
      rect( 250, 25 + i * 60, 5, 5 );
      fill( 255, 0, 0 );
      rect( 350, 25 + i * 60, 5, 5 );
      if ( i >= 5 ) {
        fill( 0, 0, 255 );
        rect( 450, 25 + i * 60, 5, 5 );
      }
  }

}