// la boucle est une manière de répéter une opération un nomnbre de fois défini

void setup() {
  
  size( 600, 600 );
  noStroke();
  noLoop();
  
}

void draw() {

  background( 0 );
  
  fill( 255 );
  rect( 150, 25, 5, 5 );
  rect( 150, 25 + 60, 5, 5 );
  rect( 150, 25 + 120, 5, 5 );
  rect( 150, 25 + 180, 5, 5 );
  rect( 150, 25 + 240, 5, 5 );
  rect( 150, 25 + 300, 5, 5 );
  rect( 150, 25 + 360, 5, 5 );
  rect( 150, 25 + 420, 5, 5 );
  rect( 150, 25 + 480, 5, 5 );
  rect( 150, 25 + 540, 5, 5 );

  fill( 0, 255, 0 );
  for ( int i = 0; i < 600; i += 60 ) {
      rect( 250, 25 + i, 5, 5 );
  }

  fill( 255, 0, 0 );
  for ( int i = 0; i < 10; i++ ) {
      rect( 350, 25 + i * 60, 5, 5 );
  }

  fill( 0, 0, 255 );
  for ( int i = 5; i < 10; i++ ) {
      rect( 450, 25 + i * 60, 5, 5 );
  }

}