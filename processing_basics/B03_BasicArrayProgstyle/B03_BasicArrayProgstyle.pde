int[] ints;
float[] floats;
String[] strings;

int count;
int current;

void setup() {

  size( 600, 600 );

  count = 22;
  ints = new int[ count ];
  floats = new float[ count ];
  strings = new String[ count ];
  for ( int i = 0; i < count; i++ ) {
    ints[i] = (int) random( 0, 1000 );
    floats[i] = random( 0, 1 );
    strings[i] = generation_de_chaine();
  }

  current = 0;
}

void draw() {

  background( 0 );
  fill( 255 );
  text( "nombre à afficher: " + current, 30, 50 );
  text( "nombre entier à la position " + current + " >> " + ints[ current ], 30, 70 );
  text( "nombre à virgule à la position " + current + " >> " + floats[ current ], 30, 90 );
  text( "chaîne de caractères à virgule à la position " + current + " >> " + strings[ current ], 30, 110 );

  text( "nombres entiers", 30, 140 );
  text( "nombres à virgule", 150, 140 );
  text( "chaînes de caractères", 300, 140 );
  for ( int i = 0; i < count; i++ ) {
    if ( i == current ) {
      fill( 255, 0, 0 );
    } else {
      fill( 190 );
    }
    text( "["+ i + "]: " + ints[i], 30, 160 + i *20 );
    text( "["+ i + "]: " + floats[i], 150, 160 + i *20 );
    text( "["+ i + "]: " + strings[i], 300, 160 + i *20 );
  }
}

void keyPressed() {

  current += 1;
  if ( current >= count ) {
    current = 0;
  }
}

String generation_de_chaine() {

  int charnum = (int) random( 5, 30 );
  String out = "";
  for ( int i = 0; i < charnum; i++ ) {
    out += char( (int) random( 49, 120 ) );
  }
  return out;
}