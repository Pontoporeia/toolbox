
void setup() {
  
  size( 600, 600 );
  noLoop();
  
}

void draw() {

  background( 0 );
  
  int y = 50;
  
  int a = 20;
  int b = 20;
  boolean comparaison;
  
  text( "a => " + a + " et b => " + b, 30, y ); y += 40;
  
  // égalité
  comparaison = a == b;
  text( "" + a + " == " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // inégalité
  comparaison = a != b;
  text( "" + a + " != " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // plus grand ou égal
  comparaison = a >= b;
  text( "" + a + " >= " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // plus petit ou égal
  comparaison = a <= b;
  text( "" + a + " <= " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // plus grand
  comparaison = a > b;
  text( "" + a + " > " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // plus petit
  comparaison = a < b;
  text( "" + a + " < " + b + "? => " + comparaison, 30, y ); y += 20;
  
  y += 20;
  b = 21;
  text( "b => " + b, 30, y ); y += 40;
  
  // égalité
  comparaison = a == b;
  text( "" + a + " == " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // inégalité
  comparaison = a != b;
  text( "" + a + " != " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // plus grand ou égal
  comparaison = a >= b;
  text( "" + a + " >= " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // plus petit ou égal
  comparaison = a <= b;
  text( "" + a + " <= " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // plus grand
  comparaison = a > b;
  text( "" + a + " > " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // plus petit
  comparaison = a < b;
  text( "" + a + " < " + b + "? => " + comparaison, 30, y ); y += 20;
  
  // combinaison de tests, deuxième colonne
  y = 50;
  
  int c = b;
  text( "a => " + a + ", b => " + b + " et c => " + c, 300, y ); y += 40;
  
  // tests d'égailté entre a et b et b et c
  boolean ab = a == b;
  boolean bc = b == c;
  text( "" + a + " == " + b + "? => " + ab, 300, y ); y += 20;
  text( "" + b + " == " + c + "? => " + bc, 300, y ); y += 40;
  
  // AND
  comparaison = a == b && b == c;
  text( "" + ab + " && " + bc + "? => " + comparaison, 300, y ); y += 20;
  text( "(" + a + " == " + b + ") && (" + b + " == " + c + ")? => " + comparaison, 300, y ); y += 40;
  
  // OR
  comparaison = a == b || b == c;
  text( "" + ab + " || " + bc + "? => " + comparaison, 300, y ); y += 20;
  text( "(" + a + " == " + b + ") || (" + b + " == " + c + ")? => " + comparaison, 300, y ); y += 40;
  
  
}