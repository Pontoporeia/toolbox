boolean c_compute;
char c_positif;
char c_negatif;
char c_limit_positif;
char c_limit_negatif;
int  c_pow2;

int   i_positif;
int   i_negatif;
int   i_positif_limite;
int   i_negatif_limite;
int   i_pow2;

float f_positif;
float f_negatif;
float f_positif_limite;
float f_negatif_limite;
int   f_pow2;

double d_positif;
double d_negatif;
double d_positif_limite;
double d_negatif_limite;
int   d_pow2;

void setup() {
  
  size( 600, 700 );
  
  c_compute = true;
  c_positif = 0;
  c_negatif = 0;
  c_limit_positif = 0;
  c_limit_negatif = 0;
  
  i_positif = 0;
  i_negatif = 0;
  i_positif_limite = 0;
  i_negatif_limite = 0;
  
  f_positif = 0;
  f_negatif = 0;
  f_positif_limite = 0;
  f_negatif_limite = 0;
  
  d_positif = 0;
  d_negatif = 0;
  d_positif_limite = 0;
  d_negatif_limite = 0;
  
  frameRate(5);
  
}

void draw() {

  background( 0 );
  
  int y = 50;

  text( "nombre d'images calculées => " + frameCount, 30, y ); y += 20;

  if ( c_compute ) {
    c_positif = (char) pow( 2, frameCount );
    c_negatif = (char) -pow( 2, frameCount );
    if ( c_limit_positif < c_positif ) {
      c_limit_positif = c_positif;
      c_pow2 = frameCount;
    } else {
      c_compute = false;
    }
    if ( c_limit_negatif > c_negatif ) {
      c_limit_negatif = c_negatif;
    }
  }
  
  int icp = c_positif;
  int icn = c_negatif;
  
  text( "caractère positif => " + c_positif, 30, y ); y += 20;
  text( "équivalent en entier => " + icp, 30, y ); y += 20;
  text( "bits => " + showBits( c_positif ), 30, y ); y += 20;
  text( "caractère négatif => " + c_negatif, 30, y ); y += 20;
  text( "équivalent en entier => " + icn, 30, y ); y += 20;
  text( "bits => " + showBits( c_negatif ), 30, y ); y += 20;
  text( "limite caractère => " + int( c_limit_positif ), 30, y ); y += 20;
  text( "puissance de 2 => " + c_pow2, 30, y ); y += 40;
  
  i_positif = (int) pow( 2, frameCount );
  if ( i_positif > i_positif_limite ) {
    i_positif_limite = i_positif;
    i_pow2 = frameCount;
  }
  text( "entier positif => " + i_positif, 30, y ); y += 20;
  text( "bits => " + showBits( i_positif ), 30, y ); y += 20;
  text( "limite entier positif => " + i_positif_limite, 30, y ); y += 20;
  
  i_negatif = (int) -pow( 2, frameCount );
  if ( i_negatif < i_negatif_limite ) {
    i_negatif_limite = i_negatif;
  }
  text( "entier négatif => " + i_negatif, 30, y ); y += 20;
  text( "bits => " + showBits( i_negatif ), 30, y ); y += 20;
  text( "limite entier négatif => " + i_negatif_limite, 30, y ); y += 20;
  text( "puissance de 2 => " + i_pow2, 30, y ); y += 40;
  
  f_positif = pow( 2.f, frameCount );
  if ( f_positif != Float.POSITIVE_INFINITY && f_positif > f_positif_limite ) {
    f_positif_limite = f_positif;
    f_pow2 = frameCount;
  }
  text( "nombre à virugule positif => " + f_positif, 30, y ); y += 20;
  text( "limite nombre à virugule positif => " + f_positif_limite, 30, y ); y += 20;
  
  f_negatif = -pow( 2.f, frameCount );
  if ( f_negatif != Float.NEGATIVE_INFINITY && f_negatif < f_negatif_limite ) {
    f_negatif_limite = f_negatif;
  }
  text( "nombre à virugule négatif => " + f_negatif, 30, y ); y += 20;
  text( "limite nombre à virugule négatif => " + f_negatif_limite, 30, y ); y += 20;
  text( "puissance de 2 => " + f_pow2, 30, y ); y += 40;
  
  d_positif = java.lang.Math.pow( 2.d, frameCount );
  if ( d_positif != Float.POSITIVE_INFINITY && d_positif > d_positif_limite ) {
    d_positif_limite = d_positif;
    d_pow2 = frameCount;
  }
  text( "double positif => " + d_positif, 30, y ); y += 20;
  text( "limite double positif => " + d_positif_limite, 30, y ); y += 20;
  
  d_negatif = -java.lang.Math.pow( 2.d, frameCount );
  if ( d_negatif != Float.NEGATIVE_INFINITY && d_negatif < d_negatif_limite ) {
    d_negatif_limite = d_negatif;
  }
  text( "double négatif => " + d_negatif, 30, y ); y += 20;
  text( "limite double négatif => " + d_negatif_limite, 30, y ); y += 20;
  text( "puissance de 2 => " + d_pow2, 30, y ); y += 40;

}

String showBits( char c ) {
  String out = "";
  for ( int i = 0; i < 16; i++ ) {
    out += ( c >> i ) & 1;
  }  
  return new StringBuilder(out).reverse().toString();
}

String showBits( int vi ) {
  String out = "";
  for ( int i = 0; i < 32; i++ ) {
    out += ( vi >> i ) & 1;
  }  
  return new StringBuilder(out).reverse().toString();
}