// Array est le terme anglais désignant un "TABLEAU", ou encore une "LISTE" de valeurs quelconque

int[] nombres_entiers;
float[] nombres_a_virgule;
String[] chaines_de_caracteres;

int longueur_des_tableaux;
int position_a_afficher;

void setup() {

  size( 600, 600 );
  
  longueur_des_tableaux = 22;
  nombres_entiers = new int[ longueur_des_tableaux ];
  nombres_a_virgule = new float[ longueur_des_tableaux ];
  chaines_de_caracteres = new String[ longueur_des_tableaux ];
  for( int i = 0; i < longueur_des_tableaux; i++ ) {
    nombres_entiers[i] = (int) random( 0, 1000 );
    nombres_a_virgule[i] = random( 0, 1 );
    chaines_de_caracteres[i] = generation_de_chaine();
  }
  
  position_a_afficher = 0;
  
}

void draw() {

  background( 0 );
  fill( 255 );
  text( "nombre à afficher: " + position_a_afficher, 30, 50 );
  text( "nombre entier à la position " + position_a_afficher + " >> " + nombres_entiers[ position_a_afficher ], 30, 70 );
  text( "nombre à virgule à la position " + position_a_afficher + " >> " + nombres_a_virgule[ position_a_afficher ], 30, 90 );
  text( "chaîne de caractères à virgule à la position " + position_a_afficher + " >> " + chaines_de_caracteres[ position_a_afficher ], 30, 110 );
  
  text( "nombres entiers", 30, 140 );
  text( "nombres à virgule", 150, 140 );
  text( "chaînes de caractères", 300, 140 );
  for ( int i = 0; i < longueur_des_tableaux; i++ ) {
    if ( i == position_a_afficher ) {
      fill( 255, 0, 0 );
    } else {
      fill( 190 );
    }
    text( "["+ i + "]: " + nombres_entiers[i], 30, 160 + i *20 );
    text( "["+ i + "]: " + nombres_a_virgule[i], 150, 160 + i *20 );
    text( "["+ i + "]: " + chaines_de_caracteres[i], 300, 160 + i *20 );
  }
  
}

void keyPressed() {

  position_a_afficher += 1;
  if ( position_a_afficher >= longueur_des_tableaux ) {
    position_a_afficher = 0;
  }
  
}

String generation_de_chaine() {

  int caracteres = (int) random( 5, 30 );
  String out = "";
  for( int i = 0; i < caracteres; i++ ) {
    out += char( (int) random( 49, 120 ) );
  }
  return out;

}