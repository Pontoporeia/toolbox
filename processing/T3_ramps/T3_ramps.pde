int last_time;
int delta_time;
float delta_seconds;

Pulser[] pulsers;
private int[][] pulser_colors;

int defx;
int defy;

int base1 = 300;
float mult1 = 14;
int mod1 = 39;
float speed1 = 10;

int base2 = 50;
float mult2 = 27;
int mod2 = 15;
float speed2 = 100;

void setup () {

  last_time = 0;
  delta_time = 0;
  delta_seconds = 0;

  size( 600, 600 );

  pulser_colors = new int[5][3];
  pulser_colors[0] = new int[] { 255, 0, 0 };
  pulser_colors[1] = new int[] { 0, 255, 255 };
  pulser_colors[2] = new int[] { 200, 10, 180 };
  pulser_colors[3] = new int[] { 136, 200, 136 };
  pulser_colors[4] = new int[] { 255, 255, 0 };

  int grid = 199;
  float cellsizex = width * 1.f / grid;
  float cellsizey = height * 1.f / grid;
  defx = int( width / cellsizex );
  defy = int( height / cellsizey );
  pulsers = new Pulser[defx*defy];
  int i = 0;
  for ( int y = 0; y < defy; ++y ) {
    for ( int x = 0; x < defx; ++x ) {
      pulsers[i] = new Pulser( x * cellsizex, y * cellsizey, cellsizex, cellsizey );
      pulsers[i].set_colors( pulser_colors );
      int[] vs = pulser_values( i,x,y );
      pulsers[i].configure( i * 203 + vs[0], vs[1], vs[2] );
      ++i;
    }
  }
}

void draw() {

  // measuring elapsed time since last frame, in milliseconds
  int now = millis();
  int delta_time = 0;
  if ( last_time != 0 ) {
    delta_time = now - last_time;
  }
  delta_seconds = delta_time * 0.001;
  last_time = now;
  
  //mult1 += delta_seconds;
  //base1 = 300 + (base1 + delta_time) % 100;
  mod1 += 1;
  //mult2 += delta_seconds;

  background( 0 );

  // forcing delta_time @ 30 fps
  //delta_time = 33;

  for ( int i = 0; i < pulsers.length; ++i ) {
    pulsers[i].update( delta_time );
    pulsers[i].draw();
  }

  int i = 0;
  for ( int y = 0; y < defy; ++y ) {
    for ( int x = 0; x < defx; ++x ) {
      int[] vs = pulser_values( i,x,y );
      pulsers[i].modify( vs[0], vs[1], vs[2] );
      ++i;
    }
  }
  
}

int[] pulser_values( int i, int x, int y ) {
  return new int[] {
    0,
    //base1 + ( ( i % ( y + 1 ) ) * int( mult1 ) % mod1 ) * int( speed1 ),
    base1 + ( x  * int( mult1 ) % mod1 ) * int( speed1 ),
    base2 + ( y * int( mult2 ) % mod2 ) * int( speed2 )
  };
}