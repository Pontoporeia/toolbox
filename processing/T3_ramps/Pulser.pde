class Pulser {

  public float[] pos;
  public float[] size;

  public int start_delay;
  public int frequency;
  private int current_millis;
  public boolean pulse;
  
  private int[][] colors;
  private float colorpos;
  public int color_frequency;
  private int current_color_millis;
  
  private int init_color;
  private int current_color;

  Pulser(float x, float y, float w, float h) {
    pos = new float[] {x, y};
    size = new float[] {w, h};
    configure( 0, 0 );
    colorpos = 0;
    colors = null;
  }

  public void configure( int start, int freq ) {
    configure( start, freq, 1000 );
    current_color_millis = 0;
  }
  
  public void configure( int start, int freq, int color_freq ) {
    start_delay = 0;
    frequency = freq;
    current_millis = start;
    color_frequency = color_freq;
    current_color_millis = start;
    pulse = false;
  }
  
  public void modify( int delay, int freq, int color_freq ) {
    current_millis += delay;
    current_color_millis += delay;
    frequency = freq;
    color_frequency = color_freq;
  }
  
  public void set_colors( int[][] cs ) {
    colors = cs;
    init_color = render_color();
    current_color = init_color;
  }

  public boolean update( int delta_time ) {
    
    if ( start_delay > 0 ) {
      start_delay -= delta_time;
      if ( start_delay < 0 ) {
        current_millis = -start_delay;
        current_color_millis = -start_delay;
        start_delay = 0;
      } else {
        return false;
      }
    } else {
      current_millis += delta_time;
      if ( current_millis >= frequency * 1 ) {
        pulse = true;
      } else {
        pulse = false;
      }
      current_millis %= frequency;
    }
    
    current_color_millis += delta_time;
    
    current_color = render_color();
    
    return current_color == init_color;
    
  }
  
  int get_mills() { return current_millis; }
  int get_color_mills() { return current_color_millis; }
  
  int render_color() {
    
    current_color_millis %= color_frequency;
    colorpos = ( current_color_millis * 1.f / color_frequency ) * colors.length;
    while ( colorpos > colors.length ) {
      colorpos -= colors.length;
    }
    
    float c = ( current_millis % 510 ) / 255.f;
    if ( c > 1 ) {
      c = 1 - ( c - 1 );
    }
    int lower_stop = int( colorpos );
    while ( lower_stop > colors.length - 1 ) {
      lower_stop = 0;
    }
    int upper_stop = lower_stop + 1;
    while ( upper_stop > colors.length - 1 ) {
      upper_stop = 0;
    }
    float upper_dist = colorpos - lower_stop;
    float lower_dist = 1 - upper_dist;
    int[] rgb = new int[]{0,0,0};
    for( int i = 0; i < 3; ++i ) {
      rgb[i] = int(
        colors[lower_stop][i] * lower_dist +
        colors[upper_stop][i] * upper_dist
      );
    }
    return color( c * rgb[0], c * rgb[1], c * rgb[2] );
  }

  void draw() {
    //if ( !pulse ) return;
    if ( start_delay > 0 ) return;
    noStroke();
    fill( current_color );
    rect( pos[0], pos[1], size[0], size[1] );
    
    // debug
    //fill(255);
    //text( current_millis, pos[0] + 5, pos[1] + 20 );
    //text( current_color_millis, pos[0] + 5, pos[1] + 35 );
    
  }
}