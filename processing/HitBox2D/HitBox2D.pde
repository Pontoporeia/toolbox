float[] rectangle;

void setup() {
  
  size( 600, 600 );
  //                         x  y    w    h
  //                         0    1   2    3
  rectangle = new float[] { 150, 60, 100, 200 };
  

}

void draw() {
  
  rectangle[0] = 50 + frameCount % 100;
  
  int mx = mouseX;
  int my = mouseY;
  
  boolean hit = false;
  if ( 
    mx >= rectangle[0] && my >= rectangle[1] && // après l'origine?
    mx <= rectangle[0] + rectangle[2] && my <= rectangle[1] + rectangle[3]
    ) {
    hit = true;
  }
  
  background( 0 );
  
  stroke( 255,0,0 );
  fill( 255 );
  if ( hit ) {
    fill( 255,0,0 );
  }
  rect( rectangle[0], rectangle[1], rectangle[2], rectangle[3] );
    
    fill( 0, 255, 0 );
    ellipse( mx, my, 20, 20 );
    
}