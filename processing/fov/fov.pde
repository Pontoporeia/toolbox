// script permettant de trouver l'angle d'un point en fonction d'un point de vue et d'une ouverture
// voir "inna" pour avoir l'angle en RADIAN
// fovlimit décrit un angle de sécurité à l'intérieur du champ de vision
// quand le point se rapproche dangereusement du bord, "ratio" passe de 0 à 1 ( limite atteinte ) et >1 si en dehors du champ


float fov = 58.5f / 180.f * PI;
float fovlimit  = 5.f / 180.f * PI;
PVector cam;

void setup() {

  size( 600,600 );
  cam = new PVector( 10, 300 );
  println( fovlimit );
  
}

void draw() {

  background( 5 );
  
  strokeWeight( 4 );
  
  // point of view
  stroke( 255 );
  point( cam.x, cam.y );
  strokeWeight( 1 );
  
  stroke( 0, 255, 0 );
  line( cam.x, cam.y, cam.x + cos( -fov * 0.5f ) * 500, cam.y + sin( -fov * 0.5f ) * 500 );
  stroke( 255, 0, 0, 100 );
  line( cam.x, cam.y, cam.x + cos( ( -fov * 0.5f ) + fovlimit ) * 500, cam.y + sin( ( -fov * 0.5f ) + fovlimit ) * 500 );
  stroke( 0, 0, 255 );
  line( cam.x, cam.y, cam.x + cos( fov * 0.5f ) * 500, cam.y + sin( fov * 0.5f ) * 500 );
  stroke( 255, 0, 0, 100 );
  line( cam.x, cam.y, cam.x + cos( ( fov * 0.5f ) - fovlimit ) * 500, cam.y + sin( ( fov * 0.5f ) - fovlimit ) * 500 );
  
  float dx = mouseX - cam.x;
  float dy = mouseY - cam.y;
  float hypothenuse = sqrt( dx * dx + dy * dy );
  float inna = atan2( dy, dx );
  float ratio = 1;
  boolean inside = true;
  if ( 
    ( inna < 0 && ( inna - ( -fov * 0.5f ) < fovlimit ) ) ||
    ( inna > 0 && ( ( fov * 0.5f ) - inna < fovlimit ) )
    ) {
      if ( inna < 0 ) {
        ratio = ( inna  + fov * 0.5f ) / fovlimit;
      }
      if ( inna > 0 ) {
        ratio = ( fov * 0.5f - inna ) / fovlimit;
      }
  }
  
//  if ( ratio > 1 ) {
//    inside = false;
//  }
  
  stroke( 255 );
  text( "distance: " + dx + ", " + dy + "\nhypothenuse: " + hypothenuse + "\nangle: " + inna + "\nratio: " + ratio, 10, 25 );
  
  stroke( 255, 100 );
  line( cam.x, cam.y, cam.x + cos( inna ) * hypothenuse, cam.y + sin( inna ) * hypothenuse );
  line( cam.x, cam.y, cam.x + dx, cam.y );
  line( cam.x + dx, cam.y, cam.x + dx, cam.y + dy );
  
  if ( inside ) {
    strokeWeight( 20 );
    stroke( 255 * ratio, 255 * ( 1 - ratio ), 0 );
    point( mouseX, mouseY ); 
  }
  
}
