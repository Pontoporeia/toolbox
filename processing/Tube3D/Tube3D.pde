/* An infinite ride in a twisting hallway.
 * Structure of the sketch:
 * - This tab contains the instanciation of all the objects
 * - "Plane" contains the class Plane, assuring the rendering of hallway floors and walls
 * - "RT" contains method "render_tex()" that update the hallway texture
 * - "Tube" contains the class Tube, containing the vertices and the planes to render
*/

Tube t;
PGraphics tex;
PGraphics tex_fixed;

void setup() {

  size( 1280, 720, P3D );
  
  // creation of tube, with 30 sections
  t = new Tube( 30 );
  
  // adapting dimensions to fit the screen
  t.set_dimension( new PVector( width, height, 20000 ) );
  
  // textures instanciation
  // gradient texture
  tex_fixed = createGraphics( 512, 512, P3D );
  tex_fixed.beginDraw();
  tex_fixed.loadPixels();
  for( int y = 0; y < tex_fixed.height; ++y ) {
  for( int x = 0; x < tex_fixed.width; ++x ) {
    tex_fixed.pixels[ x + y * tex_fixed.width ] = color(
      x * 0.5,
      y * 0.5,
      0
      );
  }
  }
  tex_fixed.updatePixels();
  tex_fixed.endDraw();
  
  // scrolling texture, mapped on tube
  tex = createGraphics( 512, 512, P3D );
  tex.beginDraw();
  tex.image( tex_fixed, 0,0 );
  tex.endDraw();
  
  // https://processing.org/reference/textureMode_.html
  textureMode(NORMAL);
  
}

void draw() {
  
  // updating the texture
  render_tex();
  
  // modification of the tube
  //t.rts( 0, new PVector(0,0,0), new PVector(1,1,1) );
  t.rts( 
    ( mouseX - width * 0.5 ) * 0.0003,   // ROTATION
    new PVector(                       // TRANSLATION
      cos(frameCount * 0.003452) * 400,
      sin(frameCount * 0.009802) * 100,
      0),
    new PVector(                         // SCALE
      1.05 + cos(frameCount * 0.2) * 0.05,
      1.05 + sin(frameCount * 0.2) * 0.05,
      1)
    );
  
  // cleanup of the previous rendering
  background(0);
  
  // tuning the perspective
  float fov = PI/3.0;
  float cameraZ = (height/2.0) / tan(fov/2.0);
  perspective(fov, float(width)/float(height), 1, -100000 );
  
  // displaying the tube at the center of the viewport
  pushMatrix();
  translate( width * 0.5, height * 0.5, 0 );
  strokeWeight( 1 );
  t.draw();
  //t.debug();    // uncomment to display the structure
  popMatrix();

}