class Tube {

  private PVector[][] origin_verts;
  private PVector[][] verts;
  private Plane[] planes;
  private PVector dimension;

  Tube( int segments ) {
    dimension = new PVector( 2, 2, 2 );
    planes = new Plane[ segments * 4 ];
    origin_verts = new PVector[ segments+1 ][ 4 ];
    verts = new PVector[ segments+1 ][ 4 ];
    int pid = 0;
    for ( int i = 0; i < segments+1; ++i ) {
      verts[i] = new PVector[4];
      verts[i][0] = new PVector( -1, -1, -i );
      verts[i][1] = new PVector( 1, -1, -i );
      verts[i][2] = new PVector( 1, 1, -i );
      verts[i][3] = new PVector( -1, 1, -i );
      origin_verts[i] = new PVector[4];
      origin_verts[i][0] = new PVector( -1, -1, -i );
      origin_verts[i][1] = new PVector( 1, -1, -i );
      origin_verts[i][2] = new PVector( 1, 1, -i );
      origin_verts[i][3] = new PVector( -1, 1, -i );
      if ( i > 0 ) {
        int h = i - 1;
        planes[pid] = new Plane(
          verts[h][0], 
          verts[h][1], 
          verts[i][1], 
          verts[i][0]
          ); 
        ++pid;
        planes[pid] = new Plane(
          verts[h][1], 
          verts[h][2], 
          verts[i][2], 
          verts[i][1]
          ); 
        ++pid;
        planes[pid] = new Plane(
          verts[h][2], 
          verts[h][3], 
          verts[i][3], 
          verts[i][2]
          ); 
        ++pid;
        planes[pid] = new Plane(
          verts[h][3], 
          verts[h][0], 
          verts[i][0], 
          verts[i][3]
          ); 
        ++pid;
      }
    }
  }

  public void set_dimension( PVector dim ) {
    dimension.set( dim );
    int depth = origin_verts.length;
    float zdepth = dimension.z / (depth-1);
    for ( int i = 0; i < depth; ++i ) {
      for ( int j = 0; j < 4; ++j ) {
        verts[i][j].set(
          origin_verts[i][j].x * dimension.x * 0.5, 
          origin_verts[i][j].y * dimension.y * 0.5, 
          origin_verts[i][j].z * dimension.z * zdepth
          );
      }
    }
  }

  public void rts( float r, PVector t, PVector sc ) {
    float angl = 0;
    int depth = origin_verts.length;
    PVector vmult = new PVector( dimension.x * 0.5, dimension.y * 0.5, dimension.z / (depth-1) );
    PVector t_accumul = new PVector(0, 0, 0);
    PVector s_accumul = new PVector(1,1,1);
    for ( int i = 0; i < depth; ++i ) {
      PVector cossin = new PVector( cos( angl ), sin( angl ) );
      for ( int j = 0; j < 4; ++j ) {
        float x = origin_verts[i][j].x * vmult.x * s_accumul.x;
        float y = origin_verts[i][j].y * vmult.y * s_accumul.y;
        float z = origin_verts[i][j].z * vmult.z * s_accumul.z;
        // https://stackoverflow.com/questions/14607640/rotating-a-vector-in-3d-space
        verts[i][j].set(
          x * cossin.x - y * cossin.y,
          x * cossin.y + y * cossin.x,
          z
          );
        verts[i][j].add( t_accumul );
      }
      angl += r;
      t_accumul.add( t );
      s_accumul.x *= sc.x;
      s_accumul.y *= sc.y;
      s_accumul.z *= sc.z;
    }
  }

  public void draw() {
    noStroke();
    fill( 255, 0, 0 );
    beginShape(QUADS);
    texture( tex );
    for ( int i = 0; i < planes.length; ++i ) {
      planes[i].vertices();
    }
    endShape();
  }

  public void debug() {
    stroke( 255 );
    noFill();
    beginShape(QUADS);
    for ( int i = 0; i < planes.length; ++i ) {
      planes[i].vertices();
    }
    endShape();
  }
  
}