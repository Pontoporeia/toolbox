class Plane {

  private PVector[] points;
  private float[][] uvs;
  private boolean display;

  private int divisions = 9;

  Plane( PVector a, PVector b, PVector c, PVector d ) {
    points = new PVector[4];
    points[0] = a;
    points[1] = b;
    points[2] = c;
    points[3] = d;
    uvs = new float[4][2];
    uvs[0] = new float[]{ 0, 0 };
    uvs[1] = new float[]{ 1, 0 };
    uvs[2] = new float[]{ 1, 1 };
    uvs[3] = new float[]{ 0, 1 };
    display = true;
  }

  public void vertices() {

    if (!display) return;

    // basic rendring, just one quad
    //vertex( points[0].x,points[0].y,points[0].z, uvs[0][0], uvs[0][1] );
    //vertex( points[1].x,points[1].y,points[1].z, uvs[1][0], uvs[1][1] );
    //vertex( points[2].x,points[2].y,points[2].z, uvs[2][0], uvs[2][1] );
    //vertex( points[3].x,points[3].y,points[3].z, uvs[3][0], uvs[3][1] );


    // better rendering, done by subdividing the quad in a 
    // 9 x 9 grid of quads
    PVector left_delta = new PVector();
    left_delta.add( points[0] );
    left_delta.mult( -1 );
    left_delta.add( points[3] );
    left_delta.mult( 1.f / divisions );

    PVector right_delta = new PVector();
    right_delta.add( points[1] );
    right_delta.mult( -1 );
    right_delta.add( points[2] );
    right_delta.mult( 1.f / divisions );

    PVector left_top = new PVector();
    left_top.add( points[0] );
    PVector left_bottom = new PVector();
    left_bottom.add( left_top );
    left_bottom.add( left_delta );

    PVector right_top = new PVector();
    right_top.add( points[1] );
    PVector right_bottom = new PVector();
    right_bottom.add( right_top );
    right_bottom.add( right_delta );

    for ( float y = 0; y < divisions; ++y ) {

      PVector top_delta = new PVector();
      top_delta.add( left_top );
      top_delta.mult( -1 );
      top_delta.add( right_top );
      top_delta.mult( 1.f / divisions );

      PVector bottom_delta = new PVector();
      bottom_delta.add( left_bottom );
      bottom_delta.mult( -1 );
      bottom_delta.add( right_bottom );
      bottom_delta.mult( 1.f / divisions );

      PVector top = new PVector();
      top.add( left_top );

      PVector bottom = new PVector();
      bottom.add( left_bottom );

      for ( float x = 0; x < divisions; ++x ) {
        
        vertex( 
          top.x, 
          top.y, 
          top.z, 
          (x + 0) / divisions, (y + 0) / divisions );
        vertex( 
          top.x + top_delta.x, 
          top.y + top_delta.y, 
          top.z + top_delta.z, 
          (x + 1) / divisions, (y + 0) / divisions );
        vertex( 
          bottom.x + bottom_delta.x, 
          bottom.y + bottom_delta.y, 
          bottom.z + bottom_delta.z, 
          (x + 1) / divisions, (y + 1) / divisions );
        vertex( 
          bottom.x, 
          bottom.y, 
          bottom.z, 
          (x + 0) / divisions, (y + 1) / divisions );

        top.add( top_delta );
        bottom.add( bottom_delta );
        
      }

      left_top.add( left_delta );
      left_bottom.add( left_delta );
      right_top.add( right_delta );
      right_bottom.add( right_delta );
      
    }
    
  }
}