public void render_tex() {

  int offy = -( frameCount * 40 ) % tex.height;
  
  tex.beginDraw();
  tex.image( tex_fixed, 0, offy );
  tex.image( tex_fixed, 0, offy + tex.height );
  tex.endDraw();
  
}