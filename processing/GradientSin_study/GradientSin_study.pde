float[][] c = new float[][] {
  {1,0,0},
  {0,1,0},
  {0,1,0},
  {1,0,0}
};

loadPixels();
for( int y = 0; y < 100; ++y ) {
  float s0 = sin( PI * 0.5 * y * 0.01 );
  float s1 = sin( PI * 0.5 * ( 1 - y * 0.01 ) );
  float ty = s0 + s1;
  float[] cL = new float[3];
  float[] cR = new float[3];
  for ( int i = 0; i < 3; ++i ) {
    cL[i] = c[0][i] * s1 / ty + c[3][i] * s0 / ty;
    cR[i] = c[1][i] * s1 / ty + c[2][i] * s0 / ty;
  }
  //stroke( s0 * 255 / t, s1 * 255 / t, 0 );
  for ( int x = 0; x < 100; ++x ) {
    float s2 = sin( PI * 0.5 * x * 0.01 );
    float s3 = sin( PI * 0.5 * ( 1 - x * 0.01 ) );
    float tx = s2 + s3;
    pixels[ x + y * 100 ] = color(
      255 * ( cL[0] * s3 / tx + cR[0] * s2 / tx ),
      255 * ( cL[1] * s3 / tx + cR[1] * s2 / tx ),
      255 * ( cL[2] * s3 / tx + cR[2] * s2 / tx )
    );
  }
}
updatePixels();