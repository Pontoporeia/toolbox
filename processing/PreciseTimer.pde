// François Zajéga - http://www.frankiezafe.org

// PreciseTimer uses its own thread, meaning it's not related to the main draw period
// thus, "framerate" can be different from the sketch's one

// HOW TO
// use "start" and "stop" methods to control the timer
// setFps to adjust the period
// implement a method called "threaded_update( PreciseTimer pt )" in your main class
// the method will be called automatically by the timer
// if you use multiple PreciseTimer, check witch one has called "threaded_update" by
// testing the argument of the method
// each precise timer identify himself via this argument


public class PreciseTimer implements Runnable {

  private PApplet parent; // processing sketch
  private Thread thread;
  private int sleep; // delay between each run in millis
  private float lastTime;
  private float deltaTime; // delta time in millis
  private int fps; // just for info
  private int counter; // number of run of the timer
  private boolean active; // activated or not

  // CONSTRUCTOR
  //////////////

  public PreciseTimer( PApplet p ) {
    parent = p;
    parent.registerDispose( this );
    sleep = -1;
    fps = -1;
  }
  
  // GETTERS & SETTERS
  ////////////////////
  
  public void setFps( int framerate ) {
    if ( framerate > 0 ) {
      fps = framerate;
      sleep = 1000 / framerate;
    }
  }
  
  public int getFps() {
    return fps;
  }

  public int getCounter() {
    return counter;
  }
  
  public float getDeltaTime() {
    return deltaTime;
  }

  public boolean isActive() {
    return active;
  }

  public void start() {
    if ( sleep == -1 )
      start( 60 );
    else
      start( fps );
  }

  // CONTROLS
  ///////////

  public void start( int framerate ) {
    setFps( framerate );
    active = true;
    thread = new Thread( this );
    thread.start();
    lastTime = millis();
    deltaTime = 0;
    counter = 0;
  }

  public void dispose() { 
    stop();
  }
  
  public void stop() { 
    active = false;
    thread = null;
    counter = 0;
    deltaTime = 0;
  }

  // MAIN METHOD
  //////////////

  public void run() {
    while( active ) {
      deltaTime = millis() - lastTime;
      lastTime = millis();
      counter++;
      threaded_update( this );
      delay( sleep );
    }
  }
}

