/**
* Reads commands coming from serial interface to drive an LED on/off
* Also prints led status back
*/
#define LEDPIN 13
int state = 0;
void setup() {
  Serial.begin(9600);
  pinMode(LEDPIN, OUTPUT);
}
void loop() {
  
  // send data only when you receive data:
  if (Serial.available() > 0) {
     // we receive a char representing an integer. let's converto to int
    int incomingState = (Serial.read() == '1');
    // say what you got:
    Serial.print("I received: ");
    Serial.println(incomingState, DEC);
    if( incomingState != state ) {
      state = incomingState;
      if ( state == 1 ) {
           digitalWrite(LEDPIN, HIGH );
    } else {
           digitalWrite(LEDPIN, LOW );
        }
    }
  }
}
