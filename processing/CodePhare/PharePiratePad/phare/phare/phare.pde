import processing.serial.*;
Serial myPort;  // Create object from Serial class
int state = 31;
String arduino =  "/dev/ttyUSB5";
boolean ledOn = false;
void setup() {
  size(600, 600);
  myPort = new Serial( this, arduino, 9600 );
}
void draw() {
    
  if(myPort.available() > 0) {
      if (ledOn) {
          myPort.write( 1 );
    } else {
          myPort.write( 0 );
    }
  }
  if ( ledOn ) {
      background( 255 );
      } else {
      background( 0 );
}
}
void mousePressed() {
    ledOn = true;
}
void mouseRelease() {
    ledOn = false;
}
