
import SimpleOpenNI.*;
SimpleOpenNI context;

import processing.serial.*;
Serial myPort;

boolean kinect = true;
int diametre = 400;

PVector com = new PVector();                                   
PVector com2d = new PVector();
float corpsX, corpsY;
boolean detect;

int state = 31;
String arduino =  "/dev/ttyUSB5";

void setup() {
  
  size(640,480);
  
  myPort = new Serial( this, arduino, 9600 );
  
  context = new SimpleOpenNI(this); 
  context.enableDepth(); 
  context.enableUser();
}

void draw() {
  
  background(0);
  
  // remise à zéro de la détection à chaque frame
  detect = false;
  // rafraichissement des données captées par la kinect
  context.update();
  
  if(kinect == true) {
    
    // affichage de la camera kinect
    image(context.userImage(),0,0);
    
    // parcours de la liste des users détectés
    int[] userList = context.getUsers();
    for(int i=0; i<userList.length; i++){ 
      
      if(context.getCoM(userList[i],com)){
        context.convertRealWorldToProjective(com,com2d);
        
        // définition du x et y du user
        corpsX = com2d.x;
        corpsY = com2d.y;
        
        // calcul de la distance entre le centre du user et le centre de la kinect
        float hyp = sqrt( pow(corpsX-width/2,2) + pow(height/2-corpsY,2) );
        
        // si centre user < que rayon du cercle, détection = true
        if(hyp < diametre/2) { detect = true; }
        
        // dessin de la ligne entre le centre du user et le centre de la kinect
        stroke(255);
        line(width/2,height/2,corpsX,corpsY);
      }
      
    }
    
  }
  else {
      // pour la souris
      corpsX = mouseX;
      corpsY = mouseY;
      
      float hyp = sqrt( pow(corpsX-width/2,2) + pow(height/2-corpsY,2) );
      if(hyp < diametre/2) { detect = true; }
      
      stroke(255);
      line(width/2,height/2,corpsX,corpsY);
  }
  
  // envoi de la détection vers le port
  if(myPort.available() > 0) {
    if (detect == true) { myPort.write(1); }
    else { myPort.write(0); }
  }
  
  // dessin du cercle de détection
  // si la détection = true, couleur verte
  if(detect == true) {
    stroke(0,255,0);
    fill(0,255,0,100);
  }
  // si la détection = false, couleur rouge
  else {
    stroke(255,0,0);
    fill(255,0,0,100);
  }
  ellipse(width/2,height/2,diametre,diametre);
  
  // dessin du centre corps
  noStroke();
  fill(255);
  ellipse(corpsX,corpsY,10,10);
  
}
