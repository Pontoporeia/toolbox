int[] couleur1 = new int[]{255,0,0};
int[] couleur2 = new int[]{0,255,0};
float pow_couleur1;
float pow_couleur2;

PGraphics render;

public void setup() {
    
  size( 300,150 );
  render = createGraphics( 5000, 5000 );
  
  
}

void draw() {
  
  couleur1[2] = int( random( 0, 255 ) );
  
  render.beginDraw();
  render.loadPixels();
  int i = 0;
  for( float y = 0; y < render.height; ++y ) {
  for( float x = 0; x < render.width; ++x ) {
    pow_couleur2 = x / render.width;
    pow_couleur1 = 1 - pow_couleur2;
    float[] pixel_color = new float[]{0,0,0};
    for (int c = 0; c < 3; ++c) {
      pixel_color[c] = couleur1[c] * pow_couleur1 + couleur2[c] * pow_couleur2;
    }
    render.pixels[i] = color( pixel_color[0], pixel_color[1], pixel_color[2] );
    ++i;
  }
  }
  render.updatePixels();
  render.endDraw();
  
  println( frameCount );
  String path = "anim/test_";
  if ( frameCount < 10 ) {
    path += "0000";
  } else if ( frameCount < 100 ) {
    path += "000";
  } else if ( frameCount < 1000 ) {
    path += "00";
  } else if ( frameCount < 10000 ) {
    path += "0";
  }
  path += frameCount + ".jpg";
  render.save( path );
  
  background( 0 );
  image( render, 0,0, height * ( render.width * 1.f / render.height ), height );
  
}