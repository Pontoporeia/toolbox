int[] couleur1 = new int[]{255,0,0};
int[] couleur2 = new int[]{0,255,0};
float pow_couleur1;
float pow_couleur2;

public void setup() {
    
  size( 300,150 );
  
}

void draw() {
  
  couleur1[2] = int( random( 0, 255 ) );
  
  loadPixels();
  int i = 0;
  for( float y = 0; y < height; ++y ) {
  for( float x = 0; x < width; ++x ) {
    pow_couleur2 = x / width;
    pow_couleur1 = 1 - pow_couleur2;
    float[] pixel_color = new float[]{0,0,0};
    for (int c = 0; c < 3; ++c) {
      pixel_color[c] = couleur1[c] * pow_couleur1 + couleur2[c] * pow_couleur2;
    }
    pixels[i] = color( pixel_color[0], pixel_color[1], pixel_color[2] );
    ++i;
  }
  }
  updatePixels();
  
  println( frameCount );
  String path = "anim/test_";
  if ( frameCount < 10 ) {
    path += "0000";
  } else if ( frameCount < 100 ) {
    path += "000";
  } else if ( frameCount < 1000 ) {
    path += "00";
  } else if ( frameCount < 10000 ) {
    path += "0";
  }
  path += frameCount + ".jpg";
  //saveFrame( path );
  
}