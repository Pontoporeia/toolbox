
// configuration du nom de l'exportation
int imagenum = 300; // nombre d'image à exporter
int digits = 5; // nombre de chiffres dans le nom
String prefix = "export/img_";
String suffix = ".png";

void setup() {
  size( 600, 600 );
}

void draw() {
  
  background( random( 0, 255 ), random( 0, 255 ), random( 0, 255 ) );
  
  // on sauve chaque frame
  saveFrame( sequentialPath() );

  // on arrête dés qu'on a atteint la dernière image
  if ( frameCount > imagenum ) {
    exit();
  }
  
}

String sequentialPath() {
  String path = prefix;
  int pow10 = 1;
  int div = 10;
  int f = frameCount - 1;
  while( float( f ) / div >= 1 ) {
    div *= 10;
    pow10++;
  }
  for ( int i = 0; i < digits - pow10; i++ ) {
    path += "0";
  }
  path += f + suffix;
  return path;
}
