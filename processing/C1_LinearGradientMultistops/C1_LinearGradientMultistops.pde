 void setup() {
  size(500, 500);
  smooth();
  frameRate(30);
  background(226, 225, 215);
  PImage pincelmar; 
  pincelmar = createImage(300, 20, RGB);
  pincelmar.loadPixels();
  color c1 = color(255, 0, 0);
  color c2 = color(0, 255, 0);
  color c3 = color(0, 0, 255);
  color c4 = color(255, 255, 0);
  for (int i = 0; i < pincelmar.height; i++) {   
    for (int j = 0; j < pincelmar.width; j++) {
      int index = j + i * pincelmar.width;
      color sc = color( 0, 0, 0 );
      float pc = (float) j / (float) pincelmar.width;
      float third = 1.f/3;
      if ( pc <= third ) {
        sc = lerpColor(c1, c2, pc * 3);
      } else if ( pc <= 2*third ) {
        sc = lerpColor(c2, c3, ( pc - third) * 3);
      } else {
        sc = lerpColor(c3, c4, ( pc - 2*third) * 3);
      }
      pincelmar.pixels[index] = sc;
    }
  }
  pincelmar.updatePixels();
  translate(100, 200);
  image(pincelmar, 0, 0);
 }