
// tableau comprenant les couleurs en float
float[][] fcolors;

// j'ai 29 images de 256 x 256
int IMG_WIDTH = 256;
int IMG_HEIGHT = 256;
int IMG_NUM = 29;

// sert à constituer le nom des fichiers à lire
String img_path = "resized_img_";
String img_ext = ".jpg";

// permet de faire la comparaison entre les 2 techniques
PGraphics result;
PGraphics simplestack;

void setup() {

  //size( IMG_WIDTH * 2, IMG_HEIGHT );
  size( 512, 256, P2D );
  
  // nombre total de pixels, identiques pour toutes les images
  int pnum = IMG_WIDTH * IMG_HEIGHT;
  
  // création de l'image standard
  simplestack = createGraphics( 256, 256, P2D );
  
  // on prépare le tableau RGB
  fcolors = new float[ pnum ][ 3 ];
  
  // et c'est parti!
  // attention, mes images commencent à 1, pas à 0
  for ( int i = 1; i <= IMG_NUM; i++ ) {
    String fname = img_path;
    if ( i < 10 ) {
      fname += "00";
    } else if ( i < 100 ) {
      fname += "0";
    }
    fname += i + img_ext;
    // voilà, le nom est ok
    // on charge l'image
    PImage im = loadImage( fname );
    // on charge les pixels
    im.loadPixels();
    // et on les lit
    for ( int p = 0; p < pnum; p++ ) {
      // on divise chaque canal par le nombre d'images
      fcolors[ p ][ 0 ] += red( im.pixels[ p ] ) / IMG_NUM;
      fcolors[ p ][ 1 ] += green( im.pixels[ p ] ) / IMG_NUM;
      fcolors[ p ][ 2 ] += blue( im.pixels[ p ] ) / IMG_NUM;
    }
    // bête empilement alpha
    simplestack.beginDraw();
    if ( i > 1 ) {
      // alpha dépend du nombre d'images... pas top, top, mais bon...
      simplestack.tint( 255, 255.f / IMG_NUM );
    }
    simplestack.image( im, 0, 0 );
    simplestack.endDraw();
  }
  // fcolor contient maintenant le mélange de couleurs en float
  // donc moins de perte de précision
  // reste à les afficher
  result = createGraphics( 256, 256, P2D );
  result.loadPixels();
  // cette méthode permet de convertir les fcolors en couleurs affichables
  for ( int p = 0; p < pnum; p++ ) {
    result.pixels[ p ] = color( fcolors[ p ][ 0 ], fcolors[ p ][ 1 ], fcolors[ p ][ 2 ] );
  }
  // on update et on vérifie dans le draw
  result.updatePixels();
  
}

void draw() {

  image( result, 0, 0 );
  image( simplestack, IMG_WIDTH, 0 );

  text( "ma manière", 10, 25 );
  text( "la manière alpha", IMG_WIDTH + 10, 25 );

}

// bon, ok tout ça, mais si j'en rajoute une?
// ben c'est tout simple
// avant toute chose:
// IMG_NUM++
// il faut ensuite charger tous les pixels de la nouvelle image
// et utiliser, pour chaque canal, cette formule-ci
// float percent = 1.f / IMG_NUM;
// fcolors[ p ][ 0 ] = fcolors[ p ][ 0 ] * ( 1 - percent ) + red( im.pixels[ p ] ) * percent;
// et mettre à jour le PGraphics result (même procédure que dans le setup)