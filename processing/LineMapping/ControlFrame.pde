class ControlFrame extends PApplet {

  ControlP5 ui = null;
  ListBox ui_list = null;
  Button ui_add = null;
  ArrayList<Slider> ui_line_sliders;
  Textfield ui_osc_address;
  Textfield ui_texture_path;
  Button ui_confirm = null;
  Button ui_delete = null;
  Button ui_save = null;
  
  int w, h;
  LineMapping parent;
  ControlP5 cp5;

  public ControlFrame(LineMapping _parent, int _w, int _h, String _name) {
    super();   
    parent = _parent;
    w=_w;
    h=_h;
    PApplet.runSketch(new String[]{this.getClass().getName()}, this);
  }

  public void settings() {
    size(w, h);
  }

  public void setup() {
    
    surface.setLocation(10, 10);
    
    int uiy_top = 20;
    int uiy = uiy_top;
    
    ui = new ControlP5(this);
    
    ui_add = ui.addButton("add_line").setPosition(20, uiy).setSize(50, 15);
    ui_save = ui.addButton("save_all").setPosition(240, uiy).setSize(50, 15); uiy += 20;
    ui_list = ui.addListBox("lines").setPosition(20, uiy).setSize(270, 120);
    
    uiy = uiy_top;
    
    ui_line_sliders = new ArrayList<Slider>();
    // thickness
    ui_line_sliders.add( ui.addSlider("thickness_a").setPosition(20,uiy).setSize(200, 12).setRange(0,300) ); uiy += 15;
    ui_line_sliders.add( ui.addSlider("thickness_mid").setPosition(20,uiy).setSize(200, 12).setRange(0,300) ); uiy += 15;
    ui_line_sliders.add( ui.addSlider("thickness_b").setPosition(20,uiy).setSize(200, 12).setRange(0,300) ); uiy += 15;
    ui_line_sliders.add( ui.addSlider("thickness_all").setPosition(20,uiy).setSize(200, 12).setRange(0,300) ); uiy += 20;
    // rgba
    ui_line_sliders.add( ui.addSlider("red_tint").setPosition(20,uiy).setSize(120, 12).setRange(0,1) ); uiy += 15;
    ui_line_sliders.add( ui.addSlider("green_tint").setPosition(20,uiy).setSize(120, 12).setRange(0,1) ); uiy += 15;
    ui_line_sliders.add( ui.addSlider("blue_tint").setPosition(20,uiy).setSize(120, 12).setRange(0,1) ); uiy += 15;
    ui_line_sliders.add( ui.addSlider("opacity").setPosition(20,uiy).setSize(120, 12).setRange(0,1) ); uiy += 20;
    // positions
    ui_line_sliders.add( ui.addSlider("pos_x_a").setPosition(20,uiy).setSize(200, 12).setRange(0,parent.width) ); uiy += 15;
    ui_line_sliders.add( ui.addSlider("pos_y_a").setPosition(20,uiy).setSize(200, 12).setRange(0,parent.height) ); uiy += 15;
    ui_line_sliders.add( ui.addSlider("pos_x_b").setPosition(20,uiy).setSize(200, 12).setRange(0,parent.width) ); uiy += 15;
    ui_line_sliders.add( ui.addSlider("pos_y_b").setPosition(20,uiy).setSize(200, 12).setRange(0,parent.height) ); uiy += 20;
    
    ui_osc_address = ui.addTextfield("osc_address").setPosition(20,uiy).setSize(200,15); uiy += 30;
    ui_texture_path = ui.addTextfield("texture_path").setPosition(20,uiy).setSize(200,15); uiy += 40;
    
    ui_confirm = ui.addButton("confirm").setPosition(20,uiy).setSize(50,15);
    ui_delete= ui.addButton("del").setPosition(170,uiy).setSize(50,15); uiy += 30;
    
    parent.on_controlframe_loaded();
    
    noLoop();
    
  }

  void draw() {
    background(0);
  }
  
  public void add_line(int i) {
    newline = true;
    editline = new Line( parent );
    editline.init( 
      parent.width * 0.5, 
      parent.height * 0.15, 
      parent.width * 0.5, 
      parent.height * 0.25 );
    editline.set_texture_path( default_texture_path );
    editline.load_texture();
    parent.editmode = EDITMODE_EDIT;
    ui_refresh();
  }
  
  public void save_all(int i) {
    parent.save_lines();
  }
  
  public void confirm(int i) {
    if ( parent.editmode == EDITMODE_EDIT || parent.editmode == EDITMODE_DRAG ) {
      if ( parent.newline ) {
        ls.add( parent.editline );
      }
      parent.newline = false;
      parent.editline.set_osc_address( ui_osc_address.getText() );
      parent.editline.set_texture_path( ui_texture_path.getText() );
      parent.editline.load_texture();
      parent.editline = null;
      parent.editmode = EDITMODE_NORMAL;
      regenerate_ui();
      ui_refresh();
    }
  }
  
  public void del(int i) {
    if ( parent.editmode == EDITMODE_EDIT || parent.editmode == EDITMODE_DRAG ) {
      if ( !parent.newline ) {
        ls.remove( parent.editline );
      }
      parent.newline = false;
      parent.editline = null;
      parent.editmode = EDITMODE_NORMAL;
      regenerate_ui();
      ui_refresh();
    }
  }
  
  public void thickness_a( float v ) {
    if ( editline != null ) {
      parent.editline.set_thickness( 0, v );
    }
  }
  
  public void thickness_mid( float v ) {
    if ( editline != null ) {
      parent.editline.set_thickness( 1, v );
    }
  }
  
  public void thickness_b( float v ) {
    if ( editline != null ) {
      parent.editline.set_thickness( 2, v );
    }
  }
  
  public void thickness_all( float v ) {
    if ( parent.editline != null ) {
      for( int i = 0; i < 3; ++i ) {
        ui_line_sliders.get(i).setValue( v );
      }
    }
  }
  
  public void red_tint( float v ) {
    if ( editline != null ) {
      parent.editline.set_rgba( 0, v );
    }
  }
  
  public void green_tint( float v ) {
    if ( parent.editline != null ) {
      parent.editline.set_rgba( 1, v );
    }
  }
  
  public void blue_tint( float v ) {
    if ( parent.editline != null ) {
      parent.editline.set_rgba( 2, v );
    }
  }
  
  public void opacity( float v ) {
    if ( parent.editline != null ) {
      parent.editline.set_rgba( 3, v );
      
    }
  }
  
  public void pos_x_a( float v ) {
    if ( parent.editline != null ) {
      parent.editline.set_a_x(v);
    }
  }
  
  public void pos_y_a( float v ) {
    if ( parent.editline != null ) {
      parent.editline.set_a_y(v);
    }
  }
  
  public void pos_x_b( float v ) {
    if ( parent.editline != null ) {
      parent.editline.set_b_x(v);
    }
  }
  
  public void pos_y_b( float v ) {
    if ( parent.editline != null ) {
      parent.editline.set_b_y(v);
    }
  }
  
  void controlEvent(ControlEvent e) {
    if( e.getName().equals("lines") ) {
      int lineid = (int)e.getValue();
      parent.editline = parent.ls.get( lineid );
      parent.editmode = EDITMODE_EDIT;
      ui_refresh();
    }
  }
  
  public void show_main( boolean visible ) {
    if ( visible ) {
      ui_add.show();
      ui_save.show();
      ui_list.show();
    } else {
      ui_add.hide();
      ui_save.hide();
      ui_list.hide();
    }
  }
  
  public void show_edit( boolean visible ) {
    if ( visible ) {
      for( Slider s : ui_line_sliders ) {
        s.show();
      }
      ui_osc_address.show();
      ui_texture_path.show();
      ui_confirm.show();
      ui_delete.show();
    } else {
      for( Slider s : ui_line_sliders ) {
        s.hide();
      }
      ui_osc_address.hide();
      ui_texture_path.hide();
      ui_confirm.hide();
      ui_delete.hide();
    }
  }
  
  public void ui_refresh() {
    if ( ui == null ) {
      return;
    }
    switch( parent.editmode ) {
      case EDITMODE_NORMAL:
        show_main(true);
        show_edit(false);
        break;
      case EDITMODE_EDIT:
        {
          int i = 0;
          for( Slider s : ui_line_sliders ) {
            if ( editline != null ) {
              switch( i ) {
                case 0:
                case 1:
                case 2:
                  s.setValue( editline.get_thickness( i ) );
                  break;
                case 4:
                case 5:
                case 6:
                case 7:
                  s.setValue( editline.get_rgba( i - 4 ) );
                  break;
                case 8:
                  s.setValue( editline.get_a().x );
                  break;
                case 9:
                  s.setValue( editline.get_a().y );
                  break;
                case 10:
                  s.setValue( editline.get_b().x );
                  break;
                case 11:
                  s.setValue( editline.get_b().y );
                  break;
              }
            }
            ++i;
          }
          ui_osc_address.setValue( editline.get_osc_address() );
          ui_texture_path.setValue( editline.get_texture_path() );
          show_main(false);
          show_edit(true);
        }
        break;
      case EDITMODE_DRAG:
        show_main(false);
        show_edit(true);
        {
        int i = 0;
        for( Slider s : ui_line_sliders ) {
          if ( editline != null ) {
            switch( i ) {
              case 8:
                s.setValue( editline.get_a().x );
                break;
              case 9:
                s.setValue( editline.get_a().y );
                break;
              case 10:
                s.setValue( editline.get_b().x );
                break;
              case 11:
                s.setValue( editline.get_b().y );
                break;
              default:
                break;
            }
          }
          ++i;
        }
        }
    }
  }
  
  public void line_list(int i) {
    print( "line selected: " + i );
  }
  
  public void regenerate_ui() {
    int i = 0;
    String[] names = new String[ls.size()];
    for( Line l : ls ) {
      names[i] = "" + i + " * " + l.get_osc_address() + " * " + l.get_texture_path();
      ++i;
    }
    ui_list.setItems( names );
  }

}