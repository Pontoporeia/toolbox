import processing.core.PVector;
import processing.core.PApplet;
import processing.core.PImage;
import java.util.ArrayList;
import oscP5.*;

class Line implements java.io.Serializable {
  
  public static final int MESH_SUBDIVIDE = 3;

  private transient LineMapping parent;
  private transient PImage texture;
  private transient float[][] geom_uv;

  private PVector a;
  private PVector mid;
  private PVector b;
  private PVector normal;
  private float thickness_a;
  private float thickness_mid;
  private float thickness_b;
  private PVector selected;
  private String osc_address;
  
  private String texture_path;
  private float[] rgba;

  Line( LineMapping p ) {
    parent = p;
    osc_address = "/L";
    texture_path = "";
    float[] c = {1,1,1,1};
    rgba = c;
    reset();
  }

  Line() {
    parent = null;
    osc_address = "/L";
    texture_path = "";
    float[] c = {1,1,1,1};
    rgba = c;
    reset();
  }

  public void reset() {
    
    texture = null;
    a = new PVector();
    mid = new PVector();
    b = new PVector();
    normal = new PVector();
    thickness_a = 10;
    thickness_mid = 10;
    thickness_b = 10;
    selected = null;
    geom_uv = null;
    generate_geometry();
    
  }
  
  public void load_texture() {
    if ( texture_path.equals("") ) { 
      texture = null;
      return;
    }
    texture = parent.get_texture( texture_path );
  }
  
  public PVector get_a() {
    return a;
  }
  
  public void set_a_x( float v ) {
    a.x = v;
    update_vectors();
    generate_geometry();
  }
  
  public void set_a_y( float v ) {
    a.y = v;
    update_vectors();
    generate_geometry();
  }
  
  public PVector get_b() {
    return b;
  }
  
  public void set_b_x( float v ) {
    b.x = v;
    update_vectors();
    generate_geometry();
  }
  
  public void set_b_y( float v ) {
    b.y = v;
    update_vectors();
    generate_geometry();
  }

  public String get_texture_path() {
    return texture_path;
  }

  public void set_texture_path( String s ) {
    texture_path = s;
  }

  public float get_rgba( int i ) {
    return rgba[i];
  }
  
  public void set_rgba( int i, float f ) {
    rgba[i] = f;
  }
  
  public float get_opacity() {
    return rgba[3];
  }

  public void set_opacity( float o ) {
    rgba[3] = o;
  }
  
  public void set_parent( LineMapping p ) {
    if ( parent == null ) {
      parent = p;
      update_vectors();
      generate_geometry();
      load_texture();
    }
  }

  public float get_thickness( int i ) {
    switch( i ) {
    case 0:
      return thickness_a;
    case 1:
      return thickness_mid;
    case 2:
      return thickness_b;
    };
    return 0;
  }

  public void set_thickness( int i, float v ) {
    switch( i ) {
    case 0:
      thickness_a = v;
      break;
    case 1:
      thickness_mid = v;
      break;
    case 2:
      thickness_b = v;
      break;
    }
    generate_geometry();
  }

  public String get_osc_address() {
    return osc_address;
  }

  public void set_osc_address( String s ) {
    osc_address = s;
  }

  public void init( float ax, float ay, float bx, float by ) {
    a.x = ax;
    a.y = ay;
    b.x = bx;
    b.y = by;
    update_vectors();
    generate_geometry();
  }
  
  private void generate_tris( 
    ArrayList<float[]> faces, 
    PVector top, PVector bottom,
    PVector top_uv, PVector bottom_uv,
    float top_thick, float bottom_thick
    ) {
    
      float[] f;
      f = new float[5];
      f[0] = top.x - normal.x * top_thick;
      f[1] = top.y - normal.y * top_thick;
      f[2] = top.z - normal.z * top_thick;
      f[3] = top_uv.x;
      f[4] = top_uv.y;
      faces.add( f );
      f = new float[5];
      f[0] = top.x + normal.x * top_thick;
      f[1] = top.y + normal.y * top_thick;
      f[2] = top.z + normal.z * top_thick;
      f[3] = bottom_uv.x;
      f[4] = top_uv.y;
      faces.add( f );
      f = new float[5];
      f[0] = bottom.x;
      f[1] = bottom.y;
      f[2] = bottom.z;
      f[3] = top_uv.x + ( bottom_uv.x - top_uv.x ) * 0.5f;
      f[4] = bottom_uv.y;
      faces.add( f );
      
      f = new float[5];
      f[0] = bottom.x;
      f[1] = bottom.y;
      f[2] = bottom.z;
      f[3] = top_uv.x + ( bottom_uv.x - top_uv.x ) * 0.5f;
      f[4] = bottom_uv.y;
      faces.add( f );
      f = new float[5];
      f[0] = top.x + normal.x * top_thick;
      f[1] = top.y + normal.y * top_thick;
      f[2] = top.z + normal.z * top_thick;
      f[3] = bottom_uv.x;
      f[4] = top_uv.y;
      faces.add( f );
      f = new float[5];
      f[0] = bottom.x + normal.x * bottom_thick;
      f[1] = bottom.y + normal.y * bottom_thick;
      f[2] = bottom.z + normal.z * bottom_thick;
      f[3] = bottom_uv.x;
      f[4] = bottom_uv.y;
      faces.add( f );
      
      f = new float[5];
      f[0] = bottom.x;
      f[1] = bottom.y;
      f[2] = bottom.z;
      f[3] = top_uv.x + ( bottom_uv.x - top_uv.x ) * 0.5f;
      f[4] = bottom_uv.y;
      faces.add( f );
      f = new float[5];
      f[0] = bottom.x - normal.x * bottom_thick;
      f[1] = bottom.y - normal.y * bottom_thick;
      f[2] = bottom.z - normal.z * bottom_thick;
      f[3] = top_uv.x;
      f[4] = bottom_uv.y;
      faces.add( f );
      f = new float[5];
      f[0] = top.x - normal.x * top_thick;
      f[1] = top.y - normal.y * top_thick;
      f[2] = top.z - normal.z * top_thick;
      f[3] = top_uv.x;
      f[4] = top_uv.y;
      faces.add( f );
      
  }
  
  private void generate_geometry( 
    ArrayList<float[]> faces, 
    PVector start, PVector stop,
    PVector start_uv, PVector stop_uv,
    float start_thick, float stop_thick
    ) {
        
    // 3d
    PVector step = new PVector( 
      ( stop.x - start.x ) / MESH_SUBDIVIDE, 
      ( stop.y - start.y ) / MESH_SUBDIVIDE,
      ( stop.z - start.z ) / MESH_SUBDIVIDE );
    
    // uv
    PVector uv_step = new PVector( 
      (stop_uv.x - start_uv.x) / MESH_SUBDIVIDE, 
      (stop_uv.y - start_uv.y) / MESH_SUBDIVIDE );
    PVector uv_top = new PVector( start_uv.x, start_uv.y );
    PVector uv_bottom = new PVector( start_uv.x + uv_step.x, start_uv.y + uv_step.y );
    
    // thickness
    float thick_step = ( stop_thick - start_thick ) / MESH_SUBDIVIDE;
    float thick_top = start_thick;
    float thick_bottom = start_thick + thick_step;
    
    PVector top = new PVector( start.x, start.y, start.z );
    PVector bottom = new PVector( start.x, start.y, start.z );
    bottom.add( step );
     
    float div_tmp = 0.5f - ( 1.f / MESH_SUBDIVIDE ) * 0.5f;
     
    for ( int y = 0; y < MESH_SUBDIVIDE; ++y ) {
      
      PVector top_tmp = new PVector( 
        top.x - normal.x * thick_top * div_tmp,
        top.y - normal.y * thick_top * div_tmp,
        top.z - normal.z * thick_top * div_tmp );
      PVector top_step = new PVector( 
        normal.x * thick_top / MESH_SUBDIVIDE, 
        normal.y * thick_top / MESH_SUBDIVIDE, 
        normal.z * thick_top / MESH_SUBDIVIDE );
      
      PVector bottom_tmp = new PVector( 
        bottom.x - normal.x * thick_bottom * div_tmp,
        bottom.y - normal.y * thick_bottom * div_tmp,
        bottom.z - normal.z * thick_bottom * div_tmp );
      PVector bottom_step = new PVector( 
        normal.x * thick_bottom / MESH_SUBDIVIDE, 
        normal.y * thick_bottom / MESH_SUBDIVIDE, 
        normal.z * thick_bottom / MESH_SUBDIVIDE );
      
      for ( int x = 0; x < MESH_SUBDIVIDE; ++x ) {
        
        generate_tris( 
          faces,
          top_tmp, bottom_tmp,
          uv_top, uv_bottom,
          thick_top / MESH_SUBDIVIDE * 0.5f, 
          thick_bottom / MESH_SUBDIVIDE * 0.5f
          );
        
        top_tmp.add(top_step);
        bottom_tmp.add(bottom_step);
        
        uv_top.x += uv_step.x;
        uv_bottom.x += uv_step.x;
        
      }
      
      top.add( step );
      bottom.add( step );
      
      uv_top.x = start_uv.x;
      uv_bottom.x = start_uv.x + uv_step.x;
      uv_top.y += uv_step.y;
      uv_bottom.y += uv_step.y;
      
      thick_top += thick_step;
      thick_bottom += thick_step;
      
    }
    
  }
  
  private void generate_geometry() {
    ArrayList<float[]> tmp_faces = new ArrayList<float[]>();
    generate_geometry(
      tmp_faces,
      a, mid,
      new PVector( 0,0 ),
      new PVector( 1,0.5f ),
      thickness_a,
      thickness_mid
      );
    generate_geometry(
      tmp_faces,
      b, mid,
      new PVector( 0,1 ),
      new PVector( 1,0.5f ),
      thickness_b,
      thickness_mid
      );
    geom_uv = new float[ tmp_faces.size() ][5];
    for( int i = 0; i < geom_uv.length; ++i ) {
      geom_uv[i] = tmp_faces.get(i);
    }
  }

  private void update_vectors() {
    normal.set( b.x-a.x, b.y-a.y, 0 );
    mid.set( normal.x * 0.5f, normal.y * 0.5f, b.z-a.z );
    normal.normalize();
    normal = normal.cross( new PVector(0, 0, 1) );
    mid.add( a );
  }

  public void tri() {
    if ( parent == null || geom_uv == null ) return;
    parent.noStroke();
    if ( texture != null ) { 
      parent.tint( 255*rgba[0], 255*rgba[1], 255*rgba[2], 255*rgba[3] );
      parent.texture(texture);
    } else {
      parent.fill( 255*rgba[0], 255*rgba[1], 255*rgba[2], 255*rgba[3] );
    }
    //stroke( 255, frameCount % 255 );
    parent.beginShape( parent.TRIANGLES );
    if ( texture != null ) { 
      parent.texture(texture);
    }
    int len = geom_uv.length;
    for( int i = 0; i < len; ++i ) {
       parent.vertex( geom_uv[i][0],geom_uv[i][1],geom_uv[i][2],geom_uv[i][3],geom_uv[i][4] );
    }
    parent.endShape();
    if ( texture != null ) { 
      parent.noTint();
    }
  }

  public void edit() {
    if ( parent == null ) return;
    parent.noFill();
    if ( selected == a ) {
      parent.stroke( 255, 0, 0 );
    } else {
      parent.stroke( 255, 255, 0 );
    }
    parent.ellipse( a.x, a.y, 20, 20 );
    if ( selected == b ) {
      parent.stroke( 255, 0, 0 );
    } else {
      parent.stroke( 255, 255, 0 );
    }
    parent.ellipse( b.x, b.y, 20, 20 );
  }

  public void drag( float x, float y ) {
    if ( selected == null ) {
      return;
    }
    selected.x = x;
    selected.y = y;
    update_vectors();
    generate_geometry();
  }

  PVector hit( int mx, int my ) {
    if ( parent.dist( mx, my, a.x, a.y ) < 10 ) {
      selected = a;
    } else if ( parent.dist( mx, my, b.x, b.y ) < 10 ) {
      selected = b;
    } else {
      selected = null;
    }
    return selected;
  }
  
  public void parse(OscMessage msg) {
    
    if ( !msg.checkAddrPattern(osc_address) ) return;
    
    byte[] types = msg.getTypetagAsBytes();
    int argnum = types.length;
    try {
      for( int i = 0; i < argnum; ++i ) {
        if ( types[i] == 's'  ) {
          String argname = msg.get(i).stringValue();
          if ( argname.equals("opacity") ) {
            ++i; rgba[3] = msg.get(i).floatValue();
          } else if ( argname.equals("red") ) {
            ++i; rgba[0] = msg.get(i).floatValue();
          } else if ( argname.equals("green") ) {
            ++i; rgba[1] = msg.get(i).floatValue();
          } else if ( argname.equals("blue") ) {
            ++i; rgba[2] = msg.get(i).floatValue();
          } else if ( argname.equals("rgb") ) {
            ++i; rgba[0] = msg.get(i).floatValue();
            ++i; rgba[1] = msg.get(i).floatValue();
            ++i; rgba[2] = msg.get(i).floatValue();
          } else if ( argname.equals("rgba") ) {
            ++i; rgba[0] = msg.get(i).floatValue();
            ++i; rgba[1] = msg.get(i).floatValue();
            ++i; rgba[2] = msg.get(i).floatValue();
            ++i; rgba[3] = msg.get(i).floatValue();
          } else {
            System.out.println( "??? " + argname );
          }
        } 
      }
    } catch( Exception e ) {}
    
  }
  
}