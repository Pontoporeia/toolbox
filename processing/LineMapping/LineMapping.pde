import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;
import oscP5.*;
import netP5.*;
import controlP5.*;

static final int EDITMODE_NORMAL = 1;
static final int EDITMODE_EDIT = 2;
static final int EDITMODE_DRAG = 3;

java.util.ArrayList<Line> ls;

String default_texture_path = "line-texture.png";
java.util.HashMap<String,PImage> texture_atlas;

OscP5 oscP5;
int oscP5_port = 23000;

public int editmode;
Line editline;
boolean newline;
PVector offset;

ControlFrame cf;

void settings() {
  size( 640, 400, P3D );
}

void setup() {
  
  //size( 1280, 800, P3D );
  
  smooth(8);
  
  cf = new ControlFrame( this, 310, 600, "Controls");
  
  surface.setLocation(420, 10);
  
  editmode = EDITMODE_NORMAL;
  editline = null;
  newline = false;
  offset = new PVector();
  
  texture_atlas = new java.util.HashMap<String,PImage>();
  load_default_texture();
  textureMode(NORMAL);
  textureWrap(CLAMP);
  
  start_osc();
  
}

void on_controlframe_loaded() {
  cf.ui_refresh();
  load_lines();
  cf.regenerate_ui();
}

void draw() {
  
  cf.redraw();
  
  background(0);
  if ( editline != null ) {
    editline.tri();
    editline.edit();
  }
  
  for( Line l : ls ) {
    if ( l == editline ) {
      continue;
    }
    l.tri();
  }
  
}

void mouseMoved() {
  if ( editmode == EDITMODE_EDIT ) {
    editline.hit( mouseX, mouseY );
  }
}

void mouseDragged() {
  if ( editmode == EDITMODE_DRAG ) {
    editline.drag( mouseX + offset.x, mouseY + offset.y );
  }
}

void mouseReleased() {
  if ( editmode == EDITMODE_DRAG ) {
    editmode = EDITMODE_EDIT;
    cf.ui_refresh();
  }
}

void mousePressed() {
  if ( editmode == EDITMODE_EDIT ) {
    PVector v = editline.hit( mouseX, mouseY );
    if ( v != null ) {
      offset.set( v.x - mouseX, v.y - mouseY );
      editmode = EDITMODE_DRAG;
      cf.ui_refresh();
    }
  }
}