
public PImage get_default_texture() {
  return get_texture( default_texture_path );
}

public PImage get_texture( String path ) {
  if ( !texture_atlas.containsKey( path ) && !load_texture( path ) ) {
    return null;
  }
  return texture_atlas.get( path );
}

private void load_default_texture() {
  load_texture( default_texture_path );
}

private boolean load_texture( String path ) {
  PImage tmp = loadImage( path );
  if ( tmp.width > 0 ) {
    texture_atlas.put( path, tmp );
    return true;
  }
  return false;
}