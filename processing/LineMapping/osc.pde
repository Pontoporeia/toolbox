
public void start_osc() {
  oscP5 = new OscP5( this, oscP5_port );
}

public void oscEvent(OscMessage msg) {

  if ( editmode != EDITMODE_NORMAL ) {
    return;
  }
  for( Line l : ls ) {
    l.parse( msg );
  }
  
}