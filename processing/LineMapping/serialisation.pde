String serialisation_path() {
  return dataPath("") + "/lines";
}

void save_lines() {
  // https://www.tutorialspoint.com/java/java_serialization.htm
  try {
    FileOutputStream fileOut = new FileOutputStream(serialisation_path());
    ObjectOutputStream out = new ObjectOutputStream(fileOut);
    out.writeObject( ls );
    out.close();
    fileOut.close();
  } 
  catch( IOException e ) {
    e.printStackTrace();
  }
}

void load_lines() {
  // https://beginnersbook.com/2013/12/how-to-serialize-arraylist-in-java/
  ls = new ArrayList<Line>();
  try {
    FileInputStream fis = new FileInputStream(serialisation_path());
    ObjectInputStream ois = new ObjectInputStream(fis);
    ls = (ArrayList<Line>) ois.readObject();
    ois.close();
    fis.close();
  } 
  catch( IOException e ) {
    e.printStackTrace();
    return;
  } 
  catch(ClassNotFoundException e) {
    System.out.println("Class not found");
    e.printStackTrace();
    return;
  }
  for ( Line l : ls ) {
    l.set_parent(this);
  }
}