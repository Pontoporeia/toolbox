import processing.video.*;

// EFFET PHOTOCOPIEUSE


Capture webcam;
boolean newframe;

PGraphics webcamcopy;

PGraphics webcamline;
PGraphics compo1;
PGraphics compo2;

PGraphics buffer1;
PGraphics buffer2;

void setup() {
  
  size( 1280, 960 );
  webcam = new Capture( this, 640, 480 );
  webcam.start();
  newframe = false;
  webcamcopy = createGraphics( webcam.width, webcam.height );
  webcamline = createGraphics( 1, webcam.height );
  webcamline.beginDraw();
  webcamline.background( 255,0,0 );
  webcamline.noStroke();
  webcamline.fill( 255 );
  webcamline.rect( 0,100,10,10 );
  webcamline.endDraw();
  
  compo1 = createGraphics( 1000, webcam.height );
  compo1.beginDraw();
  compo1.background( 255, 0, 0 );
  compo1.endDraw();
  
  compo2 = createGraphics( 1000, webcam.height );
  compo2.beginDraw();
  compo2.background( 0 );
  compo2.endDraw();
  
  buffer1 = compo1;
  buffer2 = compo2;
  
}

void captureEvent(Capture c) {
  c.read();
  webcamcopy.beginDraw();
  webcamcopy.image( c, 0, 0 );
  webcamcopy.endDraw();
  webcamcopy.updatePixels();
  newframe = true;
}

void draw() {
  
  if ( newframe ) {
    
    newframe = false;
    
    // chargement du tampon
    webcamline.beginDraw();
    webcamline.image( webcamcopy, -320, 0 );
    webcamline.endDraw();
    
    buffer2.updatePixels();
    buffer1.beginDraw();
    buffer1.image( buffer2, 1, 0 );
    buffer1.image( webcamline, 0, 0 );
    buffer1.endDraw();
    
    // swapping buffers
    PGraphics tmpb = null;
    tmpb = buffer1;
    buffer1 = buffer2;
    buffer2 = tmpb;
    
  } else {
    
    return;
    
  }
  
  background( 128 );
  image( webcamcopy, 0, 0, 200,160 );
  image( webcamline, 210, 0, 10, 160 );
  image( compo2, 230, 0, 500, 160 );
  image( compo1, 0,200 );
  
  fill( 255 );
  text( frameRate, 10, 25 );
  
}