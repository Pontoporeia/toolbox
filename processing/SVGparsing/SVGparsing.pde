
import processing.video.*;

int trinum;
float svg_x;
float svg_y;
float svg_width;
float svg_height;
PShape svg;

PImage tex;
Capture video;

color[] colors;
PImage[] textures;

void setup() {

  
  size( 1024, 768, P3D );
  
  tex = loadImage( "texture.jpg" );
  video = new Capture(this, 640, 480 );
  video.start(); 
  
  svg = loadShape("triangles.svg");

  PVector min = new PVector( 1000000, 1000000 );
  PVector max = new PVector();
  for( int i = 0; i < svg.getChildCount(); i++ ) {
    for ( int j = 0; j < svg.getChild( i ).getVertexCount(); j++ ) {
      PVector v = svg.getChild( i ).getVertex( j );
      if ( v.x < min.x ) {
        min.x = v.x;
      }
      if ( v.y < min.y ) {
        min.y = v.y;
      }
      if ( v.x > max.x ) {
        max.x = v.x;
      }
      if ( v.y > max.y ) {
        max.y = v.y;
      }
    }
  }

  svg_x = min.x;
  svg_y = min.y;
  svg_width = max.x - min.x;
  svg_height = max.y - min.y;

  trinum = svg.getChildCount();

  colors = new color[ trinum ];
  textures = new PImage[ trinum ];

  for ( int i = 0; i < trinum; i++ ) {
    colors[ i ] = color( 255 );
    textures[ i ] = null;
  }

  noStroke();

}

void captureEvent(Capture c) {
  c.read();
}

void draw() {

  background( 5 );
  
  image( tex, svg_x, svg_y, svg_width, svg_height );
  image( video, svg_x, svg_y + svg_height, svg_width, svg_height );
  fill( 0, 150 );
  rect( svg_x, svg_y, svg_width, svg_height * 2 );
  
  for( int i = 0; i < svg.getChildCount(); i++ ) {
    
    PVector center = new PVector();

    PShape triangle = svg.getChild( i );
    fill( colors[ i ] );
    
    beginShape( TRIANGLES );
    
    if ( textures[ i ] != null ) {
      texture( textures[ i ] );
    }
    for ( int j = 0; j < triangle.getVertexCount(); j++ ) {
      PVector v = triangle.getVertex( j );
      PVector uv = new PVector();
      uv.set( v );
      uv.x -= svg_x;
      uv.y -= svg_y;
      if ( textures[ i ] != null ) {
        vertex( v.x, v.y, ( uv.x / svg_width ) * textures[ i ].width, ( uv.y / svg_height ) * textures[ i ].height );
      } else {
        vertex( v.x, v.y );
      }
      center.x += v.x;
      center.y += v.y;
    }
    endShape();
    
    center.mult( 1.f / 3 );
    
    fill( 0 );
    text( i, center.x, center.y );
  
  }

}

void keyPressed() {

  if ( key == 'a' ) {
    colors[ 1 ] = color( random( 0, 255 ), random( 0, 255 ), random( 0, 255 ) );
  } else if ( key == 'b' ) {
    
    // colors[ 2 ] = color( random( 0, 255 ), random( 0, 255 ), random( 0, 255 ) );
 
    if ( textures[ 2 ] == null ) {
      textures[ 2 ] = video;
    } else{
      textures[ 2 ] = null;
    } 
    
  } else if ( key == 'c' ) {
    
    if ( textures[ 3 ] == null ) {
      textures[ 3 ] = tex;
    } else{
      textures[ 3 ] = null;
    } 
    
  } else if ( key == 'd' ) {
    
    if ( textures[ 4 ] == null ) {
      textures[ 4 ] = video;
    } else{
      textures[ 4 ] = null;
    } 
    
  }

}

