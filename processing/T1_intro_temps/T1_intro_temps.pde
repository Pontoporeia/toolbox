int last_time = 0;
float r = 0;
float g = 0;
float b = 0;

int delay_time = 1000;
int delay_current = delay_time;

void setup() {
  //println( "setup" );
}

void draw() {
  int now = millis();
  int delta_time = now - last_time;
  //println( "draw" );
  println( delta_time );
  // comment faire ralentir le frame rate
  //for( int i = 0; i < 10000000; ++i ) {
  //  float a = sin(i) * cos(i) * tan(i);
  //}
  //println( frameCount + " / " + frameRate + " / " + millis() );
  //background( frameCount );

  r += delta_time * 0.01;
  r = r % 255;
  g += delta_time * 0.1;
  g = g % 255;
  delay_current -= delta_time;
  if ( delay_current <= 0 ) {
    background( 255 );
    delay_current = delay_time;
  } else {
    //background( r, g, b );
    background( 0 );
  }
  
  last_time = now;
  
}