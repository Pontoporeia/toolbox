ArrayList<PVector> gesture;
boolean mouse_is_pressed;
String results;

void setup() {

  size( 500, 500 );
  gesture = new ArrayList<PVector>();
  mouse_is_pressed = false;
  results = "";
  
}

void draw() {

  if ( mouse_is_pressed ) {
    gesture.add( new PVector( mouseX * 1.f / width, mouseY * 1.f / height ) );
  }

  background( 0 );  
  pushMatrix();
  scale( width, height );
  strokeWeight( 1.f / width );
  stroke( 255 );
  PVector prev = null;
  for ( PVector p : gesture ) {
    if ( prev != null ) {
      line( prev.x, prev.y, p.x, p.y );
    }
    prev = p;
  }
  popMatrix();
  
  text( results, 10, 25 );
  
}

void analyseGesture() {
  float total_dist = 0;
  PVector prev = null;
  for ( PVector p : gesture ) {
    if ( prev != null ) {
      total_dist += p.dist( prev );
    }
    prev = p;
  }
  results = "total dist: " + total_dist;
}

void mousePressed() {
  gesture.clear();
  results = "";
  mouse_is_pressed = true;
}

void mouseReleased() {
  mouse_is_pressed = false;
  analyseGesture();
}