MultiSlider red_slider;
MultiSlider green_slider;
MultiSlider blue_slider;
Slider threshold_slider;

void setupUI() {
  
  Interactive.make( this );
  
  red_slider = new MultiSlider( 10, 10, 255, 10 );
  red_slider.rgb( 255,0,0 );
  red_slider.setValues( 0, 0.1 );
  
  green_slider = new MultiSlider( 10, 25, 255, 10 );
  green_slider.rgb( 0,255,0 );
  green_slider.setValues( 0, 0.1 );
  
  blue_slider = new MultiSlider( 10, 40, 255, 10 );
  blue_slider.rgb( 0,0,255 );
  blue_slider.setValues( 0, 0.1 );
  
  threshold_slider = new Slider ( 10, 55, 255, 10 );
  threshold_slider.setValue( 0.1 );
  
  Interactive.add( red_slider );
  Interactive.add( green_slider );
  Interactive.add( blue_slider );
  Interactive.add( threshold_slider );
  
}

void drawUI() {
  red_slider.draw();
  green_slider.draw();
  blue_slider.draw();
}

void getUIvalues() {
 
  MultiSlider ms = red_slider;
  
  red_key_mid = int( 255 * ms.values[0] + ( ms.values[1] - ms.values[0] ) * 0.5 );
  red_key_in = int( 255 * ms.values[0] );
  red_key_out = int( 255 * ms.values[1] );
  
  ms = green_slider;
  green_key_mid = int( 255 * ms.values[0] + ( ms.values[1] - ms.values[0] ) * 0.5 );
  green_key_in = int( 255 * ms.values[0] );
  green_key_out = int( 255 * ms.values[1] );
  
  ms = blue_slider;
  blue_key_mid = int( 255 * ms.values[0] + ( ms.values[1] - ms.values[0] ) * 0.5 );
  blue_key_in = int( 255 * ms.values[0] );
  blue_key_out = int( 255 * ms.values[1] );
  
  threshold = int( 255 * threshold_slider.value );
  
}