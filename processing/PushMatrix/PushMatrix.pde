float[] halfwin;
float[] model_scale;
float[] model_rotation;
float[] clicked_mouse;
float[] current_mouse;
float[] previous_mouse;
boolean view_pressed;

public void setup() {

  size( 600, 600, P3D );
  halfwin = new float[] { width * 0.5, height * 0.5 };
  model_scale = new float[] { 100,100,100 };
  model_rotation = new float[] { 0,0,0 };
  clicked_mouse = new float[] { 0,0 };
  current_mouse = new float[] { 0,0 };
  previous_mouse = new float[] { 0,0 };
  view_pressed = false;
  
}

public void axis() {
  stroke( 255,0,0 );
  line( 0,0,0, 1,0,0 );
  stroke( 0,255,0 );
  line( 0,0,0, 0,1,0 );
  stroke( 0,0,255 );
  line( 0,0,0, 0,0,1 );
}

public void draw() {
  
  background( 0 );
  
  noFill();
  pushMatrix();
    translate( halfwin[0], halfwin[1], 0 );
    scale( model_scale[0], model_scale[1], model_scale[2] );
    strokeWeight( 1 / model_scale[0] );
    rotateX( model_rotation[0] );
    axis();
    rotateY( model_rotation[1] );
    rotateZ( model_rotation[2] );
    axis();
    stroke( 255 );
    box( 2 );
    translate( 1, 0, 0 );
    scale( 1 / model_scale[0], 1 / model_scale[1], 1 / model_scale[2] );
    rotateZ( -model_rotation[2] );
    rotateY( -model_rotation[1] );
    rotateX( -model_rotation[0] );
    text( "XYZ", 0,0 );
  popMatrix();
  
  /*
  pushMatrix();
    translate( halfwin[0], halfwin[1], 0 );
    scale( model_scale[0], model_scale[1], model_scale[2] );
    rotateY( model_rotation[1] );
    rotateX( model_rotation[0] );
    rotateZ( model_rotation[2] );
    strokeWeight( 1 / model_scale[0] );
    axis();
    stroke( 255 );
    box( 2 );
    translate( 1, 0, 0 );
    scale( 1 / model_scale[0], 1 / model_scale[1], 1 / model_scale[2] );
    rotateZ( -model_rotation[2] );
    rotateX( -model_rotation[0] );
    rotateY( -model_rotation[1] );
    text( "YXZ", 0,0 );
  popMatrix();
  */
  
  if ( view_pressed ) {
    pushMatrix();
      noStroke();
      fill( 255 );
      translate( halfwin[0], halfwin[1], 0 );
      ellipse( clicked_mouse[0] * halfwin[0], clicked_mouse[1] * halfwin[1], 3,3 );
      ellipse( current_mouse[0] * halfwin[0], current_mouse[1] * halfwin[1], 3,3 );
      stroke( 255 );
      strokeWeight( 1 );
      line(
        clicked_mouse[0] * halfwin[0], clicked_mouse[1] * halfwin[1], 0,
        current_mouse[0] * halfwin[0], current_mouse[1] * halfwin[1], 0
        );
    popMatrix();
  }

}

public void mousePressed() {
  current_mouse[0] = ( mouseX - halfwin[0] ) / halfwin[0];
  current_mouse[1] = ( mouseY - halfwin[1] ) / halfwin[1];
  previous_mouse[0] = current_mouse[0];
  previous_mouse[1] = current_mouse[1];
  clicked_mouse[0] = current_mouse[0];
  clicked_mouse[1] = current_mouse[1];
  if ( mouseButton == LEFT ) {
    view_pressed = true;
  }
}

public void mouseReleased() {
  view_pressed = false;
}

public void mouseDragged() {
  previous_mouse[0] = current_mouse[0];
  previous_mouse[1] = current_mouse[1];
  float relx = ( mouseX - halfwin[0] ) / halfwin[0];
  float rely = ( mouseY - halfwin[1] ) / halfwin[1];
  float deltax = relx - previous_mouse[0];
  float deltay = rely - previous_mouse[1];
  if ( view_pressed ) {
    model_rotation[1] += deltax * PI;
    model_rotation[0] -= deltay * PI;
  }
  current_mouse[0] = relx;
  current_mouse[1] = rely;
}

void mouseWheel(MouseEvent event) {
  float e = event.getCount();
  if ( model_scale[0] > 10 || e > 0 ) {
    model_scale[0] += e;
    model_scale[1] += e;
    model_scale[2] += e;
  }
}