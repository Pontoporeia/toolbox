float[] boite;

void setup() {
  
  size( 600, 600, P3D );
  //                     x    y   z   w    h   d
  //                     0    1   2   3    4   5
  boite = new float[] { 150, 260, 0, 100, 200, 30 };
  
}

void draw() {
  
  boite[2] = -50 + frameCount % 100;
  
  int mx = mouseX;
  int my = mouseY;
  int mz = 0;
  
  boolean hit = false;
  if ( 
    mx >= boite[0] - boite[3] * 0.5 && 
    my >= boite[1] - boite[4] * 0.5 && 
    mz >= boite[2] - boite[5] * 0.5 && // après l'origine?
    mx <= boite[0] + boite[3] * 0.5 && 
    my <= boite[1] + boite[4] * 0.5 && 
    mz <= boite[2] + boite[5] * 0.5
    ) {
    hit = true;
  }
  
  background( 0 );
  
  //stroke( 255,0,0 );
  noFill();
  noStroke();
  stroke( 255 );
  if ( hit ) {
    stroke( 255,0,0 );
  }
  
  pushMatrix();
    translate( boite[0],boite[1],boite[2] );
    scale( boite[3], boite[4], boite[5] );
    strokeWeight( 1 / boite[3] );
    box( 1 );
    //rect( -0.5, -0.5, 1, 1  );
  popMatrix();
  
  fill( 255,0,0, 100 );
  pushMatrix();
    translate( boite[0],boite[1],0);
    scale( boite[3], boite[4], 1 );
    //strokeWeight( 1 / boite[3] );
    rect( -0.5, -0.5, 1, 1  );
  popMatrix();
  
  fill( 0, 255, 0 );
  ellipse( mx, my, 20, 20 );
    
}