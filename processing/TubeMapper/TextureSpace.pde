class TextureSpace {
  
  public PVector offset;
  public PVector scale;
  
  public TextureSpace() {
    offset = new PVector( 0,0,0 );
    scale = new PVector( 1,1,1 );
  }
  
}
