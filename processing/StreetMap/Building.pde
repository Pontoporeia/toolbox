class Building extends IDObject {

  int cellid;
  float[] pos;
  float[] size;
  PImage tex_wall1;
  PImage tex_wall2;
  PImage tex_wall3;
  PImage tex_wall4;
  PImage tex_roof;
  
  public Building( Map map, int p ) {
  
    cellid = p;
    pos = new float[] { 
      ( map.grid[ p ][ CCOL ] * map.cell[0] ) + map.cell[0] * 0.5f,
      ( map.grid[ p ][ CROW ] * map.cell[1] ) + map.cell[1] * 0.5f
    };
    
    size = new float[] { 
      map.cell[0] * random( 0.85f, 0.98f ), 
      map.cell[1] * random( 0.85f, 0.98f ),
      map.cell[0] * 0.6f + random( 0, map.cell[0] * 1.5f) 
    };
    
    if ( size[ 2 ] < map.cell[0] * 0.95f ) {
      tex_wall1 = texture_hangar;
      tex_wall2 = texture_hangar;
      tex_wall3 = texture_hangar;
      tex_wall4 = texture_hangar_door;
      tex_roof = texture_hangar_roof;
    } else {
      tex_wall1 = texture_building;
      tex_wall2 = texture_building;
      tex_wall3 = texture_building;
      tex_wall4 = texture_building;
      tex_roof = texture_roof;
    }
    
  }
  
  public void draw( PGraphics target, float draww, float drawh ) {
    
    target.pushMatrix();
    target.translate( pos[ 0 ] * draww, pos[ 1 ] * drawh, 0 );
    
    float w = size[ 0 ] * draww;
    float h = size[ 1 ] * drawh;
    float d = size[ 2 ] * draww;
    
    float[][] haut = new float[4][3];
    float[][] bas = new float[4][3];
    
    //                       X         Y     Z
    bas[0] = new float[] { -w*0.5f, -h*0.5f, 0 };
    bas[1] = new float[] { w*0.5f, -h*0.5f, 0 }; 
    bas[2] = new float[] { w*0.5f, h*0.5f, 0 }; 
    bas[3] = new float[] { -w*0.5f, h*0.5f, 0 };
    
    float a = frameCount * 0.01;
    float sina = sin( a );
    float cosa = cos( a );
    
    haut[0] = new float[] { -w*0.5f, -h*0.5f, d };
    haut[1] = new float[] { w*0.5f, -h*0.5f, d }; 
    haut[2] = new float[] { w*0.5f, h*0.5f, d }; 
    haut[3] = new float[] { -w*0.5f, h*0.5f, d }; 
    
    target.noStroke();
    if ( enable_id_display )
      target.fill( rgbID );
    else
      target.fill( 160,160,160 );
    
    target.beginShape();
      if ( !enable_id_display )
        target.texture( tex_roof );
      target.vertex( haut[0][0], haut[0][1], haut[0][2], 0,0 );
      target.vertex( haut[1][0], haut[1][1], haut[1][2], tex_roof.width,0 );
      target.vertex( haut[2][0], haut[2][1], haut[2][2], tex_roof.width, tex_roof.height );
      target.vertex( haut[3][0], haut[3][1], haut[3][2], 0,tex_roof.height );
    target.endShape();
    
    target.beginShape();
      if ( !enable_id_display )
        target.texture( tex_wall1 );
      target.vertex( bas[0][0], bas[0][1], bas[0][2], tex_wall1.width, tex_wall1.height );
      target.vertex( bas[1][0], bas[1][1], bas[1][2],  0, tex_wall1.height );
      target.vertex( haut[1][0], haut[1][1], haut[1][2], 0,0 );
      target.vertex( haut[0][0], haut[0][1], haut[0][2], tex_wall1.width,0 );
    target.endShape();
    
    target.beginShape();
      if ( !enable_id_display )
        target.texture( tex_wall1 );
      target.vertex( bas[3][0], bas[3][1], bas[3][2], 0, tex_wall1.height );
      target.vertex( bas[2][0], bas[2][1], bas[2][2], tex_wall1.width, tex_wall1.height );
      target.vertex( haut[2][0], haut[2][1], haut[2][2], tex_wall1.width,0 );
      target.vertex( haut[3][0], haut[3][1], haut[3][2], 0,0 );
    target.endShape();
    
    target.beginShape();
      if ( !enable_id_display )
        target.texture( tex_wall1 );
      target.vertex( bas[1][0], bas[1][1], bas[1][2], tex_wall1.width, tex_wall1.height );
      target.vertex( bas[2][0], bas[2][1], bas[2][2], 0, tex_wall1.height );
      target.vertex( haut[2][0], haut[2][1], haut[2][2], 0,0 );
      target.vertex( haut[1][0], haut[1][1], haut[1][2], tex_wall1.width,0 );
    target.endShape();
    
    target.beginShape();
      if ( !enable_id_display )
        target.texture( tex_wall1 );
      target.vertex( bas[0][0], bas[0][1], bas[0][2], 0, tex_wall1.height );
      target.vertex( bas[3][0], bas[3][1], bas[3][2], tex_wall1.width, tex_wall1.height );
      target.vertex( haut[3][0], haut[3][1], haut[3][2], tex_wall1.width,0 );
      target.vertex( haut[0][0], haut[0][1], haut[0][2], 0,0 );
    target.endShape();
    
    target.popMatrix();
    
  }

}