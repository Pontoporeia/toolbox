class Truck extends Vehicule {

  public Truck( Map map ) {
    
    super( map );
    paint = new int[] { 255,0,255 };
    size = new float[] { map.cell[0] * STREET_SIZE * 0.22f, map.cell[1] * STREET_SIZE * 0.7f, map.cell[0] * STREET_SIZE * 0.28f };
    
    tex_roof = texture_truck_roof;
    tex_front = texture_truck_front;
    tex_left = texture_truck_left;
    tex_back = texture_truck_back;
    tex_right = texture_truck_right;
    
  }
  
  public void init() {
    
    super.init();
    speed = map.cell[0] * random( 0.015, 0.02 );
    
  }

}
