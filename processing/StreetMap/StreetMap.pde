
Map carte;
Vehicule[] flotte;

int cellNum = 25;
int flotteNum = 100;

float drawsize = 10;
float drawsize_speed;
float drawsize_dir;
boolean drawsize_enabled;

PImage texture_building;
PImage texture_roof;
PImage texture_hangar;
PImage texture_hangar_door;
PImage texture_hangar_roof;

PImage texture_bus_roof;
PImage texture_bus_front;
PImage texture_bus_left;
PImage texture_bus_back;
PImage texture_bus_right;

PImage texture_truck_roof;
PImage texture_truck_front;
PImage texture_truck_left;
PImage texture_truck_back;
PImage texture_truck_right;

PImage[] texture_cars;

PImage[] roads;

float rotationz_speed;
float rotationz_dir;
float rotationz_current;
boolean rotationz;

float building_size;

////// contrôle de la "vue"
float[] halfwin;
float[] model_scale;
float[] model_rotation;
float[] clicked_mouse;
float[] current_mouse;
float[] previous_mouse;
boolean view_pressed;

// identification des objects
IDRenderer IDR;

// texture de rendu principale
PGraphics displayTexture;

public void setup() {
  
  size( 1024, 768, P3D );
  displayTexture = createGraphics( 1024, 768, P3D );
  
  drawsize_speed = 0;
  drawsize_dir = 0;
  drawsize_enabled = false;
  
  rotationz = false;
  rotationz_dir = 0;
  rotationz_speed = 0;
  rotationz_current = 0;
  
  // textures
  texture_building =     loadImage( "building.png" );
  texture_roof =         loadImage( "roof.png" );
  texture_hangar =       loadImage( "hangar.png" );
  texture_hangar_door =  loadImage( "hangar_door.png" );
  texture_hangar_roof =  loadImage( "hangar_roof.png" );
  
  texture_bus_roof =     loadImage( "bus_roof.png" );
  texture_bus_front =    loadImage( "bus_front.png" );
  texture_bus_left =     loadImage( "bus_left.png" );
  texture_bus_back =     loadImage( "bus_back.png" );
  texture_bus_right =    loadImage( "bus_right.png" );
  
  texture_truck_roof =     loadImage( "truck_roof.png" );
  texture_truck_front =    loadImage( "truck_front.png" );
  texture_truck_left =     loadImage( "truck_left.png" );
  texture_truck_back =     loadImage( "truck_back.png" );
  texture_truck_right =    loadImage( "truck_right.png" );
  
  texture_cars = new PImage[ 6 ];
  for ( int i = 0; i < texture_cars.length; i++ )
    texture_cars[ i ] = loadImage( "car_" + ( i + 1 ) + ".png" );
  
  roads = new PImage[ 5 ];
  for ( int i = 0; i < roads.length; i++ )
    roads[ i ] = loadImage( "road_"+( i + 1 )+".png" );

  carte = new Map( cellNum, cellNum );
  
  flotte = new Vehicule[ flotteNum ];
  for ( int i = 0; i < flotteNum; i++ ) {
    float r = random( 0, 1 );
    if ( r < 0.75 )
      flotte[ i ] = new Car( carte );
    else if ( r < 0.93 )
      flotte[ i ] = new Truck( carte );
    else
      flotte[ i ] = new Bus( carte );
  }
  
  building_size = 1;
  
  halfwin = new float[] { width * 0.5, height * 0.5 };
  model_scale = new float[] { 100,100,100 };
  model_rotation = new float[] { -0.4,0,0 };
  clicked_mouse = new float[] { 0,0 };
  current_mouse = new float[] { 0,0 };
  previous_mouse = new float[] { 0,0 };
  view_pressed = false;
  
  IDR = new IDRenderer( carte, flotte );
  IDR.init( width, height );
  
}

public void axis() {
  stroke( 255,0,0 );
  line( 0,0,0, 1,0,0 );
  stroke( 0,255,0 );
  line( 0,0,0, 0,1,0 );
  stroke( 0,0,255 );
  line( 0,0,0, 0,0,1 );
}

public void draw() {
  
  building_size = 1.5 + sin(frameCount * 0.1);
  float fov = PI/5;
  float cameraZ = (height/2.0) / tan(fov/2.0);
  
  for ( int i = 0; i < flotteNum; i++ ) {
    flotte[ i ].update();
    //flotte2[ i ].update();
  }

  IDR.render( fov, cameraZ, halfwin, model_scale, model_rotation, drawsize );
  IDR.select( mouseX * 1.f / width, mouseY * 1.f / height );
  
  // rendu principal
  displayTexture.beginDraw();
  displayTexture.lights();
  // ambientLight( 150,140,140 );
  displayTexture.directionalLight( 255, 179, 16, -0.3, 0, -0.1 );
  displayTexture.background( 255, 232, 184 );
  displayTexture.perspective(fov, float(width)/float(height), cameraZ/10.0, cameraZ*1000.0);
  displayTexture.pushMatrix();
    displayTexture.translate( halfwin[0], halfwin[1], 0 );
    displayTexture.scale( model_scale[0], model_scale[1], model_scale[2] );
    displayTexture.rotateX( model_rotation[0] );
    displayTexture.rotateY( model_rotation[1] );
    displayTexture.rotateZ( model_rotation[2] );
    displayTexture.rotateX( PI * 0.5 );
    displayTexture.strokeWeight( 1 / model_scale[0] );
    axis();
    carte.draw( displayTexture, drawsize, drawsize );
    displayTexture.translate( 0,0,-0.2 );
    for ( int i = 0; i < flotteNum; i++ ) {
      flotte[ i ].draw( displayTexture, drawsize, drawsize );
    }
  displayTexture.popMatrix();
  displayTexture.endDraw();
  
  noTint();
  image( displayTexture, 0, 0 );
  IDR.draw( 10, 10, 160, 120 );
  IDR.overlay();
  
}

public void keyReleased() {
  
  switch( keyCode ) {
    case 37:
    case 39:
      rotationz = false;
      rotationz_dir = 0;
      break;
    case 38:
    case 40:
      drawsize_enabled = false;
      break;
    default:
      break;
  }
  
}

public void keyPressed() {
  
  switch( keyCode ) {
  
    case 65: // a
      break;
      
    case 90: // z
      break;
      
    case 82: // r
      carte.init();
      //carte2.init();
      for ( int i = 0; i < flotteNum; i++ ) {
        flotte[ i ].init();
        //flotte2[ i ].init();
      }
      IDR.refresh();
      break;
    
    case 37: // left
      rotationz = true;
      rotationz_dir = -1;
      break;
    
    case 38: // haut
      drawsize_enabled = true;
      drawsize_dir = -1;
      break;
    
    case 39: // right
      rotationz = true;
      rotationz_dir = 1;
      break;
    
    case 40: // bas
      drawsize_enabled = true;
      drawsize_dir = 1;
      break;
      
    default:
      println( keyCode );
  
  }
  
}

public void mousePressed() {
  current_mouse[0] = ( mouseX - halfwin[0] ) / halfwin[0];
  current_mouse[1] = ( mouseY - halfwin[1] ) / halfwin[1];
  previous_mouse[0] = current_mouse[0];
  previous_mouse[1] = current_mouse[1];
  clicked_mouse[0] = current_mouse[0];
  clicked_mouse[1] = current_mouse[1];
  if ( mouseButton == LEFT ) {
    view_pressed = true;
  } else if ( mouseButton == RIGHT ) {
    IDR.click();
  }
}

public void mouseReleased() {
  view_pressed = false;
}

public void mouseDragged() {
  previous_mouse[0] = current_mouse[0];
  previous_mouse[1] = current_mouse[1];
  float relx = ( mouseX - halfwin[0] ) / halfwin[0];
  float rely = ( mouseY - halfwin[1] ) / halfwin[1];
  float deltax = relx - previous_mouse[0];
  float deltay = rely - previous_mouse[1];
  if ( view_pressed ) {
    model_rotation[1] += deltax * PI;
    model_rotation[0] -= deltay * PI;
  }
  current_mouse[0] = relx;
  current_mouse[1] = rely;
}

void mouseWheel(MouseEvent event) {
  float e = event.getCount() * 10;
  if ( model_scale[0] > 10 || e > 0 ) {
    model_scale[0] += e;
    model_scale[1] += e;
    model_scale[2] += e;
  }
}