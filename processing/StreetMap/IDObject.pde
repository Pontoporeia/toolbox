class IDObject {
  
  public int id;
  public int rgbID;
  boolean enable_id_display;
  
  IDObject() {
    id = 0;
    rgbID = 0;
    enable_id_display = false;
  }
  
}