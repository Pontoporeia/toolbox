class Bus extends Vehicule {

  public Bus( Map map ) {

    super( map );
    paint = new int[] { 255, 255, 0 };
    size = new float[] { 
      map.cell[0] * STREET_SIZE * 0.25f, 
      map.cell[1] * STREET_SIZE * 0.8f, 
      map.cell[0] * STREET_SIZE * 0.25f };
    
    tex_roof = texture_bus_roof;
    tex_front = texture_bus_front;
    tex_left = texture_bus_left;
    tex_back = texture_bus_back;
    tex_right = texture_bus_right;
    
  }

  public void init() {

    super.init();
    speed = map.cell[0] * random( 0.001, 0.0015 );
    
  }
  
}