class IDRenderer {

  private Map r_map;
  private Vehicule[] r_flotte;
  private ArrayList< IDObject > cells;
  private ArrayList< IDObject > buildings;
  private ArrayList< IDObject > vehicules;
  private IDObject selected_cell;
  private IDObject selected_building;
  private IDObject selected_vehicule;
  private PGraphics texture;
  private PGraphics selected;

  IDRenderer( Map m, Vehicule[] f ) {
    r_map = m;
    r_flotte = f;
    refresh();
  }
  
  public void refresh() {
    
    selected_cell = null;
    selected_building = null;
    selected_vehicule = null;
    
    cells = new ArrayList< IDObject >();
    buildings = new ArrayList< IDObject >();
    vehicules = new ArrayList< IDObject >();
    int r = 255; 
    int g = 0; 
    int b = 0;
    for ( int i = 0; i < r_map.cols * r_map.rows; ++i ) {
      IDObject io = new IDObject();
      io.id = i;
      io.rgbID = color( r,g,b );
      g += 3;
      if ( g >= 230 ) {
        b += 3;
        g = 0;
      }
      cells.add( io );
    }
    r_map.cellIds = cells;
    r = 0;
    g = 255;
    b = 0;
    for ( IDObject io : r_map.buildings ) {
      io.rgbID = color( r,g,b );
      r += 3;
      if ( r >= 230 ) {
        b += 3;
        r = 0;
      }
      buildings.add( io );
    }
    r = 0;
    g = 0;
    b = 255;
    for ( int i = 0; i < r_flotte.length; ++i ) {
      IDObject io = r_flotte[i];
      io.rgbID = color( r,g,b );
      r += 3;
      if ( r >= 230 ) {
        g += 3;
        r = 0;
      }
      vehicules.add( io );
    }
  }

  public void init( int width, int height ) {
    texture = createGraphics( width, height, P3D );
    selected = createGraphics( width, height, P3D );
  }

  public void render( float fov, float cameraZ, float[] halfwin, float[] model_scale, float[] model_rotation, float drawsize ) {
    
    texture.beginDraw();
    texture.noLights();
    texture.background( 0,0,0 );
    texture.perspective(fov, float(width)/float(height), cameraZ/10.0, cameraZ*1000.0);
    texture.pushMatrix();
    texture.translate( halfwin[0], halfwin[1], 0 );
    texture.scale( model_scale[0], model_scale[1], model_scale[2] );
    texture.rotateX( model_rotation[0] );
    texture.rotateY( model_rotation[1] );
    texture.rotateZ( model_rotation[2] );
    texture.rotateX( PI * 0.5 );
    texture.strokeWeight( 1 / model_scale[0] );
    r_map.displayids( true );
    r_map.draw( texture, drawsize, drawsize );
    r_map.displayids( false );
    texture.translate( 0, 0, -0.2 );
    for ( int i = 0; i < r_flotte.length; i++ ) {
      r_flotte[ i ].enable_id_display = true;
      r_flotte[ i ].draw( texture, drawsize, drawsize );
      r_flotte[ i ].enable_id_display = false;
    }
    texture.popMatrix();
    texture.endDraw();
    
    selected.beginDraw();
    selected.noLights();
    selected.background( 0,0,0,0 );
    selected.perspective(fov, float(width)/float(height), cameraZ/10.0, cameraZ*1000.0);
    selected.pushMatrix();
    selected.translate( halfwin[0], halfwin[1], 0 );
    selected.scale( model_scale[0], model_scale[1], model_scale[2] );
    selected.rotateX( model_rotation[0] );
    selected.rotateY( model_rotation[1] );
    selected.rotateZ( model_rotation[2] );
    selected.rotateX( PI * 0.5 );
    r_map.translate( selected, drawsize, drawsize );
    if ( selected_cell != null ) {
      int c = selected_cell.rgbID;
      selected.strokeWeight( 3 / model_scale[0] );
      selected_cell.rgbID = color( 255 );
      r_map.drawCell( selected, selected_cell.id, drawsize, drawsize );
      selected_cell.rgbID = c;
    }
    if ( selected_building != null ) {
      Building b = ((Building) selected_building);
      int c = b.rgbID;
      b.rgbID = color( 255 );
      b.enable_id_display = true;
      b.draw( selected, drawsize, drawsize );
      b.enable_id_display = false;
      b.rgbID = c;
    }
    if ( selected_vehicule != null ) {
      selected.translate( 0, 0, -0.2 );
      Vehicule v = ((Vehicule) selected_vehicule);
      int c = v.rgbID;
      v.rgbID = color( 255 );
      v.enable_id_display = true;
      v.draw( selected, drawsize, drawsize );
      v.enable_id_display = false;
      v.rgbID = c;
    }
    selected.popMatrix();
    selected.endDraw();
    
  }
  
  public void select( float x, float y ) {
    
    selected_cell = null;
    selected_building = null;
    selected_vehicule = null;
    
    texture.loadPixels();
    int px = int(x * texture.width);
    int py = int(y * texture.height);
    int id = px + py * texture.width;
    if ( id < 0 || id >= texture.pixels.length) {
      return;
    }
    // color analysis
    int rgb = texture.pixels[ id ];
    float r = red(rgb);
    float g = green(rgb);
    float b = blue(rgb);
    if ( r >= 254 ) {
      for ( IDObject io : cells ) {
        if ( io.rgbID == rgb ) {
          selected_cell = io;
          return;
        }
      }
      //println( "cell!" );
    } else if ( g >= 254 ) {
      for ( IDObject io : buildings ) {
        if ( io.rgbID == rgb ) {
          selected_building = io;
          return;
        }
      }
      //println( "buidling!" );
    } else if ( b >= 254 ) {
      for ( IDObject io : vehicules ) {
        if ( io.rgbID == rgb ) {
          selected_vehicule = io;
          return;
        }
      }
      //println( "vehicule!" );
    }
  }

  public void draw(float x, float y, float w, float h) {
    image( texture, x, y, w, h );
  }
  
  public void overlay() {
    tint( 255, 120 );
    image( selected, 0, 0); 
  }
  
  public void click() {
    if ( selected_cell != null ) {
      r_map.buildHere( selected_cell.id );
      refresh();
    } else if ( selected_building != null ) {
      r_map.unBuild( ((Building) selected_building) );
      refresh();
    }
  }
  
}  