
public static final int CELL_LOCATION_DEFAULT =        0;
public static final int CELL_LOCATION_BORDER =         1;

public static final int CELL_TYPE_DEFAULT =            0;
public static final int CELL_TYPE_ROAD =               1;
public static final int CELL_TYPE_BUILDING =           2;

public static final int CLOC =                         0;
public static final int CTYPE =                        1;
public static final int CTOP =                         2;
public static final int CRIGHT =                       3;
public static final int CBOTTOM =                      4;
public static final int CLEFT =                        5;
public static final int CCOL =                         6;
public static final int CROW =                         7;

public static final int ROAD_NUM =                     10;
public static final int ROAD_MIN_STRAIGHT_LENGTH =     30;

public static final float STREET_SIZE =                0.7f;

public static final int BUILDING_NUM =                 120;

class Map extends IDObject {

  int cols;
  int rows;
  int[][] grid;
  float[] cell;
    ArrayList< IDObject > cellIds;

  ArrayList< Building > buildings;

  public Map( int cols, int rows ) {

    this.cols = cols;
    this.rows = rows;
    grid = new int[ cols * rows ][8];
    // 0: bord ou pas
    // 1: type de cellule

    // 2: connecteur top
    // 3: connecteur right
    // 4: connecteur bottom
    // 5: connecteur left

    // 6: column
    // 7: row

    cell = new float[] { 
      ( 1.f / cols ), ( 1.f / rows )
    };
    cellIds = null;

    buildings = new ArrayList< Building >();

    init();
  }

  public void init() {

    buildings.clear();

    int g = 0;
    for ( int r = 0; r < rows; r++ ) {
      for ( int c = 0; c < cols; c++ ) {

        grid[ g ][ CTYPE ] = CELL_TYPE_DEFAULT;
        grid[ g ][ CTOP ] = 0;
        grid[ g ][ CRIGHT ] = 0;
        grid[ g ][ CBOTTOM ] = 0;
        grid[ g ][ CLEFT ] = 0;
        grid[ g ][ CCOL ] = c;
        grid[ g ][ CROW ] = r;

        if ( c == 0 ) {
          grid[ g ][ CLOC ] = CELL_LOCATION_BORDER;
          grid[ g ][ CLEFT ] = -1;
        } 
        else if ( c == cols-1 ) {
          grid[ g ][ CLOC ] = CELL_LOCATION_BORDER;
          grid[ g ][ CRIGHT ] = -1;
        }

        if ( r == 0 ) {
          grid[ g ][ CLOC ] = CELL_LOCATION_BORDER;
          grid[ g ][ CTOP ] = -1;
        } 
        else if ( r == rows-1 ) {
          grid[ g ][ CLOC ] = CELL_LOCATION_BORDER;
          grid[ g ][ CBOTTOM ] = -1;
        }

        g++;
      }
    }

    for ( int i = 0; i < ROAD_NUM; i++ )
      buildRoads();

    buildBuildings();
  }

  public void buildRoads() {

    // select a cell on the border
    float r = random( 0, 1 );
    int[] pos = new int[] { 
      0, 0
    };
    int[] lastpos = new int[] { 
      0, 0
    };
    int[] dir = new int[] { 
      0, 0
    };

    if ( r < 0.25) {
      // top row
      pos[ 0 ] =(int) random( 1, cols-1 );
      pos[ 1 ] = 0;
      dir[ 1 ] = 1;
    } 
    else if ( r < 0.5 ) {
      // right column
      pos[ 0 ] = cols-1;
      pos[ 1 ] = (int) random( 1, rows-1 );
      dir[ 0 ] = -1;
    } 
    else if ( r < 0.75) {
      // bottom row
      pos[ 0 ] = (int) random( 1, cols-1 );
      pos[ 1 ] = rows-1;
      dir[ 1 ] = -1;
    } 
    else {
      // left column
      pos[ 0 ] = 0;
      pos[ 1 ] = (int) random( 1, rows-1 );
      dir[ 0 ] = 1;
    }

    lastpos[ 0 ] = pos[ 0 ];
    lastpos[ 1 ] = pos[ 1 ];

    adaptCellForRoad( pos, lastpos, dir );

    pos[ 0 ] += dir[ 0 ];
    pos[ 1 ] += dir[ 1 ];

    int ci = getCellIndex( pos );
    int lasti = ci;
    int canchangein = ROAD_MIN_STRAIGHT_LENGTH;

    while ( grid[ ci ][ CLOC ] != CELL_LOCATION_BORDER ) {

      adaptCellForRoad( pos, lastpos, dir );

      canchangein--;
      // changements de direction
      if ( canchangein <= 0 && random( 0, 1 ) > 0.5 ) {

        if ( dir[ 0 ] == 0 ) {
          dir[ 1 ] = 0;
          if ( random( 0, 1 ) >= 0.5 )
            dir[ 0 ] = -1;
          else
            dir[ 0 ] = 1;
        } 
        else {
          dir[ 0 ] = 0;
          if ( random( 0, 1 ) >= 0.5 )
            dir[ 1 ] = -1;
          else
            dir[ 1 ] = 1;
        }
        canchangein = ROAD_MIN_STRAIGHT_LENGTH;
      }

      lastpos[ 0 ] = pos[ 0 ];
      lastpos[ 1 ] = pos[ 1 ];

      pos[ 0 ] += dir[ 0 ];
      pos[ 1 ] += dir[ 1 ];
      ci = getCellIndex( pos );
    }

    // on n'a pas eu le temps de marquer la cellule du bord
    adaptCellForRoad( pos, lastpos, dir );
  }

  private void adaptCellForRoad( int[] pos, int[] lastpos, int[] dir ) {

    int ci = getCellIndex( pos );
    int li = getCellIndex( lastpos );
    int ctype = grid[ ci ][ CTYPE ];
    int cborder = grid[ ci ][ CLOC ];
    int lborder = grid[ li ][ CLOC ];

    if ( ctype != CELL_TYPE_ROAD )
      grid[ ci ][ 1 ] = CELL_TYPE_ROAD;

    if ( dir[ 0 ] == 1 ) {
      if ( grid[ ci ][ CLEFT ] == -1 )
        grid[ ci ][ CRIGHT ] = 1;
      else
        grid[ ci ][ CLEFT ] = 1;
    }

    if ( dir[ 0 ] == -1 ) {
      if ( grid[ ci ][ CRIGHT ] == -1 )
        grid[ ci ][ CLEFT ] = 1;
      else
        grid[ ci ][ CRIGHT ] = 1;
    }

    if ( dir[ 1 ] == 1 ) {
      if ( grid[ ci ][ CTOP ] == -1 )
        grid[ ci ][ CBOTTOM ] = 1;
      else
        grid[ ci ][ CTOP ] = 1;
    }

    if ( dir[ 1 ] == -1 ) {
      if ( grid[ ci ][ CBOTTOM ] == -1 )
        grid[ ci ][ CTOP ] = 1;
      else
        grid[ ci ][ CBOTTOM ] = 1;
    }

    if ( lborder != CELL_LOCATION_BORDER ) {

      if ( lastpos[ 0 ] < pos[ 0 ] )
        grid[ li ][ CRIGHT ] = 1;

      if ( lastpos[ 0 ] > pos[ 0 ] )
        grid[ li ][ CLEFT ] = 1;

      if ( lastpos[ 1 ] > pos[ 1 ] )
        grid[ li ][ CTOP ] = 1;

      if ( lastpos[ 1 ] < pos[ 1 ] )
        grid[ li ][ CBOTTOM ] = 1;
    }
  }

  private int getCellIndex( int[] pos ) {
    return pos[ 0 ] + pos[ 1 ] * cols;
  }

  private void buildBuildings() {

    int tryouts = 0;
    ArrayList< Integer > possibilities = new ArrayList< Integer >();
  
    while ( buildings.size () <= BUILDING_NUM && tryouts < 1000 ) {

      // locating a road
      int p = (int) random( 0, grid.length );

      for ( int i = 0; i < grid.length; i++ ) {

        if ( grid[ p ][ CTYPE ] == CELL_TYPE_ROAD ) {
          possibilities.clear();
          // top:
          int tmpp = grid[ p ][ CCOL ] + ( grid[ p ][ CROW ] - 1 ) * cols;
          if ( grid[ p ][ CROW ] > 0 && grid[ tmpp ][ CTYPE ] == CELL_TYPE_DEFAULT )
            possibilities.add( tmpp );
          // right:
          tmpp = ( grid[ p ][ CCOL ] + 1 ) + ( grid[ p ][ CROW ] ) * cols;
          if ( grid[ p ][ CCOL ] < cols - 1 && grid[ tmpp ][ CTYPE ] == CELL_TYPE_DEFAULT )
            possibilities.add( tmpp );
          // bottom:
          tmpp = ( grid[ p ][ CCOL ] ) + ( grid[ p ][ CROW ] + 1 ) * cols;
          if ( grid[ p ][ CROW ] < rows - 1 && grid[ tmpp ][ CTYPE ] == CELL_TYPE_DEFAULT )
            possibilities.add( tmpp );
          // left:
          tmpp = ( grid[ p ][ CCOL ] - 1 ) + ( grid[ p ][ CROW ] ) * cols;
          if ( grid[ p ][ CROW ] > 0 && grid[ tmpp ][ CTYPE ] == CELL_TYPE_DEFAULT )
            possibilities.add( tmpp );

          if ( possibilities.size() > 0 ) {
            int sel = possibilities.get( (int) random( 0, possibilities.size() - 1 ) );
            Building b = new Building( this, sel );
            buildings.add( b );
            grid[ sel ][ CTYPE ] = CELL_TYPE_BUILDING;
          }
          break;
        }

        p++;
        if ( p >= grid.length  )
          p = 0;
      }

      tryouts++;
    }
  }
  
  public void buildHere( int i ) {
    int cy = i / rows;
    int cx = i - cy * cols;
    if ( grid[ i ][ CTYPE ] == CELL_TYPE_DEFAULT ) {
        Building b = new Building( this, i );
        buildings.add( b );
        grid[ i ][ CTYPE ] = CELL_TYPE_BUILDING;
    }
  }
  
  public void unBuild( Building b ) {
    grid[ b.cellid ][ CTYPE ] = CELL_TYPE_DEFAULT;
    buildings.remove( b );
  }

  public void translate(PGraphics target, float draww, float drawh) {
    target.rectMode( CORNER );
    target.translate( -draww * 0.5f, -drawh * 0.5f );
  }

  public void drawCell( PGraphics target, int i, float draww, float drawh ) {
  
    int cy = i / rows;
    int cx = i - cy * cols;
    target.noFill();
    target.stroke( cellIds.get( i ).rgbID );
    target.rect( cx * cell[0] * draww, cy * cell[1] * drawh, cell[0] * draww, cell[1] * drawh );
  
  }


  public void draw( PGraphics target, float draww, float drawh ) {

    target.noStroke();
    translate( target, draww, drawh );
    float gx = 0;
    float gy = 0;
    float cw = cell[0] * draww;
    float ch = cell[1] * drawh;
    
    if ( enable_id_display && cellIds != null ) {
      int i = 0;
      for ( int r = 0; r < rows; r++ ) {
        gx = 0;
        for ( int c = 0; c < cols; c++ ) {
          target.fill( cellIds.get( i ).rgbID );
          target.rect( gx * draww, gy * drawh, cw, ch );
          gx += cell[ 0 ];
          ++i;
        }
        gy += cell[ 1 ];
      }
      
    } else {
  
      for ( int r = 0; r < rows; r++ ) {
  
        gx = 0;
  
        for ( int c = 0; c < cols; c++ ) {
  
          int ci = c + ( r * cols );
          int cloc = grid[ ci ][ CLOC ];
          int ctype = grid[ ci ][ CTYPE ];
  
          switch( ctype ) {
  
          case CELL_TYPE_DEFAULT:
            if ( cloc == CELL_LOCATION_DEFAULT ) {
              target.fill( 50, 204, 20 );
            } else if ( cloc == CELL_LOCATION_BORDER ) {
              target.fill( 30, 160, 12 );
            }
            target.rect( gx * draww, gy * drawh, cw, ch );
            break;
  
          case CELL_TYPE_ROAD:
  
            int connectors = 0;
            if ( grid[ ci ][ CTOP ] == 1 )
              connectors++;
            if ( grid[ ci ][ CRIGHT ] == 1 )
              connectors++;
            if ( grid[ ci ][ CBOTTOM ] == 1 )
              connectors++;
            if ( grid[ ci ][ CLEFT ] == 1 )
              connectors++;
            
            target.fill( 255 );
  
            target.pushMatrix(); 
            target.translate( gx * draww + cw * 0.5f, gy * drawh + ch * 0.5f, 0 );
  
            if ( connectors == 1 ) {
  
              if ( grid[ ci ][ CLEFT ] == 1 )
                target.rotateZ( PI * 0.5f);
              else if ( grid[ ci ][ CRIGHT ] == 1 )
                target.rotateZ( -PI * 0.5f);
              else if ( grid[ ci ][ CTOP ] == 1 )
                target.rotateZ( PI );
              target.image( roads[ 0 ], -cw * 0.5f, -ch * 0.5f, cw, ch );
            
            } else if ( connectors == 2 ) {
  
              if ( 
              ( grid[ ci ][ CRIGHT ] == 1 && grid[ ci ][ CLEFT ] == 1 ) || 
                ( grid[ ci ][ CTOP ] == 1 && grid[ ci ][ CBOTTOM ] == 1 ) 
                ) {
  
                if ( grid[ ci ][ CRIGHT ] == 1 && grid[ ci ][ CLEFT ] == 1 )
                  target.rotateZ( PI * 0.5f);
                target.image( roads[ 1 ], -cw * 0.5f, -ch * 0.5f, cw, ch );
              } 
              else {
  
                if ( grid[ ci ][ CBOTTOM ] == 1 && grid[ ci ][ CLEFT ] == 1 )
                  target.rotateZ( PI * 0.5f);
                if ( grid[ ci ][ CTOP ] == 1 && grid[ ci ][ CRIGHT ] == 1 )
                  target.rotateZ( -PI * 0.5f);
                if ( grid[ ci ][ CTOP ] == 1 && grid[ ci ][ CLEFT ] == 1 )
                  target.rotateZ( PI );
                target.image( roads[ 4 ], -cw * 0.5f, -ch * 0.5f, cw, ch );
              }
            
            } else if ( connectors == 3 ) {
              
              if ( grid[ ci ][ CTOP ] != 1 )
                target.rotateZ( PI * 0.5f);
              else if ( grid[ ci ][ CBOTTOM ] != 1 )
                target.rotateZ( -PI * 0.5f);
              else if ( grid[ ci ][ CRIGHT ] != 1 )
                target.rotateZ( PI );
              
              target.image( roads[ 2 ], -cw * 0.5f, -ch * 0.5f, cw, ch );
             
            } else if ( connectors == 4 ) {
              
              target.image( roads[ 3 ], -cw * 0.5f, -ch * 0.5f, cw, ch );
             
            } else {
  
              target.fill( 190 );
              target.rect( -cw * 0.5f, -ch * 0.5f, cw, ch );
            }
  
            target.popMatrix(); 
            break;
  
          case CELL_TYPE_BUILDING:
            target.fill( 180 );
            target.rect( gx * draww, gy * drawh, cw, ch );
            break;
  
          default:
            target.noFill();
            break;
          }
  
          float[] cc = new float[] { 
            gx * draww + cw * 0.5f, gy * drawh + ch * 0.5f
          };
  
          gx += cell[ 0 ];
        }
  
        gy += cell[ 1 ];
      }
    
    }

    for ( int b = 0; b < buildings.size(); b++ )
      buildings.get( b ).draw( target, draww, drawh );
  }
  
  public void displayids( boolean enabled ) {
    enable_id_display = enabled;
    for ( int b = 0; b < buildings.size(); b++ )
      buildings.get( b ).enable_id_display = enabled;    
  }
  
}