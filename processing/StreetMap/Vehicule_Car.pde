
public static final int MODELE_BERLINE =     0;
public static final int MODELE_COUPE =       1;
public static final int MODELE_MONOSPACE =   2;

class Car extends Vehicule {

  int modele = MODELE_BERLINE;
  
  // forme de la voiture
  float avant_d = 0.5f;
  float capot_d = 0.53f;
  float capot_h = 0.23f;
  float parebrise_d = 1;
  float parebrise_h = 0.1f;
  float toit_d = 1;
  float toit_h = 0.35f;
  float coffre_d = 0.55f;
  float coffre_h = 0.12f;
  float arriere_d = 0.5f;
  
  public Car( Map map ) {
    
    super( map );
    paint = new int[] { 0,255,255 };
    size = new float[] { map.cell[0] * STREET_SIZE * 0.15f, map.cell[1] * STREET_SIZE * 0.35f, map.cell[0] * STREET_SIZE * 0.15f };
    
    tex_roof = texture_cars[ (int) random( 1, texture_cars.length ) ];
    
    // modele de base: berline
    float r = random( 0,1 );
    if ( r < 0.15 ) {
      modele = MODELE_COUPE;
      // les seuls à être rouge
      tex_roof = texture_cars[ 0 ];
      // modele coupé
      size[ 0 ] = map.cell[1] * STREET_SIZE * 0.17f;
      size[ 1 ] = map.cell[1] * STREET_SIZE * 0.38f;
      size[ 2 ] = map.cell[0] * STREET_SIZE * 0.11f;
      avant_d = 0.6f;
      capot_d = 0.65f;
      capot_h = 0.35f;
      parebrise_d = 1;
      parebrise_h = 0.15f;
      toit_d = 1;
      toit_h = 0.29f;
      coffre_d = 0.75f;
      coffre_h = 0.1f;
      arriere_d = 0.5f;
    } else if ( r < 0.4 ) {
      modele = MODELE_MONOSPACE;
      // modele monospace
      size[ 0 ] = map.cell[1] * STREET_SIZE * 0.17f;
      size[ 1 ] = map.cell[1] * STREET_SIZE * 0.38f;
      size[ 2 ] = map.cell[0] * STREET_SIZE * 0.19f;
      avant_d = 0.33f;
      capot_d = 0.65f;
      capot_h = 0.1f;
      parebrise_d = 0.95f;
      parebrise_h = 0.15f;
      toit_d = 4;
      toit_h = 0.68f;
      coffre_d = 0.75f;
      coffre_h = 0.05f;
      arriere_d = 0.4f;
    }
    
  }
  
  public void init() {
    
    super.init();
    switch ( modele ) {
      
      case MODELE_COUPE:
        speed = map.cell[0] * random( 0.023, 0.03 );
        break;
        
      case MODELE_MONOSPACE:
        speed = map.cell[0] * random( 0.018, 0.021 );
        break;
        
      default:
        speed = map.cell[0] * random( 0.018, 0.022 );
        break;
    }
    
  }
  
  public void drawMesh( PGraphics target ) {
  
    // target.fill( paint[ 0 ], paint[ 1 ], paint[ 2 ] );
    target.fill( 255 );
    
    // drawing the car in one step
    // w -> x, h -> y, d -> z & basdecaisse -> z
    
    //           d
    //         *
    //   w   *
    // *****
    // *   *
    // *   *
    // *   * h
    // *   *
    // *   *
    // *   *
    // *****
    
    // right side of the car
    float hw = w * 0.5f;
    float hh = h * 0.5f;
    float ch;
    
    target.beginShape( QUADS );
    if ( enable_id_display ) {
      target.fill( rgbID );
    } else {
      target.texture( tex_roof );
    }
    
    // calendre
    target.vertex( -hw, -hh, d * avant_d, 76, 15 );
    target.vertex( hw, -hh, d * avant_d, 124, 15 );
    target.vertex( hw, -hh, basdecaisse, 124, 0 );
    target.vertex( -hw, -hh, basdecaisse, 76, 0 );
    
    // capot
    ch = -hh;
    target.vertex( hw, ch, d * avant_d, 124, 15 );
    ch += capot_h * h;
    target.vertex( hw, ch, d * capot_d, 124, 55 );
    target.vertex( -hw, ch, d * capot_d, 76, 55 );
    ch -= capot_h * h;
    target.vertex( -hw, ch, d * avant_d, 76, 15 );
    
    // parebrise
    ch += capot_h * h;
    target.vertex( hw, ch, d * capot_d, 124, 55 );
    ch += parebrise_h * h;
    target.vertex( hw, ch, d * parebrise_d, 124, 95 );
    target.vertex( -hw, ch, d * parebrise_d, 76, 95 );
    ch -= parebrise_h * h;
    target.vertex( -hw, ch, d * capot_d, 76, 55 );
    
    // toit
    ch += parebrise_h * h;
    target.vertex( hw, ch, d * parebrise_d, 124, 95 );
    ch += toit_h * h;
    target.vertex( hw, ch, d * toit_d, 124, 145 );
    target.vertex( -hw, ch, d * toit_d, 76, 145 );
    ch -= toit_h * h;
    target.vertex( -hw, ch, d * parebrise_d, 76, 95 );
    
    // lunette arriere
     ch += toit_h * h;
    target.vertex( hw, ch, d * toit_d, 124, 145 );
    ch += coffre_h * h;
    target.vertex( hw, ch, d * coffre_d, 124, 185 );
    target.vertex( -hw, ch, d * coffre_d, 76, 185 );
    ch -= coffre_h * h;
    target.vertex( -hw, ch, d * toit_d, 76, 145 );
    
    // coffre
    ch += coffre_h * h;
    target.vertex( hw, ch, d * coffre_d, 124, 185 );
    target.vertex( hw, hh, d * arriere_d, 124, 215 );
    target.vertex( -hw, hh, d * arriere_d, 76, 215 );
    ch -= coffre_h * h;
    target.vertex( -hw, ch, d * coffre_d, 76, 185 );
    
    // arriere
    target.vertex( -hw, hh, d * arriere_d, 76, 215 );
    target.vertex( hw, hh, d * arriere_d, 124, 215 );
    target.vertex( hw, hh, basdecaisse, 124, 230 );
    target.vertex( -hw, hh, basdecaisse, 76, 230 );
    
    target.endShape();
    
    target.beginShape();
    if ( enable_id_display ) {
      target.fill( rgbID );
    } else {
      target.texture( tex_roof );
    }
    
    // right
    //            c       d
    //            *********
    //          *           *       
    // a    b *               * e f 
    // ******                   ***
    // *                          *
    // ****************************
    // h                          g
    ch = -hh;
    target.vertex( hw, ch, d * avant_d, 164, 15 );
    ch += capot_h * h;
    target.vertex( hw, ch, d * capot_d, 164, 54 );
    ch += parebrise_h * h;
    target.vertex( hw, ch, d * parebrise_d, 125, 95 );
    ch += toit_h * h;
    target.vertex( hw, ch, d * toit_d, 125, 143 );
    ch += coffre_h * h;
    target.vertex( hw, ch, d * coffre_d, 164, 183 );
    target.vertex( hw, hh, d * arriere_d, 164, 214 );
    target.vertex( hw, hh, basdecaisse, 200, 214 );
    target.vertex( hw, -hh, basdecaisse, 200, 15 );
    
    target.endShape();
    
    // left
    target.beginShape();
    if ( enable_id_display ) {
      target.fill( rgbID );
    } else {
      target.texture( tex_roof );
    }
    ch = -hh;
    target.vertex( -hw, ch, d * avant_d, 36, 15 );
    ch += capot_h * h;
    target.vertex( -hw, ch, d * capot_d, 36, 54 );
    ch += parebrise_h * h;
    target.vertex( -hw, ch, d * parebrise_d, 75, 95 );
    ch += toit_h * h;
    target.vertex( -hw, ch, d * toit_d, 75, 143 );
    ch += coffre_h * h;
    target.vertex( -hw, ch, d * coffre_d, 36, 183 );
    target.vertex( -hw, hh, d * arriere_d, 36, 214 );
    target.vertex( -hw, hh, basdecaisse, 0, 214 );
    target.vertex( -hw, -hh, basdecaisse, 0, 15 );
    target.endShape();
    
    /*
    target.beginShape( QUADS );
    target.texture( tex_roof );
        
    // front
    target.vertex( -hw, -hh, d * avant_d );
    target.vertex( hw, -hh, d * avant_d );
    target.vertex( hw, -hh, basdecaisse );
    target.vertex( -hw, -hh, basdecaisse );
    
    target.vertex( -hw, hh, d * arriere_d );
    target.vertex( hw, hh, d * arriere_d );
    target.vertex( hw, hh, basdecaisse );
    target.vertex( -hw, hh, basdecaisse );
    
    target.endShape();
    */
    
    // shadow
    if ( !enable_id_display ) {
      target.fill( 0 );
      target.beginShape();
        target.vertex( -w*0.5f, -h*0.5f, 0 );
        target.vertex( w*0.5f, -h*0.5f, 0 );
        target.vertex( w*0.5f, h*0.5f, 0 );
        target.vertex( -w*0.5f, h*0.5f, 0 );
      target.endShape();
    }
    
  }

}