
public static final int TARGET_NONE =       0;
public static final int TARGET_CENTER =     1;
public static final int TARGET_TOP =        2;
public static final int TARGET_RIGHT =      3;
public static final int TARGET_BOTTOM =     4;
public static final int TARGET_LEFT =       5;

class Vehicule extends IDObject {

  Map map;
  float[] pos;
  float[] target;
  int target_from;
  int target_to;
  float[] size;
  float speed;
  int[] paint;
  
  int[] cell;
  
  float w;
  float h;
  float d;
  float basdecaisse;
  
  PImage tex_roof = null;
  PImage tex_front = null;
  PImage tex_right = null;
  PImage tex_back = null;
  PImage tex_left = null;
  PImage tex_shadow = null;
  
  public Vehicule( Map map ) {
  
    this.map = map;
    pos = new float[2];
    target = new float[2];
    target_from = TARGET_NONE;
    target_to = TARGET_NONE;
    
    size = new float[] { map.cell[0] * STREET_SIZE * 0.3f, map.cell[1] * STREET_SIZE * 0.3f, map.cell[0] * STREET_SIZE * 0.3f };
    cell = new int[] { 0,0 };
    paint = new int[] { 255, 0,0 };
  
    init();
  
  }
  
  public void init() {
    
    speed = map.cell[0] * random( 0.02, 0.03 );
    
    // recherche d'une "entree" de route sur la carte
    int p = (int) random( 0, map.grid.length );
    for ( int i = 0; i < map.grid.length; i++ ) {
      if ( 
        map.grid[ p ][ CLOC ] == CELL_LOCATION_BORDER && 
        map.grid[ p ][ CTYPE ] == CELL_TYPE_ROAD ) {
        
        cell[ 0 ] = map.grid[ p ][ CCOL ];
        cell[ 1 ] = map.grid[ p ][ CROW ];
          
        pos[ 0 ] = ( map.grid[ p ][ CCOL ] * map.cell[0] ) + map.cell[0] * 0.5f;
        pos[ 1 ] = ( map.grid[ p ][ CROW ] * map.cell[1] ) + map.cell[1] * 0.5f;
        
        target[ 0 ] = pos[ 0 ];
        target[ 1 ] = pos[ 1 ];
        
        target_from = TARGET_CENTER;
        
        if ( map.grid[ p ][ CTOP ] == 1 ) {
          target[ 1 ] -= map.cell[1] * 0.5f;
          target_to = TARGET_TOP;
        }
        
        if ( map.grid[ p ][ CRIGHT ] == 1 ) {
          target[ 0 ] += map.cell[0] * 0.5f;
          target_to = TARGET_RIGHT;
        }
        
        if ( map.grid[ p ][ CBOTTOM ] == 1 ) {
          target[ 1 ] += map.cell[1] * 0.5f;
          target_to = TARGET_BOTTOM;
        }
        
        if ( map.grid[ p ][ CLEFT ] == 1 ) {
          target[ 0 ] -= map.cell[0] * 0.5f;
          target_to = TARGET_LEFT;
        }
        
        break;
      }
      p++;
      if ( p > map.grid.length - 1 )
        p = 0;
    }

  }
  
  public void update() {
    
    boolean targetreached = true;
    
    if ( target_to == TARGET_LEFT && target[ 0 ] < pos[ 0 ] ) {
      
      pos[ 0 ] -= speed;
      targetreached = false;
      
    } else if ( target_to == TARGET_RIGHT && target[ 0 ] > pos[ 0 ] ) {
      
      pos[ 0 ] += speed;
      targetreached = false;
      
    } else if ( target_to == TARGET_TOP && target[ 1 ] < pos[ 1 ] ) {
      
      pos[ 1 ] -= speed;
      targetreached = false;
      
    } else if ( target_to == TARGET_BOTTOM &&  target[ 1 ] > pos[ 1 ] ) {
      
      pos[ 1 ] += speed;
      targetreached = false;
      
    } else if ( target_to == TARGET_CENTER ) {
    
      if ( dist( target[ 0 ], target[ 1 ], pos[ 0 ], pos[ 1 ] ) > speed ) {
        if ( target[ 0 ] < pos[ 0 ] )
          pos[ 0 ] -= speed;
        if ( target[ 0 ] > pos[ 0 ] )
          pos[ 0 ] += speed;
        if ( target[ 1 ] < pos[ 1 ] )
          pos[ 1 ] -= speed;
        if ( target[ 1 ] > pos[ 1 ] )
          pos[ 1 ] += speed;
        targetreached = false;
      }
      
    }
    
    if ( targetreached ) {
      
      // choix d'un nouvel objectif
      int nexttarget = TARGET_NONE;
      
      switch( target_to ) {
        
        case TARGET_TOP:
        case TARGET_RIGHT:
        case TARGET_BOTTOM:
        case TARGET_LEFT:
          nexttarget = TARGET_CENTER;
          break;
        
        case TARGET_CENTER:
          nexttarget = TARGET_NONE;
          break;

      }
      
      // cellule suivante dans la direction
      if ( nexttarget == TARGET_CENTER ) {
        
        if ( target_to == TARGET_TOP )
          cell[ 1 ] -= 1;
        if ( target_to == TARGET_RIGHT )
          cell[ 0 ] += 1;
        if ( target_to == TARGET_BOTTOM )
          cell[ 1 ] += 1;
        if ( target_to == TARGET_LEFT )
          cell[ 0 ] -= 1;
        
        int p = cell[ 0 ] + cell[ 1 ] * map.cols;
        target[ 0 ] = ( map.grid[ p ][ CCOL ] * map.cell[0] ) + map.cell[0] * 0.5f;
        target[ 1 ] = ( map.grid[ p ][ CROW ] * map.cell[1] ) + map.cell[1] * 0.5f;
        
        if ( target_to == TARGET_TOP )
          target_from = TARGET_BOTTOM;
        else if ( target_to == TARGET_RIGHT )
          target_from = TARGET_LEFT;
        else if ( target_to == TARGET_BOTTOM )
          target_from = TARGET_TOP;
        else if ( target_to == TARGET_LEFT )
          target_from = TARGET_RIGHT;
        
      } else {
      
        // choix d'une des directions possibles dans la cellule courante
        pos[ 0 ] = target[ 0 ];
        pos[ 1 ] = target[ 1 ];
        
        
        int p = cell[ 0 ] + cell[ 1 ] * map.cols;
        target[ 0 ] = ( map.grid[ p ][ CCOL ] * map.cell[0] ) + map.cell[0] * 0.5f;
        target[ 1 ] = ( map.grid[ p ][ CROW ] * map.cell[1] ) + map.cell[1] * 0.5f;
        
        // quelles sont les aletrnatives?
        ArrayList< Integer > tmpdirs = new ArrayList< Integer >();
        if ( map.grid[ p ][ CTOP ] == 1 && target_from != TARGET_TOP )
          tmpdirs.add( TARGET_TOP );
        if ( map.grid[ p ][ CRIGHT ] == 1 && target_from != TARGET_RIGHT )
          tmpdirs.add( TARGET_RIGHT );
        if ( map.grid[ p ][ CBOTTOM ] == 1 && target_from != TARGET_BOTTOM )
          tmpdirs.add( TARGET_BOTTOM );
        if ( map.grid[ p ][ CLEFT ] == 1 && target_from != TARGET_LEFT )
          tmpdirs.add( TARGET_LEFT );
        
        if ( tmpdirs.size() == 0 ) {
          nexttarget = target_from;
        } else {
          nexttarget = tmpdirs.get( (int) random( 0, tmpdirs.size() ) );
        }
        
        if ( nexttarget == TARGET_TOP )
          target[ 1 ] -= map.cell[1] * 0.5f;
        else if ( nexttarget == TARGET_RIGHT )
          target[ 0 ] += map.cell[0] * 0.5f;
        else if ( nexttarget == TARGET_BOTTOM )
          target[ 1 ] += map.cell[1] * 0.5f;
        if ( nexttarget == TARGET_LEFT )
          target[ 0 ] -= map.cell[0] * 0.5f;
          
        target_from = target_to;
        
      }   
      
      target_to = nexttarget;
      
    }
    
  }
  
  public void draw( PGraphics target, float draww, float drawh ) {
  
    // de quel coté de la route?
    // si de gauche > centre ou centre > droite -> en dessous
    // si de droite > centre ou centre > gauche -> au-dessus
    // si de bas > centre ou centre > haut -> à droite
    // si de haut > centre ou centre > bas -> à gauche
    float[] decal = new float[] { 0,0 };
    float rotation = 0;
    if ( target_from == TARGET_LEFT || target_to == TARGET_RIGHT ) {
      decal[ 1 ] = map.cell[1] * STREET_SIZE * 0.24f;
      rotation = PI * 0.5f;
    }
    if ( target_from == TARGET_RIGHT || target_to == TARGET_LEFT ) {
      decal[ 1 ] = -map.cell[1] * STREET_SIZE * 0.24f;
      rotation = -PI * 0.5f;
    }
    if ( target_from == TARGET_TOP || target_to == TARGET_BOTTOM ) {
      decal[ 0 ] = -map.cell[0] * STREET_SIZE * 0.24f;
      rotation = PI;
    }
    if ( target_from == TARGET_BOTTOM || target_to == TARGET_TOP ) {
      decal[ 0 ] = map.cell[0] * STREET_SIZE * 0.24f;
    }
    target.pushMatrix();
    target.translate( 
      ( decal[ 0 ] + pos[ 0 ] ) * draww, 
      ( decal[ 1 ] + pos[ 1 ] ) * drawh, 
      0.2f );
    target.rotateZ( rotation );
    target.noStroke();
    
    w = size[0] * draww;
    h = size[1] * drawh;
    d = size[2] * draww;
    basdecaisse = map.cell[0] * 0.02 * draww;
    
    drawMesh(target);
    
    target.popMatrix();
       
  }
  
  public void drawMesh( PGraphics target ) {
    
    // roof
    if ( enable_id_display ) {
      target.fill( rgbID );
      target.beginShape();
        target.vertex( -w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, d );
      target.endShape();
    } else if ( tex_roof == null ) {
      target.fill( paint[ 0 ], paint[ 1 ], paint[ 2 ] );
      target.beginShape();
        target.vertex( -w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, d );
      target.endShape();
    } else {
      target.fill( 255 );
      target.beginShape();
        target.texture( tex_roof );
        target.vertex( -w*0.5f, -h*0.5f, d,   0, 0 );
        target.vertex( w*0.5f, -h*0.5f, d,   tex_roof.width, 0 );
        target.vertex( w*0.5f, h*0.5f, d,     tex_roof.width, tex_roof.height );
        target.vertex( -w*0.5f, h*0.5f, d,   0, tex_roof.height );
      target.endShape();
    }

    // left
    if ( enable_id_display ) {
      target.fill( rgbID );
      target.beginShape();
        target.vertex( -w*0.5f, -h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, basdecaisse );
        target.vertex( -w*0.5f, -h*0.5f, basdecaisse );
      target.endShape();
    } else if ( tex_left == null ) {
      target.fill( paint[ 0 ], paint[ 1 ], paint[ 2 ] );
      target.beginShape();
        target.vertex( -w*0.5f, -h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, basdecaisse );
        target.vertex( -w*0.5f, -h*0.5f, basdecaisse );
      target.endShape();
    } else {
      target.fill( 255 );
      target.beginShape();
        target.texture( tex_left );
        target.vertex( -w*0.5f, -h*0.5f, d, 0, 0 );
        target.vertex( -w*0.5f, h*0.5f, d, tex_left.width, 0 );
        target.vertex( -w*0.5f, h*0.5f, basdecaisse, tex_left.width, tex_left.height );
        target.vertex( -w*0.5f, -h*0.5f, basdecaisse, 0, tex_left.height );
      target.endShape();
    }

    // front
    if ( enable_id_display ) {
      target.fill( rgbID );
      target.beginShape();
        target.vertex( -w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, -h*0.5f, basdecaisse );
        target.vertex( -w*0.5f, -h*0.5f, basdecaisse );
      target.endShape();
    } else if ( tex_left == null ) {
      target.fill( paint[ 0 ], paint[ 1 ], paint[ 2 ] );
      target.beginShape();
        target.vertex( -w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, -h*0.5f, basdecaisse );
        target.vertex( -w*0.5f, -h*0.5f, basdecaisse );
      target.endShape();
    } else {
      target.fill( 255 );
      target.beginShape();
        target.texture( tex_front );
        target.vertex( -w*0.5f, -h*0.5f, d, 0, 0 );
        target.vertex( w*0.5f, -h*0.5f, d, tex_left.width, 0 );
        target.vertex( w*0.5f, -h*0.5f, basdecaisse, tex_left.width, tex_left.height );
        target.vertex( -w*0.5f, -h*0.5f, basdecaisse, 0, tex_left.height );
      target.endShape();
    }
    
    // right
    if ( enable_id_display ) {
      target.fill( rgbID );
      target.beginShape();
        target.vertex( w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, h*0.5f, d );
        target.vertex( w*0.5f, h*0.5f, basdecaisse );
        target.vertex( w*0.5f, -h*0.5f, basdecaisse );
      target.endShape();
    }  else if ( tex_right == null ) {
      target.fill( paint[ 0 ], paint[ 1 ], paint[ 2 ] );
      target.beginShape();
        target.vertex( w*0.5f, -h*0.5f, d );
        target.vertex( w*0.5f, h*0.5f, d );
        target.vertex( w*0.5f, h*0.5f, basdecaisse );
        target.vertex( w*0.5f, -h*0.5f, basdecaisse );
      target.endShape();
    } else {
      target.fill( 255 );
      target.beginShape();
        target.texture( tex_right );
        target.vertex( w*0.5f, -h*0.5f, d, tex_right.width, 0 );
        target.vertex( w*0.5f, h*0.5f, d, 0, 0 );
        target.vertex( w*0.5f, h*0.5f, basdecaisse, 0, tex_right.height );
        target.vertex( w*0.5f, -h*0.5f, basdecaisse, tex_right.width, tex_right.height );
      target.endShape();
    }
    
    // back
    if ( enable_id_display ) {
      target.fill( rgbID );
      target.beginShape();
        target.vertex( w*0.5f, h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, basdecaisse );
        target.vertex( w*0.5f, h*0.5f, basdecaisse );
      target.endShape();
      return;
    } else if ( tex_back == null ) {
      target.fill( paint[ 0 ], paint[ 1 ], paint[ 2 ] );
      target.beginShape();
        target.vertex( w*0.5f, h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, d );
        target.vertex( -w*0.5f, h*0.5f, basdecaisse );
        target.vertex( w*0.5f, h*0.5f, basdecaisse );
      target.endShape();
    } else {
      target.fill( 255 );
      target.beginShape();
        target.texture( tex_back );
        target.vertex( w*0.5f, h*0.5f, d, 0, 0  );
        target.vertex( -w*0.5f, h*0.5f, d, tex_back.width, 0 );
        target.vertex( -w*0.5f, h*0.5f, basdecaisse, tex_back.width, tex_back.height );
        target.vertex( w*0.5f, h*0.5f, basdecaisse, 0, tex_back.height );
      target.endShape();
    }

    // shadow
    target.fill( 0 );
    target.beginShape();
      target.vertex( -w*0.5f, -h*0.5f, 0 );
      target.vertex( w*0.5f, -h*0.5f, 0 );
      target.vertex( w*0.5f, h*0.5f, 0 );
      target.vertex( -w*0.5f, h*0.5f, 0 );
    target.endShape();
    
  }

}