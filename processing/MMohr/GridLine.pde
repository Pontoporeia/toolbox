
class GridLine {

  public GridPoint p1;
  public GridPoint p2;
  
  public boolean horizontal;
  public boolean vertical;
  
  public boolean enabled;
  
  public GridLine( GridPoint p1, GridPoint p2 ) {
    
    this.p1 = p1;
    this.p2 = p2;
    horizontal = false;
    vertical = false;
    enabled = true;
    
    p1.setLine( this, p2 );
    p2.setLine( this, p1 );
    
  }
  
  public void draw( float white, float opacity ) {
    
    strokeWeight( 1 );
    stroke( white, opacity );
    line( p1.x, p1.y, p1.z, p2.x, p2.y, p2.z );
    
  }
  
  public void update() {
    
  }

}
