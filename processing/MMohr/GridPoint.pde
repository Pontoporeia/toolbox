
class GridPoint extends PVector {

  public int gridx;
  public int gridy;
  public PVector origin;
  public float size;
  public float vibration;
  public boolean locked;
  
  public GridLine[] connectedlines;
  // lines order
  //  0  -  1  -  2
  //     \  |  /
  //  7 --  p  -- 3
  //      /
  //  6  -  5  -  4
  
  public GridTriangle[] connectedtriangles;
  // triangles order
  //       ____
  //     /|   /|
  //    / | 1/ |
  //   / 0| / 2|
  //  /___p/___|
  // |   /|   /
  // |5 / | 3/
  // | / 4| /
  // |/___X/
  
  public GridPoint( int gridx, int gridy, float x, float y, float z ) {
    
    super( x,y,z );
    
    this.gridx = gridx;
    this.gridy = gridy;

    origin = new PVector( x,y,z );
    size = 1;
    vibration = 0;
    locked = false;
    
    connectedlines = new GridLine[ 8 ];
    for( int i = 0; i < 8; i++ ) {
      connectedlines[ i ] = null;
    }
    
    connectedtriangles = new GridTriangle[ 6 ];
    for( int i = 0; i < 6; i++ ) {
      connectedtriangles[ i ] = null;
    }
    
  }
  
  public void setLine( GridLine l, GridPoint otherpoint ) {
    if ( this == otherpoint ) {
      System.err.println( "Impossible to set the line!" );
      return;
    }
    if ( otherpoint.gridx == gridx ) {
      if ( otherpoint.gridy < gridy ) {
        connectedlines[ 1 ] = l;
      } else {
        connectedlines[ 5 ] = l;
      }
    } else if ( otherpoint.gridy == gridy ) {
      if ( otherpoint.gridx < gridx ) {
        connectedlines[ 7 ] = l;
      } else {
        connectedlines[ 3 ] = l;
      }
    } else if ( otherpoint.gridx < gridx && otherpoint.gridy < gridy ) {
        connectedlines[ 0 ] = l;
    } else if ( otherpoint.gridx > gridx && otherpoint.gridy < gridy ) {
        connectedlines[ 2 ] = l;
    } else if ( otherpoint.gridx > gridx && otherpoint.gridy > gridy ) {
        connectedlines[ 4 ] = l;
    } else if ( otherpoint.gridx < gridx && otherpoint.gridy > gridy ) {
        connectedlines[ 6 ] = l;
    }
  }
  
  public void setTriangle( GridTriangle t ) {
    if ( this == t.p1 && t.p2.gridx == gridx ) {
      connectedtriangles[ 4 ] = t;
    } else if ( this == t.p1 && t.p2.gridy == gridy ) {
      connectedtriangles[ 3 ] = t;
    } else if ( this == t.p2 && t.p1.gridx == gridx ) {
      connectedtriangles[ 0 ] = t;
    } else if ( this == t.p2 && t.p1.gridy == gridy ) {
      connectedtriangles[ 5 ] = t;
    } else if ( this == t.p3 && t.p1.gridx == gridx ) {
      connectedtriangles[ 1 ] = t;
    } else if ( this == t.p3 && t.p2.gridy == gridy ) {
      connectedtriangles[ 2 ] = t;
    }
  }

  public void update() {
    
    if ( vibration != 0 && !locked ) {
      x += random( -vibration, vibration );
      y += random( -vibration, vibration );
      z += random( -vibration, vibration );
    } else {
      x += ( origin.x - x ) * 0.1;
      y += ( origin.y - y ) * 0.1;
      z += ( origin.z - z ) * 0.1;
    }
    
  }

}
