
class GridTriangle {

  public GridPoint p1;
  public GridPoint p2;
  public GridPoint p3;
  
  public boolean enabled;
  
  public GridTriangle( GridPoint p1, GridPoint p2, GridPoint p3 ) {
    
    // points have to be given in this order:
    //
    //       0
    //      /\
    //     /  \
    //    /    \
    //   /______\
    //  2        1
    //
    //      or
    //
    //  0      1
    //  ________
    //  \      /
    //   \    /
    //    \  /
    //     \/
    //      2
    //
    // 0: top (left)
    // 1: bottom (right)
    // 2: bottom (left)
    
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
    p1.setTriangle( this );
    p2.setTriangle( this );
    p3.setTriangle( this );
    enabled = true;
    
  }
  
  public void draw( float white, float opacity ) {
      noStroke();
      fill( white, opacity );
      beginShape(TRIANGLES);
      vertex( p1.x, p1.y, p1.z );
      vertex( p2.x, p2.y, p2.z );
      vertex( p3.x, p3.y, p3.z );
      endShape();
  }
  
  public void update() {
    
  }

}
