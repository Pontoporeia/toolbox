
class LineTraveller {

  public GridPoint target;
  public GridLine line;
  int time2live;
  int moveeach;
  
  int historySize;
  ArrayList< GridLine > history;
  
  public LineTraveller( GridPoint p ) {
    
    target = p;
    line = null;
    time2live = 50;
    moveeach = 1;
    historySize = 2;
    history = new ArrayList< GridLine >();
    
    selectNextLine();
    
  }
  
  private void selectNextLine() {
    
    if ( line != null ) {
      history.add( line );
      while ( history.size() > historySize ) {
        history.remove( 0 );
      }
    }
    
    // selecting next line
    ArrayList< GridLine > ls = new ArrayList< GridLine >();
    for ( int i = 0; i < target.connectedlines.length; i++ ) {
      if ( target.connectedlines[ i ] != null &&  target.connectedlines[ i ] != line ) {
        ls.add( target.connectedlines[ i ] );
      }
    }
    int r = (int) random( 0, ls.size() );
    if ( r == ls.size() ) {
      r = ls.size() - 1;
    }
    line = ls.get( r );
    if ( target == line.p1 ) {
      target = line.p2;
    } else {
      target = line.p1;
    }
  }
  
  public void update() {
    if ( time2live % moveeach == 0 ) {
      selectNextLine();
    }
    time2live--;
  }
  
  public void draw() {
    stroke( 255 );
    strokeWeight( line.p1.size + line.p2.size * 0.5 );
    line( line.p1.x, line.p1.y, line.p1.z, line.p2.x, line.p2.y, line.p2.z );
    for ( GridLine l : history ) {
      strokeWeight( l.p1.size + l.p2.size * 0.5 );
      line( l.p1.x, l.p1.y, l.p1.z, l.p2.x, l.p2.y, l.p2.z );
    }
  }


}
