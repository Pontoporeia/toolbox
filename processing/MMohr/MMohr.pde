// simple grid system
// drawing of points, lines and triangles
// based on Manfred Mohr studies

Grid g;
ArrayList< LineTraveller > ltravs;

void setup() {

  size( 1000, 500, P3D );
  smooth();
  
  g = new Grid( 50,25 );
  ltravs = new ArrayList< LineTraveller >();
  
}

void draw() {

  for ( int i = 0; i < ltravs.size(); i++ ) {
    LineTraveller lt = ltravs.get( i );
    lt.update();
    if ( lt.time2live == 0 ) {
      ltravs.remove( lt );
      i--;
    }
  }
  
  background( 5 );
  
  // drawing only diagonal lines
//  for ( int i = 0; i < g.lines.length; i++ ) {
//    GridLine l = g.lines[ i ];
//    if ( !l.vertical && !l.horizontal ) {
//      l.draw( 255, 255 );
//    }
//  }

  g.draw();
  
  GridPoint mp = g.getClosestPoint( mouseX, mouseY, 0 );
  
  // drawing lines and triangles linked to point
//  if ( mp != null ) {
//    for ( int i = 0; i < mp.connectedlines.length; i++ ) {
//      GridLine gl = mp.connectedlines[ i ];
//      if ( gl != null ) {
//        gl.draw( 255, 255 );
//      }
//    }
//    for ( int i = 0; i < mp.connectedtriangles.length; i++ ) {
//      GridTriangle gt = mp.connectedtriangles[ i ];
//      if ( gt != null ) {
//        gt.draw( 255, 200 );
//      }
//    }
//  }
  
  for ( LineTraveller lt : ltravs ) {
    lt.draw();
  }
  
  stroke( 255,0,0 );
  text( frameRate, 20, 20 );
  
}

void mousePressed() {
  GridPoint mp = g.getClosestPoint( mouseX, mouseY, 0 );
  for ( int i = 0; i < 5; i++ ) {
    ltravs.add( new LineTraveller( mp ) );
  }
}

void keyPressed() {
  if ( key == 'b' ) {
    g.toggleLockBorders();
  } else if ( key == 'v' ) {
    g.toggleVibrations();
  } else if ( key == 'm' ) {
    g.pointSizes( 1, 1 );
  } else if ( key == 'n' ) {
    g.pointSizes( 1, 5 );
  } else if ( key == 't' ) {
    g.toggleTriangles();
  } else if ( key == 'l' ) {
    g.toggleLines();
  } else if ( key == 'p' ) {
    g.togglePoints();
  }
}
