ArrayList<Particule> ps;
float diagonal;
float multiplier;
float multiplier_speed;
int pxgap = 2;

void setup() {
  size( 800, 600 );
  PImage im;
  im = loadImage( "img.png" );
  float im_diagonal = sqrt( pow( im.width, 2 ) + pow( im.height, 2 ) ) * 0.5;
  im.loadPixels();
  ps = new ArrayList<Particule>();
  for( int y = 0; y < im.height; y += pxgap ) {
    for( int x = 0; x < im.width; x += pxgap ) {
      int id = x + y * im.width;
      Particule p = new Particule();
      p.pos.x = ( x - im.width * 0.5 + 0.5 ) * 2;
      p.pos.y = ( y - im.height * 0.5 + 0.5 ) * 2;
      p.c = im.pixels[id];
      p.process( im_diagonal * 2 );
      ps.add( p );
    }
  }
  diagonal = sqrt( pow( width, 2 ) + pow( height, 2 ) ) * 0.5;
  multiplier = 0;
}

void draw() {

  float target_multiplier = diagonal - dist( width * 0.5, height * 0.5, mouseX, mouseY );
  float diff = target_multiplier - multiplier;
  multiplier_speed = multiplier_speed * 0.8 + diff * 0.3;
  multiplier += multiplier_speed;
  
  background( 0 );
  strokeWeight(4);
  pushMatrix();
  translate( width * 0.5, height * 0.5 );
  for( Particule p : ps ) {
    p.draw();
  }
  popMatrix();
  
  stroke( 255 );
  text( frameRate, 10, height - 25 );

}