class Particule {
  public PVector pos;
  public PVector dir;
  public float d;
  public int c;
  public Particule() {
    pos = new PVector();
    dir = new PVector();
  }
  public void process( float div ) {
    d = pos.mag() / div;
    dir.add( pos );
    dir.normalize();
    float a = atan2( dir.y, dir.x );
    dir.x = cos( a ) * ( d * 0.2 + ( 1 - d ) * 1.2 );
    dir.y = sin( a ) * ( d * 0.2 + ( 1 - d ) * 1.2 );
  }
  public void draw() {
    stroke( c );
    point( 
      pos.x + dir.x * d * multiplier, 
      pos.y + dir.y * d * multiplier
      );
  }
}