PreciseTimer precisetime;
PreciseTimer precisetime2;
int pt_counter;

void setup() {
  
  size( 800, 600 );

  boolean test_printer = false;
  if ( test_printer ) {
    // PRINTING
    // il faut d'abord lister les imprimantes disponibles, ensuite sélectionner la bonne et lancer l'impression
    // 1. > voir le nom des imprimantes disponibles
    listPrinters();
    // dans la console, le nom des imprimantes s'imprime
    // 2. récupérer l'imprimante par son nom, chez moi, c'est 'Hewlett-Packard-HP-Color-LaserJet-CP1515n'
    PrintService imprimante = getPrinterByName("Hewlett-Packard-HP-Color-LaserJet-CP1515n");
    // on teste si ça a fonctionné
    if ( imprimante != null ) {
      // 3. on lance l'impression en spécifiant quelle imprimante utiliser
      printText( imprimante, "Mon texte à imprimer" );
      // tu peux lancer autant d'impression que tu veux sur 'imprimante'
      // tu peux aussi créer une variable 'PrintService imprimante2' qui a un autre nom.
      // normalment...
    } else {
      println("Impossible de connecter cette imprimante!");
    }
  }
  
  precisetime = new PreciseTimer( this );
  precisetime.start( 30 );
  pt_counter = 0;
  
  precisetime2 = new PreciseTimer( this );
  precisetime2.start( 5 );
  
}

void threaded_update( PreciseTimer pt ) {
  if ( pt == precisetime2 ) {
    println( "I'm the second timer :) " + precisetime2.getCounter() ); 
  }
}

void draw() {

  background( 0 );
  fill( 255 );
  int ty = 30;
  text( "sketch is running at " + frameRate + " fps", 15, ty ); ty += 15;
  if ( precisetime.isActive() ) {
    text( "precise timer 1 is running at " + precisetime.getFps() + " fps", 15, ty ); ty += 15;
    text( "precise timer 1 last delta time " + precisetime.getDeltaTime() + " millis ~ " + ( precisetime.getDeltaTime() / 1000.f ) + " sec", 15, ty ); ty += 15;
    text( "precise timer 1 made " + ( precisetime.getCounter() - pt_counter ) + " updates since last draw", 15, ty ); ty += 15;
  } else {
    text( "precise timer 1 is not running", 15, ty ); ty += 15;
  }
  if ( precisetime2.isActive() ) {
    text( "precise timer 2 is running at " + precisetime2.getFps() + " fps", 15, ty ); ty += 15;
    text( "precise timer 2 last delta time " + precisetime2.getDeltaTime() + " millis ~ " + ( precisetime2.getDeltaTime() / 1000.f ) + " sec", 15, ty ); ty += 15;
  } else {
    text( "precise timer 2 is not running", 15, ty ); ty += 15;
  }
  pt_counter = precisetime.getCounter();
  
}

void keyPressed() {

  if ( key == 'f' ) {
    if ( precisetime.getFps() == 300 )
      precisetime.setFps( 10 );
    else
      precisetime.setFps( 300 );
  }
  
  if ( key == 's' ) {
    if ( precisetime.isActive() )
      precisetime.stop();
    else
      precisetime.start();
  }

}
