// ajout de zéros au début de la chaîne en fonction de la longueur définie
public String leadingZeros( int val, int len ) {
  String out = "";
  while( val < pow( 10, len - 1 ) && len > 1 ) {
    out += "0";
    len--;
  }
  out += val;
  return out;
}

// generation d'une chaine unique exprimant la date 
// dont chaque partie est préfixée d'un zéro si plus petit que 10
// (conserve l'ordre dans un gestionnaire de fichier)
public String timestamp() {
  int m = ( (int) millis() % 1000 );
  return leadingZeros( year(), 4 ) + "." +
    leadingZeros( month(), 2 ) + "." +
    leadingZeros( day(), 2 ) + "." +
    leadingZeros( hour(), 2 ) + "." +
    leadingZeros( minute(), 2 ) + "." +
    leadingZeros( second(), 2 ) + "." +
    leadingZeros( m, 3 );
}
