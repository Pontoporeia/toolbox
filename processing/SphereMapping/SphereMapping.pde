import saito.objloader.*;
import processing.video.*;

Movie mov;

PVector offset;

// declare that we need a OBJModel and we'll be calling it "model"
OBJModel model;

float rotX;
float rotY;

void setup() {
  
  size(displayWidth, displayHeight, P3D);

  // making an object called "model" that is a new instance of OBJModel
  model = new OBJModel( this, "sphere4.obj", "relative" );
  noStroke();
  
  mov = new Movie(this, "transit.mov");
  mov.loop();
  
  offset = new PVector();
  // model.shapeMode(LINES);
   

}


void movieEvent(Movie movie) {
  mov.read();
}

void draw()
{

  model.setTexture( mov );
  
  background(100);
  lights();

  pushMatrix();
  translate( width/2 + offset.x, height/2 + offset.y, offset.z );
  rotateX(rotY);
  rotateY(rotX);
  scale(1.7);
  //translate(0,-300);
 // stroke(255,0,0);
  rotateY(PI/2);
  model.draw();
  popMatrix();
 
}
  
void mouseDragged()
{
  rotX += (mouseX - pmouseX) * 0.01;
  rotY -= (mouseY - pmouseY) * 0.01;
}


void keyPressed() {

  if ( keyCode == UP ) {
    offset.z--;
  } else if ( keyCode == DOWN ) {
    offset.z++;
  } else if ( keyCode == LEFT ) {
    offset.x--;
  } else if ( keyCode == RIGHT ) {
    offset.x++;
  }

}
