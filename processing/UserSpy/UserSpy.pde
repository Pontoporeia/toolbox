// FIRST OF ALL: copy the folder "library/JNativeHook" in a folder
// "library" in your sketchbook folder. If you don't have one, just
// create it.
// Code allows you to listen to all user's input accross
// the whole system.
// Based on JNativeHook 1.1.4 library
// https://code.google.com/p/jnativehook/
// adapted by frankiezafe.org

// note: if you change the name of the sketch (not UserSpy anymore)
// you'll have to change the class of "parent" params in
// GlobalListener tab.

GlobalListener glistener;

void setup() {
  
  size( 600, 600 );
  glistener = new GlobalListener( this );
  
}

void draw() {
  
}

void nativeMousePressed( int x, int y, int button ) {}
void nativeMouseReleased( int x, int y, int button ) {}
void nativeMouseClicked( int x, int y, int button ) {}
void nativeMouseMoved( int x, int y ) {}
void nativeMouseDragged( int x, int y ) {}
void nativeMouseWheel( int rot ) {}

void nativeKeyPressed( int nk ) {}
void nativeKeyReleased( int nk ) {}
void nativeKeyTyped( int nk ) {}

