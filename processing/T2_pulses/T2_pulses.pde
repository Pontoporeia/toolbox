int last_time;
int delta_time;
float delta_seconds;

Pulser[] pulsers;

void setup () {

  last_time = 0;
  delta_time = 0;
  delta_seconds = 0;
  
  size( 600,600 );
  
  int grid = 3;
  int cellsize = 200;
  pulsers = new Pulser[grid*grid];
  int i = 0;
  for( int y = 0; y < grid; ++y ) {
  for( int x = 0; x < grid; ++x ) {
    pulsers[i] = new Pulser( x * cellsize, y * cellsize, cellsize, cellsize );
    pulsers[i].configure( i * 500, 1000 );
    ++i;
  }
  }
  
}

void draw() {

  // measuring elapsed time since last frame, in milliseconds
  int now = millis();
  int delta_time = now - last_time;
  delta_seconds = delta_time * 0.001;
  last_time = now;
  
  background( 0 );
  
  for( int i = 0; i < pulsers.length; ++i ) {
    pulsers[i].update( delta_time );
    pulsers[i].draw();
  }
  
}