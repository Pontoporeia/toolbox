class Pulser {

  public int[] pos;
  public int[] size;

  public int start_delay;
  public int frequency;
  private int current_millis;
  public boolean pulse;

  Pulser(int x, int y, int w, int h) {
    pos = new int[] {x, y};
    size = new int[] {w, h};
    configure( 0, 0 );
  }

  public void configure( int start, int freq ) {
    start_delay = start;
    frequency = freq;
    current_millis = 0;
    pulse = false;
  }

  public void update( int delta_time ) {
    if ( start_delay > 0 ) {
      start_delay -= delta_time;
      if ( start_delay < 0 ) {
        current_millis = -start_delay;
        start_delay = 0;
      } else {
        return;
      }
    } else {
      current_millis += delta_time;
      if ( current_millis >= frequency * 1 ) {
        pulse = true;
      } else {
        pulse = false;
      }
      current_millis %= frequency;
    }
  }

  void draw() {
    if ( !pulse ) return;
    noStroke();
    fill( 255 );
    rect( pos[0], pos[1], size[0], size[1] );
  }
}