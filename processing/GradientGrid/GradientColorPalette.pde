static class GradientColorPalette {

  private static GradientColorPalette ref;
  private ArrayList< GradientColor > colors;
  public static float angle = 0;
  public static float sinus = 0;

  private GradientColorPalette() {
    colors = new ArrayList< GradientColor >();
  }

  public static void remove( GradientColor src ) {
    if ( ref == null ) {
      ref = new GradientColorPalette();
    }
    ref.colors.remove( src );
  }

  public static void store( GradientColor src ) {
    if ( ref == null ) {
      ref = new GradientColorPalette();
    }
    for ( GradientColor gc : ref.colors ) {
      if ( src == gc ) {
        return;
      }
    }
    ref.colors.add( src );
  }

  public static void update( float delta ) {
    if ( ref == null ) {
      ref = new GradientColorPalette();
    }
    GradientColorPalette.angle += delta * 0.01;
    GradientColorPalette.sinus = ( 1 + sin( GradientColorPalette.angle ) ) * 0.5;
    for ( GradientColor gc : ref.colors ) {
      gc.update( delta );
    }
  }
}