class GradientColor extends PVector {

  public PVector angl = new PVector( 0, 0, 0 );
  public PVector angl_speed = new PVector( 0.0001, 0, 0.000714251 );
  public PVector animated = new PVector( 0, 0, 0 );
  public GradientColor[] origins = null;
  public float[] origins_alpha = null;

  GradientColor() {
    super();
  }

  GradientColor( float x, float y, float z ) {
    super( x, y, z );
  }

  GradientColor( GradientColor src ) {

    super();
    set( src );
    this.angl.set( src.angl );
    this.angl_speed.set( src.angl_speed );
    this.animated.set( src.animated );
  }

  GradientColor( GradientColor ca, GradientColor cb, float alpha ) {

    super();
    mix( ca, cb, alpha );
    origins = new GradientColor[] { ca, cb };
    origins_alpha = render_alpha( alpha );
    
  }

  public color _color() {

    return color( 
      animated.x * 255, 
      animated.y * 255, 
      animated.z * 255
      );

  }
  
  public float[] render_alpha( float alpha ) {
    alpha = ( 1 + sin( alpha * PI * 0.5 ) ) * 0.5;
    float ahpla = ( 1 + sin( ( 1 - alpha ) * PI * 0.5 ) ) * 0.5;
    float ta = alpha + ahpla;
    return new float[] { alpha / ta, ahpla / ta };
  }

  public void mix( GradientColor ca, GradientColor cb, float alpha ) {

    float[] aa = render_alpha( alpha );
    alpha = aa[0];
    float ahpla = aa[1];
    
    //float ahpla = 1 - alpha;
    //float ra = pow( alpha, 1.2 );
    //float rai = pow( 1 - alpha, 1.2 );
    //float ta = ra + rai;
    //alpha = ra / ta;
    //float ahpla = rai / ta;
    this.x = cb.x * alpha + ca.x * ahpla;
    this.y = cb.y * alpha + ca.y * ahpla;
    this.z = cb.z * alpha + ca.z * ahpla;
    this.angl.x = cb.angl.x * alpha + ca.angl.x * ahpla;
    this.angl.y = cb.angl.y * alpha + ca.angl.y * ahpla;
    this.angl.z = cb.angl.z * alpha + ca.angl.z * ahpla;
    this.angl_speed.x = cb.angl_speed.x * alpha + ca.angl_speed.x * ahpla;
    this.angl_speed.y = cb.angl_speed.y * alpha + ca.angl_speed.y * ahpla;
    this.angl_speed.z = cb.angl_speed.z * alpha + ca.angl_speed.z * ahpla;
    this.animated.x = cb.animated.x * alpha + ca.animated.x * ahpla;
    this.animated.y = cb.animated.y * alpha + ca.animated.y * ahpla;
    this.animated.z = cb.animated.z * alpha + ca.animated.z * ahpla;
    
  }

  public void lerp( GradientColor src, float alpha ) {

    float[] aa = render_alpha( alpha );
    alpha = aa[0];
    float ahpla = aa[1];
    
    println( "lerp " + aa[0] + " / " + aa[1] );
    
    this.x = src.x * alpha + this.x * ahpla;
    this.y = src.y * alpha + this.y * ahpla;
    this.z = src.z * alpha + this.z * ahpla;
    this.angl.x = src.angl.x * alpha + this.angl.x * ahpla;
    this.angl.x = src.angl.y * alpha + this.angl.y * ahpla;
    this.angl.x = src.angl.z * alpha + this.angl.z * ahpla;
    this.angl_speed.x = src.angl_speed.x * alpha + this.angl_speed.x * ahpla;
    this.angl_speed.x = src.angl_speed.y * alpha + this.angl_speed.y * ahpla;
    this.angl_speed.x = src.angl_speed.z * alpha + this.angl_speed.z * ahpla;
    this.animated.x = src.animated.x * alpha + this.animated.x * ahpla;
    this.animated.x = src.animated.y * alpha + this.animated.y * ahpla;
    this.animated.x = src.animated.z * alpha + this.animated.z * ahpla;
    
  }

  public void add( GradientColor src ) {

    super.add( (PVector) src );
    this.angl.add( src.angl );
    this.angl_speed.add( src.angl_speed );
    this.animated.add( src.animated );
    
  }

  public PVector mult( float m ) {

    super.mult( m );
    this.angl.mult( m );
    this.angl_speed.mult( m );
    this.animated.mult( m );
    return (PVector) this;
    
  }

  public void update( float delta ) {

    if ( origins != null ) {
      
      animated.set(
        origins[0].animated.x * origins_alpha[1] + 
        origins[1].animated.x * origins_alpha[0],
        origins[0].animated.y * origins_alpha[1] + 
        origins[1].animated.y * origins_alpha[0],
        origins[0].animated.z * origins_alpha[1] + 
        origins[1].animated.z * origins_alpha[0]
      );
      return;
      
    }

    angl.x += delta * angl_speed.x;
    angl.y += delta * angl_speed.y;
    angl.z += delta * angl_speed.z;
    animated.set(
      color_ranges[0][0] + ( 1 + sin( angl.x ) ) * 0.5 * ( color_ranges[0][1] - color_ranges[0][0] ), 
      color_ranges[1][0] + ( 1 + sin( angl.y ) ) * 0.5 * ( color_ranges[1][1] - color_ranges[1][0] ), 
      color_ranges[2][0] + ( 1 + sin( angl.z ) ) * 0.5 * ( color_ranges[2][1] - color_ranges[2][0] )
      );
    //animated.set(
    //  x * angl_speed.x * GradientColorPalette.sinus,
    //  y * angl_speed.y * GradientColorPalette.sinus,
    //  z * angl_speed.z * GradientColorPalette.sinus
    //);
    
  }
}