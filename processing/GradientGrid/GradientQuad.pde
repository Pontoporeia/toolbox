class GradientQuad {

  public PVector[] aabb;
  public PVector size;
  public GradientColor[] colors;

  public GradientQuad[] children;

  public GradientQuad() {
    init(null);
  }

  public GradientQuad(PVector[] pts) {
    init(pts);
  }

  private void init( PVector[] pts ) {
    aabb = new PVector[2];
    if ( pts == null ) {
      aabb[0] = new PVector(0, 0, 0);
      aabb[1] = new PVector(width, height, 0);
    } else {
      aabb[0] = new PVector();
      aabb[0].add( pts[0] );
      aabb[1] = new PVector();
      aabb[1].add( pts[1] );
    }
    size = new PVector();
    size.add( aabb[0] );
    size.mult( -1 );
    size.add( aabb[1] );
    colors = new GradientColor[4];
    for ( int i = 0; i < 4; ++i ) {
      colors[i] = new GradientColor( 
        color_ranges[0][0],
        color_ranges[1][0],
        color_ranges[2][0]
        );
      GradientColorPalette.store( colors[i] );
    }
    children = null;
  }

  public void set( 
    GradientColor c0, 
    GradientColor c1, 
    GradientColor c2, 
    GradientColor c3 ) {
    for ( int i = 0; i < 4; ++i ) {
      GradientColorPalette.remove( colors[i] );
    }
    colors[0] = c0;
    colors[1] = c1;
    colors[2] = c2;
    colors[3] = c3;
    for ( int i = 0; i < 4; ++i ) {
      GradientColorPalette.store( colors[i] );
    }
  }

  public boolean hit( int x, int y ) {
    if ( 
      x >= aabb[0].x && x < aabb[1].x &&
      y >= aabb[0].y && y < aabb[1].y
      ) {
      return true;
    }
    return false;
  }

  public void delegate( int x, int y ) {
    for ( int i = 0; i < 4; ++i ) {
      if ( children[i].hit( x, y ) ) {
        children[i].split(x, y);
        return;
      }
    }
  }

  public void split( int x, int y ) {

    if ( size.x <= 1 || size.y <= 1 ) {
      // refusing to split
      return;
    }

    if ( children != null ) {
      delegate( x, y );
      return;
    }

    PVector[][] aabbs = new PVector[4][2];
    for ( int i = 0; i < 4; ++i ) {
      aabbs[i] = new PVector[2];
      aabbs[i][0] = new PVector();
      aabbs[i][0].add( aabb[0] );
      aabbs[i][1] = new PVector();
      aabbs[i][1].add( aabb[0] );
    }

    aabbs[0][1].x = x;
    aabbs[0][1].y = y;

    aabbs[1][0].x = x;
    aabbs[1][1].x = aabb[1].x;
    aabbs[1][1].y = y;

    aabbs[2][0].x = x;
    aabbs[2][0].y = y;
    aabbs[2][1].x = aabb[1].x;
    aabbs[2][1].y = aabb[1].y;

    aabbs[3][0].y = y;
    aabbs[3][1].x = x;
    aabbs[3][1].y = aabb[1].y;

    children = new GradientQuad[4];
    for ( int i = 0; i < 4; ++i ) {
      children[i] = new GradientQuad( aabbs[i] );
    }

    // colors

    float ry = ( y - aabb[0].y ) * 1.f / ( size.y - 1 );
    float ryi = 1 - ry;
    float rx = ( x - aabb[0].x ) * 1.f / ( size.x - 1 );
    float rxi = 1 - rx;

    GradientColor color01 = new GradientColor( colors[0], colors[1], rx );
    GradientColor color12 = new GradientColor( colors[1], colors[2], ry );
    GradientColor color23 = new GradientColor( colors[3], colors[2], rx );
    GradientColor color30 = new GradientColor( colors[0], colors[3], ry );
    GradientColor color_mid = new GradientColor( 0, 0, 0 );
    for ( int i = 0; i < 4; ++i ) {
      color_mid.add( colors[i] );
    }
    color_mid.mult( 0.25 );
    color_mid.x = 1 - color_mid.x;
    color_mid.y = 1 - color_mid.y;
    color_mid.z = 1 - color_mid.z;

    //color01.set(
    //  colors[0].x * rxi + colors[1].x * rx,
    //  colors[0].y * rxi + colors[1].y * rx,
    //  colors[0].z * rxi + colors[1].z * rx
    //);
    //color23.set(
    //  colors[3].x * rxi + colors[2].x * rx,
    //  colors[3].y * rxi + colors[2].y * rx,
    //  colors[3].z * rxi + colors[2].z * rx
    //);
    //color12.set(
    //  colors[1].x * ryi + colors[2].x * ry,
    //  colors[1].y * ryi + colors[2].y * ry,
    //  colors[1].z * ryi + colors[2].z * ry
    //);
    //color30.set(
    //  colors[0].x * ryi + colors[3].x * ry,
    //  colors[0].y * ryi + colors[3].y * ry,
    //  colors[0].z * ryi + colors[3].z * ry
    //);

    children[0].set( colors[0], color01, color_mid, color30 );
    children[1].set( color01, colors[1], color12, color_mid );
    children[2].set( color_mid, color12, colors[2], color23 );
    children[3].set( color30, color_mid, color23, colors[3] );
  }

  public void draw() {

    if ( children != null ) {
      for ( int i = 0; i < 4; ++i ) {
        children[i].draw();
      }
      return;
    }

    GradientColor cA = new GradientColor();
    GradientColor cB = new GradientColor();
    GradientColor cC = new GradientColor();

    for ( float y = aabb[0].y; y < aabb[1].y; ++y ) {

      float ry = ( y - aabb[0].y ) * 1.f / ( size.y - 1 );
      cA.mix( colors[0], colors[3], ry );
      cB.mix( colors[1], colors[2], ry );
      //float ryi = 1 - ry;
      //cA.set(
      //  colors[0].x * ryi + colors[3].x * ry,
      //  colors[0].y * ryi + colors[3].y * ry,
      //  colors[0].z * ryi + colors[3].z * ry
      //  );
      //cB.set(
      //  colors[1].x * ryi + colors[2].x * ry,
      //  colors[1].y * ryi + colors[2].y * ry,
      //  colors[1].z * ryi + colors[2].z * ry
      //  );

      for ( float x = aabb[0].x; x < aabb[1].x; ++x ) {
        float rx = ( x - aabb[0].x ) * 1.f / ( size.x - 1 );
        cC.mix( cA, cB, rx );
        pixels[ int( x + y * width ) ] = cC._color();
        //float rxi = 1 - rx;
        //cC.set(
        //  cA.x * rxi + cB.x * rx,
        //  cA.y * rxi + cB.y * rx,
        //  cA.z * rxi + cB.z * rx
        //);
        //cC.mult( 255 );
        //pixels[ int( x + y * width ) ] = color( cC.x, cC.y, cC.z );
      }
    }
  }

  void debug() {

    if ( children != null ) {
      for ( int i = 0; i < 4; ++i ) {
        children[i].debug();
      }
      return;
    }

    noFill();
    stroke( 255 );
    rect( aabb[0].x, aabb[0].y, size.x, size.y );
  }
}