GradientQuad root;
boolean enable_debug;

float[][] color_ranges;
float timestamp;

void setup() {

  size(600, 600);
  enable_debug = false;

  color_ranges = new float[][]{
    { 0, 1 }, 
    { 0.9, 0.99 }, 
    { 0.8, 1.0 }
  };

  root = new GradientQuad();
  root.colors[0].angl_speed.set( 0, 0, 0 );
  root.colors[1].angl_speed.set( 0, 0, 0 );
  root.colors[2].angl_speed.set( 0, 0, 0 );
  root.colors[3].angl_speed.set( 0, 0, 0 );
  root.colors[0].animated.set( 0, 1, 1 );
  root.colors[1].animated.set( 0.25, 1, 1 );
  root.colors[2].animated.set( 0.5, 1, 1 );
  root.colors[3].animated.set( 1, 1, 1 );

  timestamp = -1;

  colorMode(HSB, 255);
}

void draw() {

  float delta = 0;
  float now = millis();
  if ( timestamp != -1 ) {
    delta = now - timestamp;
  }
  timestamp = now;
  //GradientColorPalette.update( delta );

  background(0);
  loadPixels();
  root.draw();
  updatePixels();

  if ( enable_debug ) root.debug();
}

void mousePressed() {

  root.split( mouseX, mouseY );
}

void keyPressed() {

  if ( keyCode == 32 ) { // space bar
    enable_debug = !enable_debug;
  } else {
    println( keyCode );
  }
}