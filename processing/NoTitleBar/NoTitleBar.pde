import java.awt.MouseInfo;

void setup() {

  size( 800,600 );
  
  frame.dispose();
  frame.setUndecorated(true);
  frame.pack();

}

void draw() {

  java.awt.Point mousepos = MouseInfo.getPointerInfo().getLocation();
  
  frame.setBounds( int( mousepos.x - 400 + random( -10, 10 ) ), int( mousepos.y - 300 + random( -10, 10 ) ), 800, 600 );
  
  background( random(255), random(255), random(255) );

}
