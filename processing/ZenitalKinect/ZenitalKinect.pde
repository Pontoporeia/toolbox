// importation de SimpleOpenNI
import SimpleOpenNI.*;
// declaration de l'objet "context"
SimpleOpenNI context;

// création de variable 
int valeurPlusProche; 
int valeurXPlusProche;
int valeurYPlusProche;
int valeurProfondeurActuelle;

boolean[] vfiltrees;
int distancesol = 2900; //distance initial avec le sol 
float boulerayon = 40; //Rayon du cercle (masque) 

void setup()
{
  // dimensions de la fenetre
  size(640, 480);
  // initialisation de l'objet context 
  context = new SimpleOpenNI(this);
  // initialisation de la vue (profondeur).
  context.enableDepth();
  vfiltrees = new boolean[ 307200 ];    //creation de filtre de distance (sol)
}


void draw() {
  valeurPlusProche = 1200; 
  // initialisation de la vue profondeur
  context.update();
  // obtenir le tableau contenant toutes les valeurs de distances de l'image
  // création d'un tableau de nombres entier nommé valeursProfondeurs
  int[] valeursProfondeurs = context.depthMap();
  //detection des points entre la kinect et le filtre sol
  for ( int i = 0; i < 307200; i++ ) {
    if ( valeursProfondeurs[ i ] > 0 && valeursProfondeurs[ i ] < distancesol ) {    
      vfiltrees[ i ] = true;
    } else {
      vfiltrees[ i ] = false;
    }
  }

  int selectedi = -1;    // pixel selectionné
  int selectedx = 0;     
  int selectedy = 0;
  float smallestd = 10000;    //points le plus proche du centre
  float milieux = width * 0.5f;    
  float milieuy = height * 0.5f;
  //pour selectionner le nouveau pixel
  for ( int y = 0; y < height; y++ ) {
    for ( int x = 0; x < width; x++ ) {
      int i = x + y * width;
      // on applique le filtre de distance du sol
      if ( vfiltrees[ i ] ) {
        float d = dist( milieux, milieuy, x, y );
        // si un pixel hors de la sphere et plus petit que le dernier pixel plus proche alors c le nouveau point le plus proche selectionné
        if ( d > boulerayon && d < smallestd ) {
          smallestd = d;
          selectedi = i;
          selectedx = x;
          selectedy = y;
        }
      }
    }
  }

  // création 
  loadPixels();
  for ( int i = 0; i < 307200; i++ ) {
    if ( vfiltrees[ i ] ) {
      pixels[ i ] = color( 180 );
    } else {
      pixels[ i ] = color( 0 );
    }
  }
  updatePixels();

  noFill();
  stroke( 255, 0, 0 );
  ellipse( milieux, milieuy, boulerayon * 2, boulerayon * 2 );

  // pixel selectionné 
  if ( selectedi != -1 ) {
    ellipse( selectedx, selectedy, 10, 10 );
    text( smallestd, selectedx, selectedy );
  }
  if (smallestd>40 && smallestd<80) {
    println("zone 1");
    stroke(0, 255, 0);
    ellipse( selectedx, selectedy, 20, 20 );
  }
  if (smallestd>80 && smallestd<120) {
    println("zone 2");
    stroke(0, 0, 255);
    ellipse( selectedx, selectedy, 20, 20 );
  }
  if (smallestd>120 && smallestd<160) {
    println("zone 3");
    stroke(0, 255, 255);
    ellipse( selectedx, selectedy, 20, 20 );
  }
  if (smallestd>160) {
    println("zone 4");
    stroke(255, 0, 255);
    ellipse( selectedx, selectedy, 20, 20 );
  }
  
//  
//  float d = dist (  milieux , milieuy ,selectedx, selectedy);
//  
//  float  distance = map(d, 30, 320, 0, 1);
//  if (smallestd>0 && smallestd<0.25) {
//    println("zone 1");
//    stroke(0, 255, 0);
//    ellipse( selectedx, selectedy, 20, 20 );
//  }
//  if (smallestd>0.25 && smallestd<0.5) {
//    println("zone 2");
//    stroke(0, 0, 255);
//    ellipse( selectedx, selectedy, 20, 20 );
//  }
//  if (smallestd>0.5 && smallestd<0.75) {
//    println("zone 3");
//    stroke(0, 255, 255);
//    ellipse( selectedx, selectedy, 20, 20 );
//  }
//  if (smallestd>0.75) {
//    println("zone 4");
//    stroke(255, 0, 255);
//    ellipse( selectedx, selectedy, 20, 20 );
//  }
//    
  
  
  
  stroke(0, 255, 0);
  ellipse( milieux, milieuy, 160, 160 );

  stroke(0, 0, 255);
  ellipse( milieux, milieuy, 240, 240 );

  stroke(0, 255, 255);
  ellipse( milieux, milieuy, 320, 320 );
}











// 
void keyPressed() {
  if ( keyCode == UP ) {
    distancesol += 100;
    println( "distance sol = " + distancesol );
  } else if ( keyCode == DOWN ) {
    distancesol -= 100;
    println( "distance sol = " + distancesol );
  }
}

