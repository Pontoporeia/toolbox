// importation de SimpleOpenNI
import SimpleOpenNI.*;
// declaration de l'objet "context"
SimpleOpenNI context;

boolean[] vfiltrees;
float maxdepth = 2000;
float silhouettedepth = 350;
PGraphics pg;

float rot = 0;
float rotspeed = 0.01;
float rotoffset = 0;

PImage mask;
float[] nearcolor = new float[] { 255,0,0 };
float[] farcolor = new float[] { 0,255,0 };

void setup()
{
  // dimensions de la fenetre
  size(800, 600);
  // initialisation de l'objet context 
  context = new SimpleOpenNI(this);
  //Pgraphics
  pg = createGraphics( 640, 480 );
  mask = loadImage( "masque.png" );
  // initialisation de la vue (profondeur).
  context.enableDepth();
  vfiltrees = new boolean[ 307200 ];    //creation de filtre de distance (sol)
}

color getDistColor( int d ) {
  float ratio = ( d - 500 ) / ( maxdepth - 500 );
  float ratioi = 1 - ratio;
  return color( 
    nearcolor[ 0 ] * ratio + farcolor[ 0 ] * ratioi, 
    nearcolor[ 1 ] * ratio + farcolor[ 1 ] * ratioi, 
    nearcolor[ 2 ] * ratio + farcolor[ 2 ] * ratioi
    );
}

void draw() {
  
  // rotoffset = mouseX * 1.f / width * PI;
  
  // initialisation de la vue profondeur
  context.update();
  // obtenir le tableau contenant toutes les valeurs de distances de l'image
  // création d'un tableau de nombres entier nommé valeursProfondeurs
  float closestPoint = 0;
  int[] valeursProfondeurs = context.depthMap();
  //detection des points entre la kinect et le filtre sol
  for ( int i = 0; i < 307200; i++ ) {
    if (
    ( closestPoint == 0 && valeursProfondeurs[ i ] != 0 ) ||
    ( closestPoint > valeursProfondeurs[ i ] && valeursProfondeurs[ i ] != 0 )    
    ) {
      closestPoint = valeursProfondeurs[ i ];
    }
  }
  
  if ( closestPoint != 0 && closestPoint < maxdepth  ) {
    for ( int i = 0; i < 307200; i++ ) {
      if ( valeursProfondeurs[ i ] >= closestPoint && valeursProfondeurs[ i ] <= closestPoint + silhouettedepth ) {    
        vfiltrees[ i ] = true;
      } else {
        vfiltrees[ i ] = false;
      }
    }
  } else {
    for ( int i = 0; i < 307200; i++ ) {
      vfiltrees[ i ] = false;
    }
  }

  pg.loadPixels();
  for ( int i = 0; i < 307200; i++ ) {
    if ( vfiltrees[ i ] ) {
      pg.pixels[ i ] = getDistColor( valeursProfondeurs[ i ] );
    } else {
      pg.pixels[ i ] = color( 0 );
    }
  }
  
  rot += rotspeed;
  
  pg.updatePixels();
  background( 0 );
  pushMatrix();
  translate( width/2, height/2 );
  rotate( rotoffset + rot );
  translate( -320, -240 );
  image( pg, 0,0 );
  image( mask, 0,0 );
  popMatrix();
}

void keyPressed() {
  if ( keyCode == UP ) {
    maxdepth += 10;
    println( "max depth = " + maxdepth );
  } else if ( keyCode == DOWN ) {
    maxdepth -= 10;
    println( "max depth = " + maxdepth );
  } else if ( keyCode == RIGHT ) {
    silhouettedepth += 10;
    println( "silhouette depth = " + silhouettedepth );
  } else if ( keyCode == LEFT ) {
    silhouettedepth -= 10;
    println( "silhouette depth = " + silhouettedepth );
  } else if ( key == 'v' ) {
    rotspeed -= 0.001;
    println( "rot speed = " + rotspeed );
  } else if ( key == 'c' ) {
    rotspeed += 0.001;
    println( "rot speed = " + rotspeed );
  } else if ( key == 'z' ) {
    rotoffset -= 0.001;
    println( "rot offset = " + rotoffset );
  } else if ( key == 'a' ) {
    rotoffset += 0.001;
    println( "rot offset = " + rotoffset );
  }
}

