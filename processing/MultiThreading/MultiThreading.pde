
SubProcess subproc;

void setup() {
  size( 600, 600 );
  fill( 255 );
  
  subproc = new SubProcess();
  subproc.start();
  
}

void draw() {

  background( 5,5,5 );
  text( frameRate, 10, 25 );
  text( "main: " + frameCount, 10, 40 );
  text( "subprocess: " + subproc.getCount(), 10, 55 );
  
}
